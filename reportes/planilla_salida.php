<?php

header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
ini_set('memory_limit', '9999999999999999999M');
require_once('tcpdf/tcpdf.php');

require_once("../class/bd/classbdConsultas.php");

class reporte extends TCPDF {

    public $conect_sistemas_vtv;
    public $ObjConsulta;
    public $registros;

    public $almacenista;
    public $receptor;

    function __construct($orientation='P', $unit='mm', $format='A4', $unicode=true, $encoding='UTF-8', $diskcache=false) {
        parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache);
        $this->conect_sistemas_vtv = "../database/archi_conex/sistemas_vtv_5431";
        $this->ObjConsulta = new classbdConsultas();
    }

    function header() {

        $this->registros = $this->ObjConsulta->select_data_asignacionplani($this->conect_sistemas_vtv, $_GET['idasignacion']);

        //fix array
        $this->registros = array_merge($this->registros);
        $this->registros = array_map("array_merge", $this->registros);

        if($this->registros[0][1]==""){
            $this->registros[0][1]="NO EMPLEADO";
        }


        $this->SetFont('', '', 10);


        $this->almacenista=utf8_encode($this->registros[0][1]);
        $this->receptor=utf8_encode($this->registros[0][2]);


        $htmltable = '<table border="0" width="650px" cellspacing="4">
	  <tr>
		<td width="160px" rowspan="3"><img src="imagenes/logo_vtv.jpg" style="width: 173px; height: 72px;" alt="logo"/></td>
		<td width="60px" ><font size="10">De:</font></td>
		<td width="250px"><div align="left"><font size="10"><b>' .$this->almacenista. '</b></font></div></td>
		<td width="180px"><div align="left"><font size="10">Nº. <b>' . str_pad($_GET['idasignacion'], 10, 0, STR_PAD_LEFT) . '</b></font></div></td>
	 </tr>
	 <tr>
		<td width="60px"><div align="left"><font size="10">Para:</font></div></td>
		<td width="250px"><div align="left"><font size="10"><b>' .$this->receptor. '</b></font></div></td>
		<td width="180px"><div align="left"><font size="10">FECHA: <b>' . date("d/m/Y H:i:s") . '</b></font></div></td>
	 </tr>
	 <tr>
		<td width="60px"><div align="left"><font size="10">ASUNTO:</font></div></td>
		<td colspan="3"><div align="justify"><font size="10">ENTREGA DE MATERIAL</font></div></td>
	  </tr>
	  </table>

';

        // echo $htmltable;
        $this->writeHTML($htmltable);

        //$this->Image('../imagenes/bandera.jpg','', $this->GetY()-3, 168);
    }

    function footer() {
        $this->SetFont('', '', 6);
        $this->Ln(3);
        $this->Cell(0, 0, 'SISTEMA INVENTARIO', 0, 0, 'L');
    }

    function renderizarimagetofile($url, $name, $path="imagenes/") {
        if (($f = fopen($url, 'r')) != false) {
            fclose($f);
            $res = join(file($url));
            if (($f = fopen($path . $name . ".png", "w")) != false) {
                fwrite($f, $res);
                fclose($f);
            }
        }
    }

}

//$pdf2=new MEM_IMAGE();
$pdf = new reporte(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, '', PDF_HEADER_STRING);//PDF_HEADER_TITLE
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, 'LISTADO CONSTANCIAS DE TRABAJO DEL '.$desde.' AL '.$hasta, PDF_HEADER_STRING);//PDF_HEADER_TITLE
// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(15, 38, 20);
$pdf->SetHeaderMargin(15);
$pdf->SetFooterMargin(20);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

$pdf->AddPage('P');
require_once('tcpdf/htmlcolors.php');
$pdf->Ln(2);

//print_r($pdf->registros);

$body = '<br><table border="1" width="650px">
    <tbody>
        <tr nobr="true">
        <td style="width: 100px; background-color: rgb(204, 204, 204);"><p>Lugar de traslado</p></td>
        <td colspan="3"  style="width: 550px;"><p >' . mb_strtolower($pdf->registros[0][4]) . '</p></td>
        </tr>
        <tr nobr="true">
        <td style="background-color: rgb(204, 204, 204);"><p >Programa</p></td>
        <td colspan="3" style="width: 550px;"><p >' . mb_strtolower($pdf->registros[0][3]) . '</p></td>
        </tr>
        <tr nobr="true">
        <td style="background-color: rgb(204, 204, 204);">SOLICITANTE<br>Que Recibe</td>
        <td style="width: 220px;"><br><br>
        Firma___________<br>
        '.$pdf->receptor.'</td>
        <td style="width: 110px; background-color: rgb(204, 204, 204);">ALMACENISTA<br>Que Entrega</td>
        <td style="width: 220px;"><br><br>
        Firma___________<br>
        '.$pdf->almacenista.'</td>
        </tr>
    </tbody>
    </table><br><table border="1" width="650">
    <thead>
        <tr nobr="true">
            <td style="width: 100px; background-color: rgb(204, 204, 204);">Cant</td>
            <td style="width: 510px; background-color: rgb(204, 204, 204);">Equipo</td>
            <td style="width: 20px; background-color: rgb(204, 204, 204);">E</td>
            <td style="width: 20px; background-color: rgb(204, 204, 204);">S</td>
        </tr>
    </thead>
<tbody>';
//echo $_GET['idasignacion'];
$regmat = $pdf->ObjConsulta->select_equiposasignacion($pdf->conect_sistemas_vtv, $_GET['idasignacion']);

$regmat = array_merge($regmat);
$regmat = array_map(array_merge, $regmat);

for ($i = 0; $i < count($regmat); $i++) {
    $data[$regmat[$i][1]][] = $regmat[$i][2];
}

if($data!=""){
foreach ($data as $llave => $valor) {
    $body.='<tr nobr="true">
            <td style="width: 100px;">' . count($data[$llave]) . ' ' . $llave . '</td>';
    $body.='<td style="width: 510px;">';
    foreach ($valor as $llave2 => $valor2) {
        $body.=utf8_encode($valor2) . "<br>";
    }
    $body.='</td>';
    $body.='<td style="width: 20px;"><hr></td>';
    $body.='<td style="width: 20px;"><hr></td>';
    $body.='</tr>';
}
}
$body.='</tbody>
</table>';

$body.= '<br><table border="1" width="650px">
    <tbody>
       <tr nobr="true">
        <td style="background-color: rgb(204, 204, 204); width: 125px;">SOLICITANTE<br>Que Entrega</td>
        <td style="width: 200px;"><br><br>
        Firma___________<br>
        '.$pdf->receptor.'</td>
        <td style="width: 125px; background-color: rgb(204, 204, 204);">ALMACENISTA<br>Que Recibe</td>
        <td style="width: 200px;"><br><br>
        Firma___________<br>
        '.$pdf->almacenista.'</td>
        </tr>
    </tbody>
    </table>
    <br>
    OBSERVACIONES:<hr>';

//echo $body;

$pdf->Cell(10);
$pdf->writeHTML($body, true, 0, true, 0);
$pdf->Output("planilla_".$_GET['idasignacion'].".pdf", 'I');
?>