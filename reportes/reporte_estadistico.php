<?php

header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
ini_set('memory_limit', '9999999999999999999M');
require_once('tcpdf/tcpdf.php');

require_once("../class/bd/classbdConsultas.php");
require_once("../librerias/classlibFecHor.php");

class reporte extends TCPDF {

    public $conect_sistemas_vtv;
    public $ObjConsulta;
	public $Objfechahora;
    public $registros;


    public $almacenista;
    public $receptor;

    function __construct($orientation='P', $unit='mm', $format='A4', $unicode=true, $encoding='UTF-8', $diskcache=false) {
        parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache);
        $this->conect_sistemas_vtv = "../database/archi_conex/sistemas_vtv_5431";
        $this->ObjConsulta = new classbdConsultas();
		$this->Objfechahora=new classlibFecHor();
    }

    function header() {

       // $this->registros = $this->ObjConsulta->select_data_asignacionplani($this->conect_sistemas_vtv, $_GET['idasignacion']);

        //fix array
      //  $this->registros = array_merge($this->registros);
        //$this->registros = array_map("array_merge", $this->registros);

        if($this->registros[0][1]==""){
            $this->registros[0][1]="NO EMPLEADO";
        }


        $this->SetFont('', '', 10);


        $this->almacenista=utf8_encode($this->registros[0][1]);
        $this->receptor=utf8_encode($this->registros[0][2]);

        $titulo1="ESTADISTICAS";

        $htmltable = '<table border="0" width="650px" cellspacing="4">
	  <tr>

		<td width="160px" rowspan="3"><img src="imagenes/logo_vtv.jpg" style="width: 173px; height: 72px;" alt="logo"/></td>
		<!--<div align="center"><font size="10">FECHA: <b>' . date("d/m/Y H:i:s") . '</b></font></div>-->
		<td ><br /><br /><div align="center"><font size="10"><b>'.$titulo1.'</b></font></div></td>
		<!--<td width="180px"><div align="left"><font size="10">Nº. <b>' . str_pad($_GET['idasignacion'], 10, 0, STR_PAD_LEFT).'</b></font></div></td>-->
	 </tr>
	 </table>

';
		$fechaimp=date("d/m/Y H:i:s");

        // echo $htmltable;
        $this->writeHTML($htmltable);

        //$this->Image('../imagenes/bandera.jpg','', $this->GetY()-3, 168);
    }

    function footer() {
        $this->SetFont('', '', 6);
        $this->Ln(3);
        $this->Cell(0, 0, 'FECHA:'.date("d/m/Y H:i:s").'', 0, 0, 'L');
    }

    function renderizarimagetofile($url, $name, $path="imagenes/") {
        if (($f = fopen($url, 'r')) != false) {
            fclose($f);
            $res = join(file($url));
            if (($f = fopen($path . $name . ".png", "w")) != false) {
                fwrite($f, $res);
                fclose($f);
            }
        }
    }

}

//$pdf2=new MEM_IMAGE();
$pdf = new reporte(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, '', PDF_HEADER_STRING);//PDF_HEADER_TITLE
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, 'LISTADO CONSTANCIAS DE TRABAJO DEL '.$desde.' AL '.$hasta, PDF_HEADER_STRING);//PDF_HEADER_TITLE
// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(15, 38, 20);
$pdf->SetHeaderMargin(15);
$pdf->SetFooterMargin(20);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

$pdf->AddPage('P');
require_once('tcpdf/htmlcolors.php');
$pdf->Ln(2);


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		$gerencia=$_SESSION['gerencia'];
    	$fecha_ini=$_GET['fini'];
    	$fecha_ini = $pdf->Objfechahora->flibInvertirEsIn($fecha_ini);
    	$fecha_fin=$_GET['fefin'];
    	$fecha_fin = $pdf->Objfechahora->flibInvertirEsIn($fecha_fin);
        $fechaini = $pdf->Objfechahora->flibInvertirInEs($fecha_ini);
        $fechafin = $pdf->Objfechahora->flibInvertirInEs($fecha_fin);



        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        $fecha = '<table align="center" border="1">
        <tr nobr="true"><th bgcolor="#8B0000" colspan="2"><font size="10" color="white"><b>FECHAS CONSULTADAS</b></font></th></tr>
        <tr nobr="true"><th><div align="center"><font size="10">Fecha Inicio</font></div></th><th><div align="center"><font size="10">Fecha Final</font></div></th></tr>
        <tr nobr="true"><td  align="center"><font size="8"> '.$fechaini . '</font></td><td  align="center"><font size="8">'.$fechafin.'</font></td></tr>
        </table>
        ';

    	///////////////////////////Busqueda de las pautas con estatus 26(Informe generado)//////////////////////////////////
      	$finalizadas=$pdf->ObjConsulta->selectpautasfinalizadas($pdf->conect_sistemas_vtv, $fecha_ini, $fecha_fin, $gerencia);
    	$cont_f=count($finalizadas);
    	if ($cont_f!= 0){

    		foreach ($finalizadas as $llave => $valor) {
	        $nom_evento = $valor[1];
	        $nom_programa = $valor[2];
	        $productor = $valor[3];
	        $gerencia_prod = $valor[4];
	        $fecha_citacion = $valor[5];
            $fecha_citacion = $pdf->Objfechahora->flibInvertirInEs($fecha_citacion);


	        if($nom_evento!= ""){
                $descripcion=$nom_evento;
            }else{
                $descripcion=$nom_programa;
            }
	        $botonAcc.='<a class="boton" href="../reportes/reporte_mod.php">Generar PDF</a>';
	    	$botonAcc.='<input type="button" class="boton" value="Cancelar" OnClick=CancelarRegresar("classlista.php?modulo=listadepautas");>';

	        $p_finalizada.='<tr nobr="true"><td  align="center"><font size="8"> '.$descripcion . '</font></td><td  align="center"><font size="8">'.$productor.'</font></td><td  align="center"><font size="8">'.$gerencia_prod.'</font></td><td  align="center"><font size="8">'.$fecha_citacion.'</font></td></tr>';
	    	}

	 	}else{

    		$p_finalizada = '<tr nobr="true" ><th  colspan="4" align="center"><div style="color: #FF0000;font-weight: bold;" align="center"><font size="8">Disculpe NO se encontraron PAUTAS FINALIZADAS para la fecha indicada.</font></div></th></tr>';
    	}

    	$p_finalizadas = '<table align="center" border="1">
        <tr nobr="true"><th bgcolor="#8B0000" colspan="4"><font size="10" color="white"><b>PAUTAS FINALIZADAS</b></font></th></tr>
        <tr nobr="true"><th><div align="center"><font size="10">Descripci&oacute;n</font></div></th><th><div align="center"><font size="10">Productor</font></div></th><th><div align="center"><font size="10">Gerencia</font></div></th><th><div align="center"><font size="10">Fecha de citaci&oacute;n</font></div></th></tr>
        ' . $p_finalizada . '
        <tr nobr="true"><td  align="right" colspan="4"><font size="8">Total de pautas :  '.$cont_f.'</font></td></tr>
        </table>
        ';

        //////////////////Busqueda de las pautas con estatus 25 (Informe en proceso)//////////////////////////////////
	    $pautasconinf=$pdf->ObjConsulta->selectpautasinformesenproceso($pdf->conect_sistemas_vtv, $fecha_ini, $fecha_fin, $gerencia);

	    $cont_inf= count($pautasconinf);
    	if ($cont_inf!= 0){
    		foreach ($pautasconinf as $llave3 => $valor3) {
	        $nom_evento = $valor3[1];
	        $nom_programa = $valor3[2];
	        $productor = $valor3[3];
	        $gerencia_prod = $valor3[4];
	        $fecha_citacion = $valor3[5];
	        $fecha_citacion = $pdf->Objfechahora->flibInvertirInEs($fecha_citacion);

	        if($nom_evento!= ""){
                $descripcion=$nom_evento;
            }else{
                $descripcion=$nom_programa;
            }
	        $botonAcc.='<a class="boton" href="../reportes/reporte_mod.php">Generar PDF</a>';
	    	$botonAcc.='<input type="button" class="boton" value="Cancelar" OnClick=CancelarRegresar("classlista.php?modulo=listadepautas");>';

	        $p_coninf.='<tr nobr="true" ><td  align="center"><font size="8">' .$descripcion . '</font></td><td  align="center"><font size="8">'.$productor.'</font></td><td  align="center"><font size="8">'.$gerencia_prod.'</font></td><td  align="center"><font size="8">'.$fecha_citacion.'</font></td></tr>';
	    	}

    	}else{

    		$p_coninf = '<tr nobr="true" ><th  colspan="4" align="center"><div style="color: #FF0000;font-weight: bold;" align="center"><font size="8">Disculpe NO se encontraron PAUTAS CON INFORMES EN PROCESO para la fecha indicada.</font></div></th></tr>';
    	}

    	$p_coninfs ='<table align="center" border="1">
        <tr nobr="true"><th bgcolor="#8B0000" colspan="4"><font size="10" color="white"><b>PAUTAS CON INFORMES EN PROCESO</b></font></th></tr>
        <tr nobr="true"><th><div align="center"><font size="10">Descripci&oacute;n</font></div></th><th><div align="center"><font size="10">Productor</font></div></th><th><div align="center"><font size="10">Gerencia</font></div></th><th><div align="center"><font size="10">Fecha de citaci&oacute;n</font></div></th></tr>
        ' . $p_coninf . '
        <tr nobr="true"><td  align="right" colspan="4"><font size="8">Total de pautas :  '.$cont_inf.'</font></td></tr>
        </table>
        ';


	    //////////////////Busqueda de las pautas caducadas XX(Pauta caducada)/////////////////////////////////////////////////
	    $nofinalizadas=$pdf->ObjConsulta->selectpautasnofinalizadas($pdf->conect_sistemas_vtv, $fecha_ini, $fecha_fin, $gerencia);
    	$cont_nf= count($nofinalizadas);
    	if ($cont_nf!= 0){
    		foreach ($nofinalizadas as $llave2 => $valor2) {
	        $nom_evento = $valor2[1];
	        $nom_programa = $valor2[2];
	        $productor = $valor2[3];
	        $gerencia_prod = $valor2[4];
	        $fecha_citacion = $valor2[5];
	        $fecha_citacion = $pdf->Objfechahora->flibInvertirInEs($fecha_citacion);

	        if($nom_evento!= ""){
                $descripcion=$nom_evento;
            }else{
                $descripcion=$nom_programa;
            }
	        $botonAcc.='<a class="boton" href="../reportes/reporte_mod.php">Generar PDF</a>';
	    	$botonAcc.='<input type="button" class="boton" value="Cancelar" OnClick=CancelarRegresar("classlista.php?modulo=listadepautas");>';

	        $p_nofinalizada.='<tr nobr="true" ><td  align="center"><font size="8">' .$descripcion . '</font></td><td  align="center"><font size="8">'.$productor.'</font></td><td  align="center"><font size="8">'.$gerencia_prod.'</font></td><td  align="center"><font size="8">'.$fecha_citacion.'</font></td></tr>';
	    	}

    	}else{

    		$p_nofinalizada = '<tr nobr="true" ><th  colspan="4" align="center"><div style="color: #FF0000;font-weight: bold;" align="center"><font size="8">Disculpe NO se encontraron PAUTAS CADUCADAS para la fecha indicada.</font></div></th></tr>';
    	}
    	$p_nofinalizadas ='<table align="center" border="1">
        <tr nobr="true"><th bgcolor="#8B0000" colspan="4"><font size="10" color="white"><b>PAUTAS CADUCADAS</b></font></th></tr>
        <tr nobr="true"><th><div align="center"><font size="10">Descripci&oacute;n</font></div></th><th><div align="center"><font size="10">Productor</font></div></th><th><div align="center"><font size="10">Gerencia</font></div></th><th><div align="center"><font size="10">Fecha de citaci&oacute;n</font></div></th></tr>
        ' . $p_nofinalizada . '
        <tr nobr="true"><td  align="right" colspan="4"><font size="8">Total de pautas :  '.$cont_nf.'</font></td></tr>
        </table>
        ';

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
$body = '

<table  align="center" border="0" ><tr nobr="true" ><td colspan="3"></td></tr></table>
        '.$fecha.'
<table  align="center" border="0" ><tr nobr="true" ><td colspan="3"></td></tr></table>
		'.$p_finalizadas.'
<table  align="center" border="0" ><tr nobr="true" ><td colspan="3"></td></tr></table>
    	'.$p_coninfs.'
<table  align="center" border="0" ><tr nobr="true" ><td colspan="3"></td></tr></table>
    	'.$p_nofinalizadas.'


';

$pdf->Cell(10);
$pdf->writeHTML(utf8_encode ($body), true, 0, true, 0);
$pdf->Output("Reporte_estadistico.pdf", 'I');
?>