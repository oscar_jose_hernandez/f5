<?php
$classcambioclave = new classcambioclave();
class classcambioclave
{
	function classcambioclave()
	{
		//Librerias comunes
		require("../librerias/classlibCabPie.php");
		// Libreria de bd
		require("../class/bd/classbdConsultas.php");
		// Clase Other
		require("../class/other/classOtherMenu.php");
		// Clase Interfaz
		require("../librerias/classlibSession.php");
		require("../class/interfaz/classMensaje.php");
		////////////////////////////////////////////////////
		$this->ObjclasslibSession = new classlibSession();
		$this->conect_sistemas_vtv="../database/archi_conex/sistemas_vtv_5431";


		if(isset($_SESSION['cedula']))
		{
			$this->cargarPagina();
		}
		else{
			echo"<script>var pagina='classRegistro.php';
			alert('Disculpa la session ha expirado, debe iniciar sesion nuevamente.');
			function redireccionar() {
			location.href=pagina;
			}
			setTimeout ('redireccionar()', 0);
			</script>
			";
		}
	}

	function cargarPagina()
	{
		$ficherosjs="
			<script type='text/javascript' src='../class/other/classjavascript.js'></script>";

		$this->ObjCabPie=new classlibCabPie("CAMBIO DE CLAVE","");
		$this->ObjOther=new classOtherMenu();
		$this->ObjMensaje=new classMensaje("","mostrar");
		$this->ObjclasslibSession = new classlibSession();
		$this->ObjConsulta=new classbdConsultas();
		$cedula=$_SESSION['cedula'];
		$administrador=$_SESSION['id_tipo_usuario'];
		$nombres=$_SESSION['nombres'];
		$apellidos=$_SESSION['apellidos'];

		$botonA="<input type=\"button\" class='boton' value=\"Aceptar\" OnClick='cambioclave();'>";
		$botonC="<input type=\"button\" class='boton' value=\"Cancelar\" OnClick=\"CancelarRegresar('classbienvenida.php');\">";
		$claveactual="<input type='password' id='claveactual' name='claveactual'>";
		$clavenueva="<input type='password' id='clavenueva' name='clavenueva'>";
		$claveconfirmacion="<input type='password' id='claveconfirmacion' name='claveconfirmacion'>";

		$codHtml="
		<br><br>
		<div id='datosp' align='center'>
		<div id='loadiing' align='center'></div>
		<table class='tabla'  style='width:500px';>
		<tr><th class='titulo' colspan='2'>CAMBIO DE CLAVE</th></tr>
		<tr><th>CLAVE ACTUAL</th><td>".$claveactual."</td></tr>
		<tr><th>NUEVA CLAVE</th><td>".$clavenueva."</td></tr>
		<tr><th>CONFIRMAR CLAVE</th><td>".$claveconfirmacion."</td></tr>
		</table>
		<table class='tabla'  style='width:500px';>
		<tr><th colspan='2'><div align='center'>".$botonA."&nbsp;&nbsp;&nbsp;&nbsp;".$botonC."</div></tr>
		</table>
		<BR>
		<!--<div id='loading' align='center'></div>
		<div id='datosp' name='datosp' align='center'></div>-->
		</div>
		<br>
		";


		$datosusuario=$this->ObjConsulta->selectdatousuario($this->conect_sistemas_vtv,$cedula);


		$d_nombres=$datosusuario[1][1];
		$d_apellidos=$datosusuario[1][2];

		$correo=$datosusuario[1][3];
		$telefono1=$datosusuario[1][4];
		$telefono2=$datosusuario[1][5];

		$titulo="DATOS PERSONALES";
		$htm =$this->ObjCabPie->flibHtmCab(0,$ficherosjs,'',$this->ObjOther->fomArregloAsocia2($administrador),0);
		$htm.=$this->ObjMensaje->interfazcambioclave($titulo,$cedula,$d_nombres,$d_apellidos,$correo,$telefono1,$telefono2,$codHtml);
		$htm.=$this->ObjCabPie->flibCerrarHtm("");
		echo $htm;
	}

}

?>

