<?php
require("class/htmlCargador.php"); //<-- kernel html
require("class/lectorTXT.php");

//librerias a cargar
$Archivos=
array("javascript"=>array(
"lib/jquery.js",
"lib/jquery-ajax-queue_1.0.js",
"lib/jquery-treeview/jquery.treeview.js",
"index.js"
),
"css"=>array(
"lib/jquery-treeview/jquery.treeview.css"
)
);



function subdep($arch){
	if (file_exists("DB/$arch")){
		$html.="<ul>";
		$main=new leerDatosTXT();
		$dep=$main->leerTxT("DB/$arch",";");
		array_map("htmlentities",$dep);
		$main->__destruct();

		for ($elm=0;$elm<count($dep);$elm++){
			list($arch,$datos)=$dep[$elm];
			$arrayDatos=explode("|",$datos);
			$html.="			<li><span class=\"file\">
					<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >
						<thead>
							<tr>
								<td><b>$arrayDatos[0]</b></td>
							</tr>
						</thead>
						<tbody>
							";
			for ($dt=1;$dt<=(count($arrayDatos)-1);$dt++){
				$html.= "
					<tr>
						<td>$arrayDatos[$dt]</td>
					</tr>
					";
			}
			$html.="
						</tbody>
					</table>
			</span>";
			$html.=subdepsub($arch);
			$html.="</li>";
		}
		$html.="
		</ul>";
	}
	return $html;
}

function subdepsub($arch){
	if (file_exists("DB/$arch")){
		$html.="<ul>";
		$main=new leerDatosTXT();
		$dep=$main->leerTxT("DB/$arch",";");
		array_map("htmlentities",$dep);
		$main->__destruct();

		for ($elm=0;$elm<count($dep);$elm++){
			list($arch,$datos)=$dep[$elm];
			$arrayDatos=explode("|",$datos);
			$html.="			<li><span class=\"file\">
					<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >
						<thead>
							<tr>
								<td><b>$arrayDatos[0]</b></td>
							</tr>
						</thead>
						<tbody>
							";
			for ($dt=1;$dt<=(count($arrayDatos)-1);$dt++){
				$html.= "
					<tr>
						<td>$arrayDatos[$dt]</td>
					</tr>
					";
			}
			$html.="
						</tbody>
					</table>
			</span>";
			$html.=subdepsubsub($arch);
			$html.="</li>";
		}
		$html.="
		</ul>";
	}
	return $html;
}

function subdepsubsub($arch){

	if (file_exists("DB/$arch")){
		$html.="<ul>";
		$main=new leerDatosTXT();
		$dep=$main->leerTxT("DB/$arch",";");
		array_map("htmlentities",$dep);
		$main->__destruct();

		for ($elm=0;$elm<count($dep);$elm++){
			list($arch,$datos)=$dep[$elm];
			$arrayDatos=explode("|",$datos);
			$html.="			<li><span class=\"file\">
					<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >
						<thead>
							<tr>
								<td><b>$arrayDatos[0]</b></td>
							</tr>
						</thead>
						<tbody>
							";
			for ($dt=1;$dt<=(count($arrayDatos)-1);$dt++){
				$html.= "
					<tr>
						<td>$arrayDatos[$dt]</td>
					</tr>
					";
			}
			$html.="
						</tbody>
					</table>
			</span>";
			//$html.=subdep($arch);
			$html.="</li>";
		}
		$html.="
		</ul>";
	}
	return $html;
}


?>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<?php 
cargarArchivos($Archivos);
?>
</head>
<body>



<?php

$main= new leerDatosTXT();

$dato=$main->leerTxT("DB/MAIN",";");
$main->__destruct();
//print_r($dato);
?>

<?php
array_map("htmlspecialchars",$dato);
for ($i=0;$i<count($dato);$i++){
	
	list($arch,$datos)=$dato[$i];

	$arrayDatos=explode("|",$datos);

	$html.="
	<ul id=\"example$i\" class=\"treeview-gray\">
	<li class=\"closed\">
		<span class=\"folder\">
			<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >
				<thead>
					<tr>
						<td><b>".$arrayDatos[0]."</b></td>
					</tr>
				</thead>
				<tbody>";
	for ($dt=1;$dt<=(count($arrayDatos)-1);$dt++){
		$html.= "
					<tr>
						<td>$arrayDatos[$dt]</td>
					</tr>
					";
	}
	$html.="
				</tbody>
			</table>
		</span>";

	$html.=subdep($arch);

	$html.="</li>
</ul>";
}



echo $html;
?>


</body>
</html>