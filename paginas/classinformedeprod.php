<?php

//ini_set("error_reporting","E_ALL & ~E_NOTICE");
$classmostrarpautaconasig = new classmostrarpautaconasig();

class classmostrarpautaconasig {

    function classmostrarpautaconasig() {
        //Librerias comunes
        require("../librerias/classlibCabPie.php");
        // Libreria de bd
        require("../class/bd/classbdConsultas.php");
        // Clase Other
        require("../class/other/classOtherMenu.php");
        // Clase Interfaz
        require("../librerias/classlibSession.php");
        require("../class/interfaz/classMensaje.php");
        $this->ObjclasslibSession = new classlibSession();
        $this->conect_sistemas_vtv = "../database/archi_conex/sistemas_vtv_5431";
        if (isset($_SESSION['cedula'])) {
            $this->cargarPagina();
        } else {
            echo"<script>var pagina='classRegistro.php';
            alert('Disculpa la session ha expirado, debe iniciar sesion nuevamente.');
            function redireccionar() {
               location.href=pagina;
           }
           setTimeout ('redireccionar()', 0);
           </script>
           ";
       }
   }

   function cargarPagina() {
    $ficherosjs = "
    <script type='text/javascript' src='../class/other/classjavascript.js'></script>
    <script type='text/javascript' src='../librerias/datepick/jquery.datepick.pack.js'></script>
    <script type='text/javascript' src='../librerias/datepick/jquery.datepick-es.js'></script>
    <link rel='stylesheet' href='../librerias/datepick/jquery.datepick.css' type='text/css' media='screen' charset='utf-8' />
    ";
    $this->ObjCabPie = new classlibCabPie("INFORME DE PRODUCCI&Oacute;N", "");
    $this->ObjOther = new classOtherMenu();
    $this->ObjMensaje = new classMensaje("", "mostrar");
    $this->ObjclasslibSession = new classlibSession();
    $this->ObjConsulta = new classbdConsultas();
    $this->Objfechahora = new classlibFecHor();
    $cedula = $_SESSION['cedula'];
    $administrador = $_SESSION['id_tipo_usuario'];
    $nombres = $_SESSION['nombres'];
    $apellidos = $_SESSION['apellidos'];
    if ($administrador == 26 or $administrador == 22 or $administrador==24) {
        $pauta = $_GET['id_pauta'];
        $ced_sesion = $_SESSION['cedula'];
        $estatuspauta = $this->ObjConsulta->selectestatuspautas($this->conect_sistemas_vtv, $pauta);
        $estatus = $estatuspauta[1][1];
        if ($estatus == '21' or $estatus == '25' or $estatus == '26') {
            $idspauta = $this->ObjConsulta->selectidspauta($this->conect_sistemas_vtv, $pauta);
            $id_pauta = $idspauta[1][1];
            $id_tipo_pauta = $idspauta[1][2];
            $id_locacion = $idspauta[1][3];
            $id_tipo_traje = $idspauta[1][4];
            $id_program = $idspauta[1][5];
            $id_citacion = $idspauta[1][6];
            $id_montaje = $idspauta[1][7];
            $id_emision_grabacion = $idspauta[1][8];
            $id_retorno = $idspauta[1][9];
            $id_tipo_evento = $idspauta[1][10];
            $user_reg = $idspauta[1][11];
            $pauta_lugar = $idspauta[1][12];
            $pauta_descripcionr = $idspauta[1][13];
            if (($id_program > 0) or ($id_program == null)) {
                $descprograma = $this->ObjConsulta->selectdescprograma($this->conect_sistemas_vtv, $id_program);
                $idprograma = $descprograma[1][1];
                $descripcionprog = ucwords($descprograma[1][2]);
            } else {
                $descripcionprog = ucwords($pauta_descripcionr);
            }
            $desclocacion = $this->ObjConsulta->selectdesclocacion($this->conect_sistemas_vtv, $id_locacion);
            $idlocacion = $desclocacion[1][1];
            $descripcionloc = ucwords($desclocacion[1][2]);
            $desctipotraje = $this->ObjConsulta->selectdesctipotraje($this->conect_sistemas_vtv, $id_tipo_traje);
            $descripciontraje = ucwords($desctipotraje[1][1]);
            $descproductor = $this->ObjConsulta->selectdescproductor($this->conect_sistemas_vtv, $user_reg);
            $descripcionprod = strtolower($descproductor[1][1]);
            $descripcionprod = ucwords($descripcionprod);
            $desctipoevento = $this->ObjConsulta->selectdesctipoevento($this->conect_sistemas_vtv, $id_tipo_evento);
            $idtipoevento = $desctipoevento[1][1];
            $descripcionteven = ucwords($desctipoevento[1][2]);
            $datoscitacion = $this->ObjConsulta->selectcitacion($this->conect_sistemas_vtv, $id_citacion);
            $idcitacion = $datoscitacion[1][1];
            $fechacitacion = $datoscitacion[1][2];
            $horacitacion = $datoscitacion[1][3];
            $id_lugar_citacion = $datoscitacion[1][4];
            $fechacitacion = $this->Objfechahora->flibInvertirInEs($fechacitacion);
            $lugarcitacion = $this->ObjConsulta->selectlugar($this->conect_sistemas_vtv, $id_lugar_citacion);
            $idcitacion = $lugarcitacion[1][1];
            $descripcitacion = ucwords($lugarcitacion[1][2]);
            $datosmontaje = $this->ObjConsulta->selectmontaje($this->conect_sistemas_vtv, $id_montaje);
            $idmontaje = $datosmontaje[1][1];
            $fechamontaje = $datosmontaje[1][2];
            $horamontaje = $datosmontaje[1][3];
            $id_lugar_montaje = $datosmontaje[1][4];
            $fechamontaje = $this->Objfechahora->flibInvertirInEs($fechamontaje);
            $descripmontaje = ucwords($pauta_lugar);
            $datosemision = $this->ObjConsulta->selectemision($this->conect_sistemas_vtv, $id_emision_grabacion);
            $idemision = $datosemision[1][1];
            $fechaemision = $datosemision[1][2];
            $horaemision = $datosemision[1][3];
            $id_lugar_emision = $datosemision[1][4];
            $fechaemision = $this->Objfechahora->flibInvertirInEs($fechaemision);
            $descripemision = ucwords($pauta_lugar);
            $datosretorno = $this->ObjConsulta->selectretorno($this->conect_sistemas_vtv, $id_retorno);
            $idretorno = $datosretorno[1][1];
            $fecharetorno = $datosretorno[1][2];
            $horaretorno = $datosretorno[1][3];
            $id_lugar_retorno = $datosretorno[1][4];
            $fecharetorno = $this->Objfechahora->flibInvertirInEs($fecharetorno);
            $descripretorno = ucwords($pauta_lugar);
            if ($descripretorno == '0') {
                $descripretorno = "";
            } else {
                $descripretorno = "<tr><th>Retorno:</th><td>" . $descripretorno . "</td><th>Fecha:</th><td>" . $fecharetorno . "</td><th>Hora:</th><td>" . $horaretorno . "</td></tr>";
            }
            if ($descripcionprog == '') {
                $evento = "<th>Evento:</th><td>" . $descripcioneven . "</td>";
            }
            if ($descripcioneven == '') {
                $evento = "<th>Programa:</th><td>" . $descripcionprog . "</td>";
            }
            $estatuspauta = $this->ObjConsulta->selectestatuspautas($this->conect_sistemas_vtv, $pauta);
            $estatus = $estatuspauta[1][1];

            $datoslista = $this->ObjConsulta->selectlistmatasig2($this->conect_sistemas_vtv, $id_pauta);
            foreach ($datoslista as $llave3 => $valor3) {
                $id_detalle_servicio = $valor3[1];
                $id_recurso_asignado = $valor3[2];
                $id_recurso = $valor3[3];
                $datosnojust = $this->ObjConsulta->selectlistnojust($this->conect_sistemas_vtv, $id_detalle_servicio, $id_pauta);
                $id_estatus_actual = $datosnojust[1][1];
                if ($id_estatus_actual == 13 or $id_estatus_actual == 14 or  $id_estatus_actual == 20 or $id_estatus_actual == 23 or $id_estatus_actual == 28) {
                    $datosdesc2 = $this->ObjConsulta->selectdetalles($this->conect_sistemas_vtv, $id_recurso);
                    $desc_id_recurso = $datosdesc2[1][1];
                    $idtipoinventario = $datosdesc2[1][2];
                    $datoscaracteristicas = $this->ObjConsulta->selectcaracteristicas2($this->conect_sistemas_vtv, $id_recurso_asignado);
                    $desc_caracteristicas = $datoscaracteristicas[1][2];
                    $desc_caracteristicas = strtoupper(str_replace("|", "<BR>", $desc_caracteristicas));
                    if ($desc_caracteristicas == ""){
                        $desc_caracteristicas = "Recurso no asignado";
                    }
                    if ($idtipoinventario == 1) {
                        $asistio = "<select id='asistio" . $llave3 . "' style='width:110px';>
                        <option value='0' selected>Seleccione</option>
                        <option value='1'>Si asistio</option>
                        <option value='2'>No asistio</option>
                        </select>";
                    } else {
                        $asistio = "<select id='asistio" . $llave3 . "'>
                        <option value='0' selected>Seleccione</option>
                        <option value='3'>Bueno</option>
                        <option value='4'>Regular</option>
                        <option value='5'>Malo</option>
                        <option value='6'>No entregado</option>
                        </select>";
                    }
                    $observaciones = "<textarea name='observaciones" . $llave3 . "' id='observaciones" . $llave3 . "' rows='2' cols='30'></textarea>";
                    $botonG = "<input type=\"button\" class='boton' value=\"Guardar\" OnClick=guardarinforme($pauta,$id_detalle_servicio,$llave3,$ced_sesion);>";
                    $asignados2.="<tr id='$id'><td  align='left'>" . strtoupper($desc_id_recurso) . "</td><td  align='left'>".$desc_caracteristicas."</td><td  align='left'>" . $asistio . "</td><td  align='left'>" . $observaciones . "</td><td  align='left'>" . $botonG . "</td></tr>";
                }
                $titulo2 = "LISTA DE RECURSOS";
                $asignados = "<table class='tabla' style='width:850px'; id='listarecursosasignados'>
                <tr><th class='titulo' colspan='8'>" . $titulo2 . "</th></tr>
                <tr><th><div align='center'>Descripci&oacute;n</div></th><th><div align='center'>Detalle</div></th><th><div align='center'>Asistencia&frasl;Estado</div></th><th><div align='center'>Observaciones</div></th></tr>
                <tr><td>" . $asignados2 . "</td></tr>
                </table>
                ";
            }
            $datoslistajust = $this->ObjConsulta->selectlistmatjust($this->conect_sistemas_vtv, $id_pauta);
            foreach ($datoslistajust as $llave4 => $valor4) {
                $id_detalle_servicio = $valor4[1];
                $id_informe = $valor4[2];
                $id_estado_informe = $valor4[3];
                $observaciones = $valor4[4];

                $datosnojust = $this->ObjConsulta->selectlistnojust($this->conect_sistemas_vtv, $id_detalle_servicio, $id_pauta);
                $id_estatus_actual = $datosnojust[1][1];
                if ($id_estatus_actual == 35) {
                    $datos_estado_inf = $this->ObjConsulta->selectestadoinf($this->conect_sistemas_vtv, $id_estado_informe);
                    $desc_estado = $datos_estado_inf[1][1];
                    $datosidrecurso = $this->ObjConsulta->selectidrecurso($this->conect_sistemas_vtv, $id_detalle_servicio);
                    $id_recurso_asignado = $datosidrecurso[1][1];
                    $id_recurso = $datosidrecurso[1][2];
                    $datosdesc2 = $this->ObjConsulta->selectdetalles($this->conect_sistemas_vtv, $id_recurso);
                    $desc_id_recurso = $datosdesc2[1][1];
                    $idtipoinventario = $datosdesc2[1][2];
                    $datoscaractasignado = $this->ObjConsulta->selectcaracteristicas2($this->conect_sistemas_vtv, $id_recurso_asignado);
                    $desc_caracteristicas = $datoscaractasignado[1][2];
                    if ($desc_caracteristicas == ""){
                        $desc_caracteristicas = "Recurso no asignado";
                    }
                        $botonM = "<input type=\"button\" class='boton' value=\"Modificar\" OnClick=modificarinforme($pauta,$id_informe,$id_detalle_servicio,$llave3,$ced_sesion);>"; // un update
                        $justificados2.="<tr id='$id'><td  align='left'>" . strtoupper($desc_id_recurso) . "</td><td  align='left'>" . strtoupper(str_replace("|", "<BR>", $desc_caracteristicas)) . "</td><td  align='center'>" . $desc_estado . "</td><td  align='left'>" . $observaciones . "</td><td  align='left'>" . $botonM . "</td></tr>";
                    }

                    $titulo3 = "LISTA DE RECURSOS JUSTIFICADOS";
                    $justificados = "<table class='tabla' style='width:850px'; id='listarecursosasignados'>
                    <tr><th class='titulo' colspan='8'>" . $titulo3 . "</th></tr>
                    <tr><th><div align='center'>Descripci&oacute;n</div></th><th><div align='center'>Detalle</div></th><th><div align='center'>Asistencia&frasl;Estado</div></th><th><div align='center'>Observaciones</div></th></tr>
                    <tr><td>" . $justificados2 . "</td></tr>
                    </table>
                    ";
                }

                $botonA = "<input type=\"button\" class='boton' value=\"Enviar\" OnClick=enviarsolicitud3($pauta);>
                <input name='canttotal'  id='canttotal' type='hidden' value=" . (count($datoslista)) . " />
                <input name='cantjust'  id='cantjust' type='hidden' value=" . count($datoslistajust) . " />";
                $botonC = "<input type=\"button\" class='boton' value=\"Cancelar\" OnClick=CancelarRegresar('classlista.php?modulo=listadeinformes');>";
                $titulo1 = "DATOS GENERALES DE LA PAUTA";
                $htm = $this->ObjCabPie->flibHtmCab(0, $ficherosjs, '', $this->ObjOther->fomArregloAsocia2($administrador), 0, "");
                $htm.=$this->ObjMensaje->interfazinformedeprod($pauta, $descripcionloc, $descripciontraje, $descripcionprod, $descripcionteven, $descripcitacion, $fechacitacion, $horacitacion, $descripmontaje, $fechamontaje, $horamontaje, $descripemision, $fechaemision, $horaemision, $descripretorno, $fecharetorno, $horaretorno, $obs, $evento, $asignados, $justificados, $titulo1, $titulo2, $titulo3, $botonC, $botonA, $botonG);
                $htm.=$this->ObjCabPie->flibCerrarHtm("");
                echo $htm;
            } else {
                $htm = $this->ObjCabPie->flibHtmCab(0, $ficherosjs, '', $this->ObjOther->fomArregloAsocia2($administrador), 0, "");
                $mensaje = "<div style='color: #009900;font-weight: bold;'><br>La pauta no finalizada<div><br>";
                $htm.=$this->ObjMensaje->InterfazExitosamente($mensaje);
                echo"<script>var pagina='classlistadepautas.php';
                function redireccionar() {
                   location.href=pagina;
               }
               setTimeout ('redireccionar()', 2800);
               </script>
               ";
               $htm.=$this->ObjCabPie->flibCerrarHtm("");
               echo $htm;
           }
       } else {
        echo"<script>var pagina='classRegistro.php';
        alert('Disculpa no tiene permitido el acceso a esta pagina.');
        function redireccionar() {
           location.href=pagina;
       }
       setTimeout ('redireccionar()', 0);
       </script>
       ";
   }
}
}

?>