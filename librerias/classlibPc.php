<?php
/*
    Caracas; 09/05/2008
    Clase Libreria de Pc (classLibPc)
    Intranet VTV Version 1.0
*/
	class classlibPc
	{
		function classlibPc()
		{
		}
		/*
			Funcion de Libreria, Host Ip (flibHostIp())
			Identificar la direccion Ip del Host conectado al Servidor
			Solo Funciona si el Servidor es APACHE
			Retorna la IP del Host conectado
		*/
		function flibHostIp()
		{
			return $_SERVER['REMOTE_ADDR'];
		}
		/*
			Funcion de Libreria, Host Name (flibHostName())
			Identifica el nombre del Host conectado al Servidor
			Solo Funciona si el Servidor es APACHE
			Retorna el nombre del Host conectado
		*/
		function flibHostName()
		{
			return gethostbyaddr($this->flibHostIp());
		}
    }
?>