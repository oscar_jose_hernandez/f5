<?php
@setlocale('LC_ALL','es_VE.UTF-8');
/**
 * clase manejadora de html y carga de datos compatible con intranet estructura
 * logica.
 */



if (defined("path")) {
	include("../database/classdb.php");
}else{
	include("../../database/classdb.php");
}
/**
 * optiene dato de post o get
 *
 * @param string $nombre
 * @return string
 */
function getpost($nombre){
	if ($_GET[$nombre]!="") {
		$dato=$_GET[$nombre];
	}elseif ($_POST[$nombre]!="") {
		$dato=$_POST[$nombre];
	}
	return  utf8_decode($dato);
}
class vtvcore {
	private $t_inicial;

	public $menu;

	function __construct($js,$css,$class_principal="contenedor_principal",$intranetCab=true){
		$this->t_inicial=microtime();
		?>
<html>
	<head>
		<title></title>
		<meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
		<?php
		for($i=0;$i<count($js);$i++){
			?>
		<script type="text/javascript" src="<?php echo $js[$i]; ?>"></script>
			<?php
		}
		for ($i=0;$i<count($css);$i++){
			?>
		<link href="<?php echo $css[$i]; ?>" rel="stylesheet" type="text/css" />	
			<?php
		}
		?>
		
		<link href="../../estilos/jquery.alerts.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="../../librerias/classcore.js"></script>
		<script type="text/javascript" src="../../librerias/jquery.alerts.js"></script>
		
	</head>
	<body>
	<div class="<?php echo $class_principal; ?>">
		<?php
		if($intranetCab==true){
			vtvcore::cablib();
		}

	}

	function login(){
		?>
<div align="center">
	<table class="tabla" style="width:500px;">
		<tr>
			<th colspan="2" class="titulo">INGRESO</th>
		</tr>
		<tr>
			<td valign="top">
				<table width="90%" border="0" align="center" valign="middle" cellpadding="0" cellspacing="5">
					<td width="50%">
						<table width="100%"  border="0" cellspacing="8" cellpadding="0">
							<tr>
								<td class="text2"><div align="right">Introduzca su cedula y clave de usuario haga click en "entrar" o presione "enter" Espere confirmación </div></td>
							</tr>
						</table>
					</td>
					<td width="1">
						<img src="images/log_03.jpg" width="2" height="236">
					</td>
		
					<form action="classCarnetFunciones.php" method="post" name="form_registro" id="form_registro">
					<td width="50%">
						<table width="100%"  border="0" cellspacing="8" cellpadding="0">
							<tr>
								<td class="text2">Introduzca su <b>cedula</b></td>
							</tr>
							<tr>
								<td>
									<input type="text" maxlength="8"  name="cedula" id="cedula" onkeypress="return solonum(event)" />
								</td>
							</tr>
							<tr>
								<td class="text2">Introduzca su <b>clave de usuario</b></td>
							</tr>
							<tr>
								<td> 
									<input type="password" name="clave" id="clave"/>
								</td>
							</tr>
							<tr>
								<td>
									<button onclick="validarIngreso()" type="button" class="boton2" value="entrar"></button>
								</td>
							</tr>
							<tr>
								<td class="text2"> 
									<!--confirmación <input name="validarUsuario" type="hidden" value="validarUsuario" />-->
								</td>
							</tr>
						</table>
					</td>
					</form>
					</tr>
				</table>
			</td>
		</tr>
	</table>	
</div>
<br>
<div name="cont" id="cont"><div name="loading" align="center" id="loading"></div>	</div>
		<?php
	}

	/**
	 * genera el encabezado de una pagina... nolmalmente 
	 *
	 */
	function cablib($logoCSS="logo_top"){
		include("classlibCabPie.php");
		$libcap=new classlibCabPie("","");

		
		?>
	<!--<div class="logo_aniversario" style="Z-index:0" >
		<!-- <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=8,0,0,0"  height="90" width="133" title="vtv">
		    <param name="allowScriptAccess" value="sameDomain" />
		    <param name="wmode" value="transparent" />
		    <param name="movie" value="css/imagenes/vtv1.swf" />
		    <param name="quality" value="high" /><param name="bgcolor" value="#ffffff" />
		    <embed wmode="transparent" src="css/imagenes/vtv1.swf" quality="high" bgcolor="#ffffff" height="90" width="133" name="banner" align="middle" allowScriptAccess="sameDomain" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" />
		</object> 
	</div>-->
	
	<!--<div class="<?php echo $logoCSS; ?>">
		
		<div class="cont_feha">
			<?php
			echo strftime("%A %e %B %Y", mktime(0, 0, 0, date("m"), date("d"), date("y")));
			?>
		</div>-->
		<?php
		if (count($_SESSION)>0) {
			$menu='<div class="menu_apli">
				<ul id="navmenu-h">';
					
					if (function_exists("menus")) {
						ob_start();
						menus();
						$menu.=ob_get_contents();
						ob_end_clean();
					}
	              
					$menu.='<li>
	                    <a href="#" onclick="cerrarsistema();">cerrar</a>
	                </li>
                </ul>
			</div>';

		

		?>
		

		
		
		<?php
		}

		echo $libcap->flibHtmCab_noestandar($menu,strftime("%A %e %B %Y", mktime(0, 0, 0, date("m"), date("d"), date("y"))));
	}

	/**
	 * genera el pie de pagina automaticamente
	 */
	function __destruct(){
		?>
	<div class="cont_bottom"/></div>
	</div>
	<div class="footer">
	Desarrollado por la Gerencia de Tecnolog&iacute;a de la Informaci&oacute;n y Comunicaci&oacute;n <br>
	Navegador Recomendado Mozilla Firefox a	Resoluci&oacute;n 1024x768 <br>
	impreso en: <?php echo microtime()-$this->t_inicial; ?> microsegundos
	</div>
	</body>
</html>
		<?php
	}



}

/**
 * clase manejadora de autenticacion de usuario, sistema de autenticacion
 * intranet
 */

class auth extends classdb {
	//private $query;
	private $cx;

	function __construct(){
		;
		//conexion
		if (defined("path")) {
			auth::classdb("../database/archi_conex/intranet_vtv");
		}else{
			auth::classdb("../../database/archi_conex/intranet_vtv");
		}

		//auth::classdb("../database/archi_conex/intranet_vtv");
		$this->cx=auth::fdbConectar();
	}

	function chequearusuario_intranet($id_aplicacion,$id_modulo,$cedula,$pass=true){
		if($cedula!=""){
			$sql="select * from intranet.t_acceso where id_aplicacion='$id_aplicacion' and id_modulo='$id_modulo' and cedula='$cedula' and fecha_exp='2222-12-31' ;";
			$exec = pg_query($sql) or die($sql);
			if(pg_numrows($exec)>0){
				//validar clave
				$sql="select * from intranet.t_intranet_usuario where clave= '$pass' and cedula='$cedula' and fecha_exp='2222-12-31'";
				$exec = pg_query($sql) or die($sql);
				if(pg_numrows($exec)>0){
					return true;
				}else {
					return false;
				}
			}else{
				return false;
			}
		}else{
			return false;
		}

	}


	/**
	 * Matar Conexion.
	 */
	function __destruct(){
		@pg_close($this->cx);
	}
}

class interfaz extends auth {
	private $idprograma;//1
	private $idmodulo;//3

	private $ci;
	private $pass;

	private $page;

	function __construct($sys_path,$root_class){
		include($sys_path);
		$this->page=new $root_class();
		parent::__construct();
	}

	function mostrarpagina(){

		if (getpost('p')==""){
			//cerrando la session anterior.
			session_destroy();
			$p="login";
		}else{
			if(parent::chequearusuario_intranet($this->idprograma,$this->idmodulo,$this->ci,$this->pass)){
				$p=getpost('p');
			}else{
				$p="login";
			}
		}
		if (method_exists($this->page,$p)) {
			$this->page->$p();
		}else{
			echo "no existe pagina consultada";
		}

	}

	function set_uids($ci,$pass){
		$this->ci=$ci;
		$this->pass=$pass;
	}

	function setids($idprograma,$idmodulo){
		$this->idprograma=$idprograma;
		$this->idmodulo=$idmodulo;
	}

	function __destruct(){
		parent::__destruct();
	}
}
?>
