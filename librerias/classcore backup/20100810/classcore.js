//depende de jAlert
//valida el ingreso de un usuario a los sistemas.
function validarIngreso()
{
	//alert('Entre en Registar Usuario');
	if($("#cedula").val()==""){
		jAlert('Disculpe debe escribir el usuario', 'Alerta');
		$("#cedula").focus();
		return false;
	}
	if($("#clave").val()==""){
		jAlert('Disculpe debe escribir la clave', 'Alerta');
		$("#clave").focus();
		return false;
	}
	
	$.ajax({
		type: "GET",
		url: "../../librerias/classcore_auth.php",
		data: "&ci="+$('#cedula').val()
		+"&pass="+$('#clave').val(),
		beforeSend: function(datos){
			$("#loading").html('<img src="../../../estilos/imagenes/estatus/cargando2.gif" />');
		},
		success: function(datos){
			window.location="index.php?p=inicio";
		},
		complete: function(datos){
			$("#loading").fadeOut('slow');
		}
	});
}

function cerrarsistema(){
	$.ajax({
		type: "POST",
		url: "../../librerias/classcore_auth.php",
		data: "salir=true",
		beforeSend: function(datos){
			//$("#loading").html('<img src="../../../estilos/imagenes/estatus/cargando2.gif" />');
		},
		success: function(datos){
			window.location="index.php";
		},
		complete: function(datos){
			//$("#loading").fadeOut('slow');
		}
	});
}

//valida la entrada de numeros en un campo de texto
function solonum(evt)
{
	var keyPressed = (evt.which) ? evt.which : evt.keyCode;
	//alert(keyPressed);
	if(keyPressed==45){
		return true;
	}else{
		return !(keyPressed > 31 && (keyPressed < 48 || keyPressed > 57));
	}
}
