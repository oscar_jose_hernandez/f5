//depende de jAlert
//valida el ingreso de un usuario a los sistemas.


var Base64 = {
	// private property
	_keyStr : "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
	// public method for encoding
	encode : function (input) {
		var output = "";
		var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
		var i = 0;
		input = Base64._utf8_encode(input);
		while (i < input.length) {
			chr1 = input.charCodeAt(i++);
			chr2 = input.charCodeAt(i++);
			chr3 = input.charCodeAt(i++);
			enc1 = chr1 >> 2;
			enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
			enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
			enc4 = chr3 & 63;

			if (isNaN(chr2)) {
				enc3 = enc4 = 64;
			} else if (isNaN(chr3)) {
				enc4 = 64;
			}
			output = output +
			this._keyStr.charAt(enc1) + this._keyStr.charAt(enc2) +
			this._keyStr.charAt(enc3) + this._keyStr.charAt(enc4);
		}

		return output;
	},

	// public method for decoding
	decode : function (input) {
		var output = "";
		var chr1, chr2, chr3;
		var enc1, enc2, enc3, enc4;
		var i = 0;

		input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

		while (i < input.length) {

			enc1 = this._keyStr.indexOf(input.charAt(i++));
			enc2 = this._keyStr.indexOf(input.charAt(i++));
			enc3 = this._keyStr.indexOf(input.charAt(i++));
			enc4 = this._keyStr.indexOf(input.charAt(i++));

			chr1 = (enc1 << 2) | (enc2 >> 4);
			chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
			chr3 = ((enc3 & 3) << 6) | enc4;

			output = output + String.fromCharCode(chr1);

			if (enc3 != 64) {
				output = output + String.fromCharCode(chr2);
			}
			if (enc4 != 64) {
				output = output + String.fromCharCode(chr3);
			}

		}

		output = Base64._utf8_decode(output);

		return output;

	},

	// private method for UTF-8 encoding
	_utf8_encode : function (string) {
		string = string.replace(/\r\n/g,"\n");
		var utftext = "";

		for (var n = 0; n < string.length; n++) {

			var c = string.charCodeAt(n);

			if (c < 128) {
				utftext += String.fromCharCode(c);
			}
			else if((c > 127) && (c < 2048)) {
				utftext += String.fromCharCode((c >> 6) | 192);
				utftext += String.fromCharCode((c & 63) | 128);
			}
			else {
				utftext += String.fromCharCode((c >> 12) | 224);
				utftext += String.fromCharCode(((c >> 6) & 63) | 128);
				utftext += String.fromCharCode((c & 63) | 128);
			}

		}

		return utftext;
	},

	// private method for UTF-8 decoding
	_utf8_decode : function (utftext) {
		var string = "";
		var i = 0;
		var c = c1 = c2 = 0;

		while ( i < utftext.length ) {

			c = utftext.charCodeAt(i);

			if (c < 128) {
				string += String.fromCharCode(c);
				i++;
			}
			else if((c > 191) && (c < 224)) {
				c2 = utftext.charCodeAt(i+1);
				string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
				i += 2;
			}
			else {
				c2 = utftext.charCodeAt(i+1);
				c3 = utftext.charCodeAt(i+2);
				string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
				i += 3;
			}

		}

		return string;
	}
}




//configuracion de variable de comunicacion a archivos
var classcoreurl="../../";
//overlays triggers
var triggers="";


/**
------- function que crea el objeto select de las gerencias de sigai
*/

function selectgerenciasigai(contenedor,nombre,callback,contenedorcallback,nombrecallback){
	$("#"+nombrecallback).remove();
	selectlibre_ajax(nombre,contenedor,"class=apolo&q=selectlibre&cx=conexion_sigai&dbconsulta=select_sigai_gerencias&archconx=conect_sigai",function (){
		callback($(this).val(),contenedorcallback,nombrecallback);
	});
}

function selectdivisionessigai(idgerencia,contenedor,nombre){
	//jAlert(idgerencia,"Alerta");
	selectlibre_ajax(nombre,contenedor,'class=apolo&q=selectlibre&cx=conexion_sigai&dbconsulta=select_sigai_division&archconx=conect_sigai&valor="'+idgerencia+'"',function (){
		jAlert($(this).val());
	});
}

/**
-------------------objetos core
*/
var corelistar= new Array();
//funcion que crea listados con la libreria flexigrid 2.0
//la regla es que el campo id siempre se llame id
function listar(c_arraydisplay,b_arraynam,nombreId,nombreListado,contEnedor,htUrl){
	var confCol= Array();
	var confBot= Array();
	for(i=0;i<c_arraydisplay.length;i++){
		confCol[(confCol.length)]={display: c_arraydisplay[i].display, name : c_arraydisplay[i].id, width : c_arraydisplay[i].width , sortable : true, align: c_arraydisplay[i].align};
	}
	for(i=0;i<b_arraynam.length;i++){
		confBot[(confBot.length)]={name:b_arraynam[i].display, bclass: b_arraynam[i].classname, boverlay:b_arraynam[i].overlaybb, onpress : b_arraynam[i].callbacks};
	}
	var tabla=$("<table>").attr("name", nombreId).attr("id",nombreId).attr("align","left");
	//tabla.addClass('tabla');
	$("#"+contEnedor).append(tabla);
	corelistar[corelistar.length]=$("#"+nombreId).flexigrid
	({
		url: htUrl,
		dataType: 'json',
		colModel : confCol,
		buttons : confBot,
		searchitems : [
		{display: 'General', name : 'nombre', isdefault: true}
		],
		sortname: "id",
		sortorder: "desc",
		usepager: true,
		title: nombreListado,
		useRp: true,
                singleSelect: true,
                pagestat: 'Viendo {from} hasta {to} de {total} registros',
                procmsg: 'Procesando espere ...',
                pagetext: 'Pagina',
		outof: 'de',
                nomsg: 'No existen registros',
                errormsg: 'ERROR EN CONEXION... ',
                findtext: 'BUSQUEDA',
		rp: 10,
		showTableToggleBtn: false,
		width: 'auto',
		height: 'auto'
	});
}

//function para crear objetos select
function crearselect(nombre,data,defaultsid_selected, namedefault){
	var selec=$("<select>").attr("name",nombre).attr("id",nombre).css("display","none");


        if(defaultsid_selected==undefined){
            
            var opcciondefecto=$("<option>")
            .attr("value","")
            .attr("selected","selected");
            
            if(namedefault!=undefined){
                opcciondefecto.html(namedefault);
            }else{
                opcciondefecto.html("-------");
            }
            opcciondefecto.appendTo(selec);
            
        }

        for(i=0;i<data.length;i++){
            var opccionselect=$("<option>");

            opccionselect.attr("value",data[i]['id']).html(data[i]['valor']);
            if(defaultsid_selected!=undefined){
                if(defaultsid_selected==data[i]['id']){
                    opccionselect.attr("selected","selected");
                }
            }
            
            opccionselect.appendTo(selec);
	}
	return selec;
}

//funcion para crear selects libres
//funcion que sirve para crear selects de los objetos.
function selectlibre_ajax(nombre,contenedor,argobj,callback,defaultsid_selected, perspectiva,ndefault){
	$("#"+perspectiva).find("#"+contenedor).find("#"+nombre).remove();
	manejadorAjax.add({
                beforeSend: function(datos){
			$("#"+perspectiva).find("#"+contenedor).html('<img id="carga_'+contenedor+'" src="css/imagenes/loading.gif" />');
		},
		success: function(html1) {

			html1=eval("(" + html1 + ")")
			var selec=crearselect(nombre,html1,defaultsid_selected,ndefault);
			//los callbacks
			if(callback!=undefined){
				selec.change(callback);
			}
			selec.appendTo($("#"+perspectiva).find("#"+contenedor));
			$("#"+perspectiva).find("#"+contenedor).find("#"+nombre).fadeIn();
		},
                complete: function(datos){
			$("#"+perspectiva).find("#"+contenedor).find("#carga_"+contenedor).fadeOut('slow',function(){
                            $(this).remove();
                        });
		},
		url: classcoreurl+'librerias/classcore_obj.php',
		data: 's=obj&tipo=combolista&'+argobj
	});
}

/*servicio ajax y conexion a datos*/
function ajaxserv(argobj,callback,msgwait,abortcallback){
    manejadorAjax.add({
                beforeSend: function(datos){
                    if(msgwait==undefined){
                        msgwait="PROCESANDO";
                    }
           
                    jAlert(msgwait+":<br><img src=\"css/imagenes/estatus/loadingAnimation.gif\" alt=\"Procesando\"/>","proceso",function(){
                        manejadorAjax.abort();
                        if(abortcallback!=undefined){
                            abortcallback();
                        }
                    });
                    $("#popup_ok").val("cancelar");

		},
		success: function(html1) {
                    $.alerts._hide();
                    if(callback!=undefined){
                        callback(html1);
                    }
		},
                type: "POST",
		url: classcoreurl+'librerias/classcore_obj.php',
		data: 's=obj&tipo=ajaxserv&q=selectlibre'+argobj
	});
}
//funcion que sirve para crear divs
function creardiv(nombre){
	nombre=nombre.replace(" ","_");
	return $("<div>").attr("id",nombre);
}
//funcion que tiene como finalidad crear botones
function crearboton(nombre,valor,callback,callbackarg){
	var boton=$("<button>").attr("id",nombre).addClass("boton").text(valor);
	if(callback!=undefined){
		boton.click(function(){
			callback(callbackarg);
		});
	}
	return boton;
}
//function que se usa para validar el ingreso de los usuarios.
function validarIngreso()
{
	//alert('Entre en Registar Usuario');
	if($("#cedula").val()==""){
		jAlert('Disculpe debe escribir el usuario', 'Alerta');
		$("#cedula").focus();
		return false;
	}
	if($("#clave").val()==""){
		jAlert('Disculpe debe escribir la clave', 'Alerta');
		$("#clave").focus();
		return false;
	}
	$.ajax({
		type: "GET",
		url: classcoreurl+"librerias/classcore_auth.php",
		data: "&ci="+$('#cedula').val()
		+"&pass="+$('#clave').val()+"&idprograma="+$('#idprograma').val()+"&idmodulo="+$('#idmodulo').val()
		+"&cxonex="+$("#cxonex").val(),
		beforeSend: function(datos){
			$("#loading").html('<img src="css/imagenes/estatus/cargando2.gif" />');
		},
		success: function(datos){
			$('#clave').val("");
			if(datos=="false"){
				jAlert('Disculpe, Usted no tiene acceso al sistema o su contraseña esta mal escrita, compruebe nuevamente', 'Alerta');
			}else{
				window.location="index.php?p=inicio";
			}
		},
		complete: function(datos){
			$("#loading").fadeOut('slow');
		}
	});
}

//funcion para cerrar el sistema de logueo y registro de usuario
function cerrarsistema(){
	$.ajax({
		type: "POST",
		url: classcoreurl+"librerias/classcore_auth.php",
		data: "salir=true",
		beforeSend: function(datos){
			//$("#loading").html('<img src="../../../estilos/imagenes/estatus/cargando2.gif" />');
		},
		success: function(datos){
			window.location="index.php";
		},
		complete: function(datos){
			//$("#loading").fadeOut('slow');
		}
	});
}

function recargarpagina(){
    window.location="index.php";
}

//rechequea la session automaticamente a partir del inicio de seccion
function auth_recheck(callback,pwdchg){
	$.ajax({
		type: "POST",
		url: classcoreurl+"librerias/classcore_auth.php",
		data: "recheck=true",
		success: function(datos){

			if(datos=="false"){
                           recargarpagina();
			}else if(datos=="" || datos==undefined){
                            $("body").css("background-color", "red").html("ERROR");
                            jAlert("Ups! Aparentemente he perdido conectividad con el servidor de aplicaciones de vtv,<br>informe de este error a los programadores y cierre el navegador y abralo nuevamente, <br> si el problema persiste informe de un posible problema en la conectividad a soporte tecnico,<br>SE A DESACTIVADO TODO EL SISTEMA POR MEDIDA DE SEGURIDAD DE DATOS","error de conectividad",recargarpagina);
                        }else{
                            if(pwdchg!=undefined){
                                datos=eval("("+datos+")");
                                if(datos['pass_sys']=="123456"){
                                    if(callback!=undefined){
                                        callback();
                                    }
                                }
                            }
                        }
		},
		timeout: 5000,
		error: function(request,error) {
                        $("body").css("background-color", "red").html("ERROR");
			jAlert("Ups! Aparentemente he perdido conectividad con el servidor de aplicaciones de vtv,<br>informe de este error a los programadores y cierre el navegador y abralo nuevamente, <br> si el problema persiste informe de un posible problema en la conectividad a soporte tecnico,<br>SE A DESACTIVADO TODO EL SISTEMA POR MEDIDA DE SEGURIDAD DE DATOS","error de conectividad",recargarpagina);
		}
	});
}


//funcion para eliminar los botones despues de un cierre automatico o manual
function cleanize_menu(){
	$("#navmenu-h").html("");
}

//valida la entrada de numeros en un campo de texto
function solonum(evt){
	var keyPressed = (evt.which) ? evt.which : evt.keyCode;
	//alert(keyPressed);
	if(keyPressed==45){
		return true;
	}else{
		return !(keyPressed > 31 && (keyPressed < 48 || keyPressed > 57));
	}
}

function pressenter(evt){
	var keyPressed = (evt.which) ? evt.which : evt.keyCode;
	if(keyPressed==13){
		validarIngreso();
	}
}

function createoverlaysadm(nombreover, callback){
    $("#principal_sistema").append(
        $("<div>").attr("id",nombreover).addClass("apple_overlay")
    );
    if(callback!=undefined){
        callback($("#"+nombreover),nombreover);
    }

    $("#"+nombreover).append(
            $("<form>").append(
                $("<table>").append($("<tr>").append($("<td>").append($("<button>").attr("type","button").addClass("close").addClass("boton").text("Cancelar"), $("<button>").attr("type","submit").addClass("boton").css("display","none").text("Guardar"))))
    ));

}

function adm_users(idprograma){
    createoverlaysadm("agregar", function(obj,nobj){
        obj.append($("<div>").attr("id","rspdata_core").append(
            $("<table>").attr("id","adm_user_"+obj.attr("id")).addClass("Tabla").css({width:"100%",padding:"1px",margin:"0px"}).append(
                $("<thead>").append($("<tr>").append($("<th>").attr("colspan","2").text("Nuevo Usuario"))),
                $("<tbody>").append($("<tr>").append($("<td>").append("Especifique la cedula de identidad"),
                $("<td>").append($("<input>").attr("id","addusrcoreinput").keyup(function(event){
                    if (event.keyCode == '13') {
                        ajaxserv('&tojson=true&class=classbdConsultas&cx=conexion_sigai&dbconsulta=sigai_dtrabajador&archconx=conect_sigai&cedula='+$(this).val(),function(data){
                        if(data!="null"){
                            data=eval("("+data+")");
                            $("#corerespuser").html($("<table>").attr("id","adm_user_"+obj.attr("id")).addClass("Tabla").css({width:"100%",padding:"1px",margin:"0px"}).append(
                            $("<thead>").append($("<tr>").append($("<th>").attr("colspan","2").text("Personal"))),
                            $("<tbody>").append($("<tr>").append(
                            $("<td>").attr("rowspan","4").css("width","70").html("<img height=\"60\" width=\"60\" src=\"../../intranet/directorio/paginas/download_foto.php?id="+Base64.encode(data[1])+"\" />"),
                            $("<td>").append($("<div>").attr("id","adm_core_nyp").text(data[2]))),
                            $("<tr>").append($("<td>").append($("<div>").attr("id","adm_core_ger").text(data[5]))),
                            $("<tr>").append($("<td>").append($("<div>").attr("id","adm_core_car").text(data[4]))),
                            $("<tr>").append($("<td>").append($("<div>").attr("id","coreperfilesadm")))))).fadeIn(function(){
                                select_admcoreperfiles("idperfil","coreperfilesadm","agregar",idprograma);
                            });

                            $("button[type='submit']").fadeIn();

                        }else{
                            jAlert("Esta persona aun no se encuentra en los sistemas de nomina avise a el departamento de nomina el ingreso de dicho personal","Aviso");
                        }
                        },"CHEQUEANDO EN SISTEMAS SIGAI");
                        
                        
                        
                        event.preventDefault();
                    }else{
                        $("#corerespuser").fadeOut(function(){
                            $(this).html("");
                        });
                        $("button[type='submit']").fadeOut();

                    }
                }))),$("<tr>").append($("<td>").attr("colspan","2").append($("<div>").css("display","none").attr("id","corerespuser"))))
            )))
    })

        var c_arraydisplay=Array(
		{display:"Nombres", id:"nombre_usuario", width:328, aling:"left"},
		{display:"Apellidos", id:"apellido_usuario", width:328, aling:"left"},
		{display:"Perfil", id:"perfil", width:272, aling:"left"}
	);
	var b_arraynam=Array(
		{display:"Agregar", classname:"addx", overlaybb:"#agregar", callbacks:function(){
                       // $("button[type='submit']").text("Guardar");
                }},
		{display:"Eliminar", classname:"del", callbacks:deladm_user}
	);
	var htUrl=classcoreurl+"librerias/classcore_obj.php?class=classbdConsultas&s=obj&tipo=listados&q=selectlibre&cx=conexion_postgree&dbconsulta=select_coreusers_sys&archconx=conect_sistemas&idprograma="+idprograma;
	var nombreId="admusers";
	var nombreListado="ADMINISTRACIÓN DE USUARIOS";
	var contEnedor="listas";
	listar(c_arraydisplay,b_arraynam,nombreId,nombreListado,contEnedor,htUrl);
}

function deladm_user(com,grid){
    if($('.trSelected',grid).length==0){
        jAlert("DEBE SELECCIONAR UNA PERSONA PARA SU ELIMINACION","alerta");
    }else{
        jConfirm( "Quiere usted eliminar el registro seleccionado", "Eliminar", function(r){
            if(r){
                servidorajax("&dbconsulta=del_admuser&id="+$('.trSelected',grid).attr("id").substr(3),function(data){
                    jAlert(data,"Proceso",function(){
                        corelistar[0].flexOptions({newp: 1}).flexReload();
                    });
                });
            }
        })
    }
}

function select_admcoreperfiles(nombre,contenedor,perspectiva,idprograma,idselected,ndefault){
	selectlibre_ajax(nombre,contenedor,"class=classbdConsultas&q=selectlibre&cx=conexion_postgree&dbconsulta=coreaplic_perfiles&archconx=conect_sistemas&idprograma="+idprograma,function(){},idselected,perspectiva,ndefault);
}

function create_form_user_adm(idprograma){
    adm_users(idprograma);
            var overlaycoreadmusr=$("span[rel]").overlay({
                                    // some mask tweaks suitable for modal dialogs
                                    mask: {
                                            color: '#ddd',
                                            loadSpeed: 200,
                                            opacity: 0.9
                                    },
                                    closeOnClick: false,
                                    oneInstance: false
            }).click(function(){
                $("button[type='submit']").css("display","none");
                perspectiva=$(this).attr("rel").substr(1);
                $("#addusrcoreinput").val("");
                //borrando resultados
                $("#corerespuser").html("");
            });

                $("form").submit(function(e){
                        adm_user_add(overlaycoreadmusr,$(this),idprograma);
                        return e.preventDefault();
                });
}


//manejador de servicio ajax para acciones de sistema core.
function coreajaxserv(arg, callback,msgwait,abortcallback){
     ajaxserv("&cx=conexion_postgree&class=classbdConsultas&archconx=conect_sistemas"+arg,function(data){
              if(callback!=undefined){
                  callback(data);
              }
     },msgwait,function(){
           if(abortcallback!=undefined){
               abortcallback();
           }
     });
}

function adm_user_add(coreobjoverlay,obj,idprograma){

    $("#addusrcoreinput").val();

     servidorajax("&dbconsulta=serch_usrcoreprogram&idprograma="+idprograma+"&ci="+$("#addusrcoreinput").val(),function(data){
         data=eval("("+data+")");
         if(data[1][1]=="0"){
             servidorajax("&dbconsulta=add_admuser&idprograma="+idprograma+"&ci="+$("#addusrcoreinput").val()+'&idperfil='+$("#idperfil").val()+'&nyp='+$("#adm_core_nyp").text()+"&geren="+$("#adm_core_ger").text()+"&cargo="+$("#adm_core_car").text(),function(data2){
                 jAlert(data2,"proceso",function(){
                      corelistar[0].flexOptions({newp: 1}).flexReload();
                 });
             })
         }else{
             servidorajax("&dbconsulta=add_admuser&idprograma="+idprograma+"&ci="+$("#addusrcoreinput").val()+'&idperfil='+$("#idperfil").val(),function(data2){
                 jAlert(data2,"proceso",function(){
                      corelistar[0].flexOptions({newp: 1}).flexReload();
                 });
             })
         }
     });

    //revisar si esta agregado a el programa....
    //
    //
    //
    //jAlert("nombre="+$("#adm_core_nyp").text()+" gerencia="+$("#adm_core_ger").text()+" cargo="+$("#adm_core_car").text());
     coreobjoverlay.eq(0).overlay().close();
}

function coresysnewuser(){
    jPrompt('Cambie su contraseña:', '123456', 'Sistema de seguridad', function(r) {
        //jAlert("es ->"+r);
        if(r!=null && r!="" && r!="null"){
            servidorajax("&dbconsulta=core_update_pwd&pwdchg="+r,function(data2){
                jAlert(data2,"alerta",function(){
                    recargarpagina();
                });
            })
        }else{
            jAlert("Se le seguira preguntando hasta que cambie su contraseña","Alerta");
        }
        
    });
}