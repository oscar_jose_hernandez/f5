--
-- PostgreSQL database dump
--

-- Dumped from database version 8.4.9
-- Dumped by pg_dump version 9.3.1
-- Started on 2014-07-23 16:43:42

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

--
-- TOC entry 26 (class 2615 OID 151365)
-- Name: f5; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA f5;


ALTER SCHEMA f5 OWNER TO postgres;

--
-- TOC entry 30 (class 2615 OID 151369)
-- Name: inventario; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA inventario;


ALTER SCHEMA inventario OWNER TO postgres;

SET search_path = f5, pg_catalog;

--
-- TOC entry 1594 (class 1255 OID 177380)
-- Name: concatenar(text, text); Type: FUNCTION; Schema: f5; Owner: postgres
--

CREATE FUNCTION concatenar(text, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$

DECLARE
t text;
BEGIN
     IF character_length($1) > 0 THEN
         IF character_length($2) > 0 THEN
             t = $1 || ' | ' || $2;
         else
             t = $1;
         end if;
     ELSE
         t = $2;
     END IF;
RETURN t;
END;
$_$;


ALTER FUNCTION f5.concatenar(text, text) OWNER TO postgres;

SET search_path = inventario, pg_catalog;

--
-- TOC entry 1595 (class 1255 OID 177381)
-- Name: concatenar(text, text); Type: FUNCTION; Schema: inventario; Owner: postgres
--

CREATE FUNCTION concatenar(text, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$

DECLARE
t text;
BEGIN
     IF character_length($1) > 0 THEN
         IF character_length($2) > 0 THEN
             t = $1 || ' | ' || $2;
         else
             t = $1;
         end if;
     ELSE
         t = $2;
     END IF;
RETURN t;
END;
$_$;


ALTER FUNCTION inventario.concatenar(text, text) OWNER TO postgres;

--
-- TOC entry 1604 (class 1255 OID 189126)
-- Name: fnc_update_valorinventariado(); Type: FUNCTION; Schema: inventario; Owner: postgres
--

CREATE FUNCTION fnc_update_valorinventariado() RETURNS void
    LANGUAGE plpgsql
    AS $$
DECLARE
reg1 RECORD;
--reg2 RECORD;

BEGIN

    FOR reg1 IN EXECUTE 'SELECT  a.idinventario, a.idmaterialgeneral,b.idtipomaterial FROM inventario.inventario a, inventario.materialgeneral b 
    WHERE a.idmaterialgeneral =b.idmaterialgeneral and a.idcaracteristica=18 and b.idtipomaterial=344'  
    LOOP

    --INSERT INTO hcm.listado VALUES (reg1.co_empleado, reg1.nu_documen, reg1.de_corto, 'TITULAR',reg1.ti_sexo, reg1.fe_nacimiento);

    --FOR reg2 IN EXECUTE 'SELECT idtipomaterial, desctipomaterial FROM inventario.tipomaterial WHERE  idtipomaterial='||reg1.idtipomaterial
   -- LOOP
       
      update inventario.inventario set valorinventariado= 'PERSONAL ADMINISTRATIVO' where idmaterialgeneral=reg1.idmaterialgeneral and idcaracteristica=18;

   -- END LOOP;

    END LOOP;

END;
$$;


ALTER FUNCTION inventario.fnc_update_valorinventariado() OWNER TO postgres;

--
-- TOC entry 1605 (class 1255 OID 189127)
-- Name: fnc_update_valorinventariado_admni(); Type: FUNCTION; Schema: inventario; Owner: postgres
--

CREATE FUNCTION fnc_update_valorinventariado_admni() RETURNS void
    LANGUAGE plpgsql
    AS $$
DECLARE
reg1 RECORD;
--reg2 RECORD;

BEGIN

    FOR reg1 IN EXECUTE 'SELECT  a.idinventario, a.idmaterialgeneral,b.idtipomaterial FROM inventario.inventario a, inventario.materialgeneral b 
    WHERE a.idmaterialgeneral =b.idmaterialgeneral and a.idcaracteristica=18 and b.idtipomaterial in (54,55,56,61,62,63,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117118,119,120,121,122,123,124,127,130,134,135,136,137,138,145,255,154,160,161,162,163,164,165,166,167,171,173,174,175,178,179,180,181,182,183,184,185,186,188,189,190,191,194,196,197,210,211,212,213,214,215,216,220,221,222,225,227,234,235,236,237,238,239,255,266,267,268,270,291,303,304,305,306,307,308,309,324,325,326,327)'  
    LOOP

    --INSERT INTO hcm.listado VALUES (reg1.co_empleado, reg1.nu_documen, reg1.de_corto, 'TITULAR',reg1.ti_sexo, reg1.fe_nacimiento);

    --FOR reg2 IN EXECUTE 'SELECT idtipomaterial, desctipomaterial FROM inventario.tipomaterial WHERE  idtipomaterial='||reg1.idtipomaterial
   -- LOOP
       
      update inventario.inventario set valorinventariado= 'PERSONAL ADMINISTRATIVO' where idmaterialgeneral=reg1.idmaterialgeneral and idcaracteristica=18;

   -- END LOOP;

    END LOOP;

END;
$$;


ALTER FUNCTION inventario.fnc_update_valorinventariado_admni() OWNER TO postgres;

--
-- TOC entry 1606 (class 1255 OID 189128)
-- Name: fnc_update_valorinventariado_al(); Type: FUNCTION; Schema: inventario; Owner: postgres
--

CREATE FUNCTION fnc_update_valorinventariado_al() RETURNS void
    LANGUAGE plpgsql
    AS $$
DECLARE
reg1 RECORD;
--reg2 RECORD;

BEGIN

    FOR reg1 IN EXECUTE 'SELECT  a.idinventario, a.idmaterialgeneral,b.idtipomaterial FROM inventario.inventario a, inventario.materialgeneral b 
    WHERE a.idmaterialgeneral =b.idmaterialgeneral and a.idcaracteristica=18 and b.idtipomaterial in (64,177)'  
    LOOP

    --INSERT INTO hcm.listado VALUES (reg1.co_empleado, reg1.nu_documen, reg1.de_corto, 'TITULAR',reg1.ti_sexo, reg1.fe_nacimiento);

    --FOR reg2 IN EXECUTE 'SELECT idtipomaterial, desctipomaterial FROM inventario.tipomaterial WHERE  idtipomaterial='||reg1.idtipomaterial
   -- LOOP
       
      update inventario.inventario set valorinventariado= 'APOYO LOGISTICO' where idmaterialgeneral=reg1.idmaterialgeneral and idcaracteristica=18;

   -- END LOOP;

    END LOOP;

END;
$$;


ALTER FUNCTION inventario.fnc_update_valorinventariado_al() OWNER TO postgres;

--
-- TOC entry 1607 (class 1255 OID 189129)
-- Name: fnc_update_valorinventariado_at(); Type: FUNCTION; Schema: inventario; Owner: postgres
--

CREATE FUNCTION fnc_update_valorinventariado_at() RETURNS void
    LANGUAGE plpgsql
    AS $$
DECLARE
reg1 RECORD;
--reg2 RECORD;

BEGIN

    FOR reg1 IN EXECUTE 'SELECT  a.idinventario, a.idmaterialgeneral,b.idtipomaterial FROM inventario.inventario a, inventario.materialgeneral b 
    WHERE a.idmaterialgeneral =b.idmaterialgeneral and a.idcaracteristica=18 and b.idtipomaterial in (131,132,133,289,290)'  
    LOOP

    --INSERT INTO hcm.listado VALUES (reg1.co_empleado, reg1.nu_documen, reg1.de_corto, 'TITULAR',reg1.ti_sexo, reg1.fe_nacimiento);

    --FOR reg2 IN EXECUTE 'SELECT idtipomaterial, desctipomaterial FROM inventario.tipomaterial WHERE  idtipomaterial='||reg1.idtipomaterial
   -- LOOP
       
      update inventario.inventario set valorinventariado= 'ASISTENTE TECNICO' where idmaterialgeneral=reg1.idmaterialgeneral and idcaracteristica=18;

   -- END LOOP;

    END LOOP;

END;
$$;


ALTER FUNCTION inventario.fnc_update_valorinventariado_at() OWNER TO postgres;

--
-- TOC entry 1608 (class 1255 OID 189130)
-- Name: fnc_update_valorinventariado_c(); Type: FUNCTION; Schema: inventario; Owner: postgres
--

CREATE FUNCTION fnc_update_valorinventariado_c() RETURNS void
    LANGUAGE plpgsql
    AS $$
DECLARE
reg1 RECORD;
--reg2 RECORD;

BEGIN

    FOR reg1 IN EXECUTE 'SELECT  a.idinventario, a.idmaterialgeneral,b.idtipomaterial FROM inventario.inventario a, inventario.materialgeneral b 
    WHERE a.idmaterialgeneral =b.idmaterialgeneral and a.idcaracteristica=18 and b.idtipomaterial in (125,126,146,147,148,149,150,201)'  
    LOOP

    --INSERT INTO hcm.listado VALUES (reg1.co_empleado, reg1.nu_documen, reg1.de_corto, 'TITULAR',reg1.ti_sexo, reg1.fe_nacimiento);

    --FOR reg2 IN EXECUTE 'SELECT idtipomaterial, desctipomaterial FROM inventario.tipomaterial WHERE  idtipomaterial='||reg1.idtipomaterial
   -- LOOP
       
      update inventario.inventario set valorinventariado= 'CAMARA' where idmaterialgeneral=reg1.idmaterialgeneral and idcaracteristica=18;

   -- END LOOP;

    END LOOP;

END;
$$;


ALTER FUNCTION inventario.fnc_update_valorinventariado_c() OWNER TO postgres;

--
-- TOC entry 1609 (class 1255 OID 189131)
-- Name: fnc_update_valorinventariado_i(); Type: FUNCTION; Schema: inventario; Owner: postgres
--

CREATE FUNCTION fnc_update_valorinventariado_i() RETURNS void
    LANGUAGE plpgsql
    AS $$
DECLARE
reg1 RECORD;
--reg2 RECORD;

BEGIN

    FOR reg1 IN EXECUTE 'SELECT  a.idinventario, a.idmaterialgeneral,b.idtipomaterial FROM inventario.inventario a, inventario.materialgeneral b 
    WHERE a.idmaterialgeneral =b.idmaterialgeneral and a.idcaracteristica=18 and b.idtipomaterial in (128,129,202,203,204,226,252,261,262,263,295,296,362,363)'  
    LOOP

    --INSERT INTO hcm.listado VALUES (reg1.co_empleado, reg1.nu_documen, reg1.de_corto, 'TITULAR',reg1.ti_sexo, reg1.fe_nacimiento);

    --FOR reg2 IN EXECUTE 'SELECT idtipomaterial, desctipomaterial FROM inventario.tipomaterial WHERE  idtipomaterial='||reg1.idtipomaterial
   -- LOOP
       
      update inventario.inventario set valorinventariado= 'IMAGEN' where idmaterialgeneral=reg1.idmaterialgeneral and idcaracteristica=18;

   -- END LOOP;

    END LOOP;

END;
$$;


ALTER FUNCTION inventario.fnc_update_valorinventariado_i() OWNER TO postgres;

--
-- TOC entry 1610 (class 1255 OID 189132)
-- Name: fnc_update_valorinventariado_in(); Type: FUNCTION; Schema: inventario; Owner: postgres
--

CREATE FUNCTION fnc_update_valorinventariado_in() RETURNS void
    LANGUAGE plpgsql
    AS $$
DECLARE
reg1 RECORD;
--reg2 RECORD;

BEGIN

    FOR reg1 IN EXECUTE 'SELECT  a.idinventario, a.idmaterialgeneral,b.idtipomaterial FROM inventario.inventario a, inventario.materialgeneral b 
    WHERE a.idmaterialgeneral =b.idmaterialgeneral and a.idcaracteristica=18 and b.idtipomaterial in (193,271,272,273,274,2789,280,285,286,287,288)'  
    LOOP

    --INSERT INTO hcm.listado VALUES (reg1.co_empleado, reg1.nu_documen, reg1.de_corto, 'TITULAR',reg1.ti_sexo, reg1.fe_nacimiento);

    --FOR reg2 IN EXECUTE 'SELECT idtipomaterial, desctipomaterial FROM inventario.tipomaterial WHERE  idtipomaterial='||reg1.idtipomaterial
   -- LOOP
       
      update inventario.inventario set valorinventariado= 'INGENIERIA' where idmaterialgeneral=reg1.idmaterialgeneral and idcaracteristica=18;

   -- END LOOP;

    END LOOP;

END;
$$;


ALTER FUNCTION inventario.fnc_update_valorinventariado_in() OWNER TO postgres;

--
-- TOC entry 1611 (class 1255 OID 189133)
-- Name: fnc_update_valorinventariado_p(); Type: FUNCTION; Schema: inventario; Owner: postgres
--

CREATE FUNCTION fnc_update_valorinventariado_p() RETURNS void
    LANGUAGE plpgsql
    AS $$
DECLARE
reg1 RECORD;
--reg2 RECORD;

BEGIN

    FOR reg1 IN EXECUTE 'SELECT  a.idinventario, a.idmaterialgeneral,b.idtipomaterial FROM inventario.inventario a, inventario.materialgeneral b 
    WHERE a.idmaterialgeneral =b.idmaterialgeneral and a.idcaracteristica=18 and b.idtipomaterial in (158,159,259,260)'  
    LOOP

    --INSERT INTO hcm.listado VALUES (reg1.co_empleado, reg1.nu_documen, reg1.de_corto, 'TITULAR',reg1.ti_sexo, reg1.fe_nacimiento);

    --FOR reg2 IN EXECUTE 'SELECT idtipomaterial, desctipomaterial FROM inventario.tipomaterial WHERE  idtipomaterial='||reg1.idtipomaterial
   -- LOOP
       
      update inventario.inventario set valorinventariado= 'PANTALLA' where idmaterialgeneral=reg1.idmaterialgeneral and idcaracteristica=18;

   -- END LOOP;

    END LOOP;

END;
$$;


ALTER FUNCTION inventario.fnc_update_valorinventariado_p() OWNER TO postgres;

--
-- TOC entry 1612 (class 1255 OID 189134)
-- Name: fnc_update_valorinventariado_pp(); Type: FUNCTION; Schema: inventario; Owner: postgres
--

CREATE FUNCTION fnc_update_valorinventariado_pp() RETURNS void
    LANGUAGE plpgsql
    AS $$
DECLARE
reg1 RECORD;
--reg2 RECORD;

BEGIN

    FOR reg1 IN EXECUTE 'SELECT  a.idinventario, a.idmaterialgeneral,b.idtipomaterial FROM inventario.inventario a, inventario.materialgeneral b 
    WHERE a.idmaterialgeneral =b.idmaterialgeneral and a.idcaracteristica=18 and b.idtipomaterial in (198,199,200)'  
    LOOP

    --INSERT INTO hcm.listado VALUES (reg1.co_empleado, reg1.nu_documen, reg1.de_corto, 'TITULAR',reg1.ti_sexo, reg1.fe_nacimiento);

    --FOR reg2 IN EXECUTE 'SELECT idtipomaterial, desctipomaterial FROM inventario.tipomaterial WHERE  idtipomaterial='||reg1.idtipomaterial
   -- LOOP
       
      update inventario.inventario set valorinventariado= 'POST PRODUCCION' where idmaterialgeneral=reg1.idmaterialgeneral and idcaracteristica=18;

   -- END LOOP;

    END LOOP;

END;
$$;


ALTER FUNCTION inventario.fnc_update_valorinventariado_pp() OWNER TO postgres;

--
-- TOC entry 1613 (class 1255 OID 189135)
-- Name: fnc_update_valorinventariado_pr(); Type: FUNCTION; Schema: inventario; Owner: postgres
--

CREATE FUNCTION fnc_update_valorinventariado_pr() RETURNS void
    LANGUAGE plpgsql
    AS $$
DECLARE
reg1 RECORD;
--reg2 RECORD;

BEGIN

    FOR reg1 IN EXECUTE 'SELECT  a.idinventario, a.idmaterialgeneral,b.idtipomaterial FROM inventario.inventario a, inventario.materialgeneral b 
    WHERE a.idmaterialgeneral =b.idmaterialgeneral and a.idcaracteristica=18 and b.idtipomaterial in (195,205,253,292,293)'  
    LOOP

    --INSERT INTO hcm.listado VALUES (reg1.co_empleado, reg1.nu_documen, reg1.de_corto, 'TITULAR',reg1.ti_sexo, reg1.fe_nacimiento);

    --FOR reg2 IN EXECUTE 'SELECT idtipomaterial, desctipomaterial FROM inventario.tipomaterial WHERE  idtipomaterial='||reg1.idtipomaterial
   -- LOOP
       
      update inventario.inventario set valorinventariado= 'PRENSA' where idmaterialgeneral=reg1.idmaterialgeneral and idcaracteristica=18;

   -- END LOOP;

    END LOOP;

END;
$$;


ALTER FUNCTION inventario.fnc_update_valorinventariado_pr() OWNER TO postgres;

--
-- TOC entry 1614 (class 1255 OID 189136)
-- Name: fnc_update_valorinventariado_pro(); Type: FUNCTION; Schema: inventario; Owner: postgres
--

CREATE FUNCTION fnc_update_valorinventariado_pro() RETURNS void
    LANGUAGE plpgsql
    AS $$
DECLARE
reg1 RECORD;
--reg2 RECORD;

BEGIN

    FOR reg1 IN EXECUTE 'SELECT  a.idinventario, a.idmaterialgeneral,b.idtipomaterial FROM inventario.inventario a, inventario.materialgeneral b 
    WHERE a.idmaterialgeneral =b.idmaterialgeneral and a.idcaracteristica=18 and b.idtipomaterial in (256)'  
    LOOP

    --INSERT INTO hcm.listado VALUES (reg1.co_empleado, reg1.nu_documen, reg1.de_corto, 'TITULAR',reg1.ti_sexo, reg1.fe_nacimiento);

    --FOR reg2 IN EXECUTE 'SELECT idtipomaterial, desctipomaterial FROM inventario.tipomaterial WHERE  idtipomaterial='||reg1.idtipomaterial
   -- LOOP
       
      update inventario.inventario set valorinventariado= 'PROMOCIONES' where idmaterialgeneral=reg1.idmaterialgeneral and idcaracteristica=18;

   -- END LOOP;

    END LOOP;

END;
$$;


ALTER FUNCTION inventario.fnc_update_valorinventariado_pro() OWNER TO postgres;

--
-- TOC entry 1615 (class 1255 OID 189137)
-- Name: fnc_update_valorinventariado_seg(); Type: FUNCTION; Schema: inventario; Owner: postgres
--

CREATE FUNCTION fnc_update_valorinventariado_seg() RETURNS void
    LANGUAGE plpgsql
    AS $$
DECLARE
reg1 RECORD;
--reg2 RECORD;

BEGIN

    FOR reg1 IN EXECUTE 'SELECT  a.idinventario, a.idmaterialgeneral,b.idtipomaterial FROM inventario.inventario a, inventario.materialgeneral b 
    WHERE a.idmaterialgeneral =b.idmaterialgeneral and a.idcaracteristica=18 and b.idtipomaterial in (240,241,59,243,246)'  
    LOOP

    --INSERT INTO hcm.listado VALUES (reg1.co_empleado, reg1.nu_documen, reg1.de_corto, 'TITULAR',reg1.ti_sexo, reg1.fe_nacimiento);

    --FOR reg2 IN EXECUTE 'SELECT idtipomaterial, desctipomaterial FROM inventario.tipomaterial WHERE  idtipomaterial='||reg1.idtipomaterial
   -- LOOP
       
      update inventario.inventario set valorinventariado= 'SEGURIDAD' where idmaterialgeneral=reg1.idmaterialgeneral and idcaracteristica=18;

   -- END LOOP;

    END LOOP;

END;
$$;


ALTER FUNCTION inventario.fnc_update_valorinventariado_seg() OWNER TO postgres;

--
-- TOC entry 1616 (class 1255 OID 189138)
-- Name: fnc_update_valorinventariado_sg(); Type: FUNCTION; Schema: inventario; Owner: postgres
--

CREATE FUNCTION fnc_update_valorinventariado_sg() RETURNS void
    LANGUAGE plpgsql
    AS $$
DECLARE
reg1 RECORD;
--reg2 RECORD;

BEGIN

    FOR reg1 IN EXECUTE 'SELECT  a.idinventario, a.idmaterialgeneral,b.idtipomaterial FROM inventario.inventario a, inventario.materialgeneral b 
    WHERE a.idmaterialgeneral =b.idmaterialgeneral and a.idcaracteristica=18 and b.idtipomaterial in (60,139,140,141,142,143,144,223,224,231,232,242,265,297)'  
    LOOP

    --INSERT INTO hcm.listado VALUES (reg1.co_empleado, reg1.nu_documen, reg1.de_corto, 'TITULAR',reg1.ti_sexo, reg1.fe_nacimiento);

    --FOR reg2 IN EXECUTE 'SELECT idtipomaterial, desctipomaterial FROM inventario.tipomaterial WHERE  idtipomaterial='||reg1.idtipomaterial
   -- LOOP
       
      update inventario.inventario set valorinventariado= 'SERVICIOS GENERALES' where idmaterialgeneral=reg1.idmaterialgeneral and idcaracteristica=18;

   -- END LOOP;

    END LOOP;

END;
$$;


ALTER FUNCTION inventario.fnc_update_valorinventariado_sg() OWNER TO postgres;

--
-- TOC entry 1617 (class 1255 OID 189139)
-- Name: fnc_update_valorinventariado_tic(); Type: FUNCTION; Schema: inventario; Owner: postgres
--

CREATE FUNCTION fnc_update_valorinventariado_tic() RETURNS void
    LANGUAGE plpgsql
    AS $$
DECLARE
reg1 RECORD;
--reg2 RECORD;

BEGIN

    FOR reg1 IN EXECUTE 'SELECT  a.idinventario, a.idmaterialgeneral,b.idtipomaterial FROM inventario.inventario a, inventario.materialgeneral b 
    WHERE a.idmaterialgeneral =b.idmaterialgeneral and a.idcaracteristica=18 and b.idtipomaterial in (57,58,96)'  
    LOOP

    --INSERT INTO hcm.listado VALUES (reg1.co_empleado, reg1.nu_documen, reg1.de_corto, 'TITULAR',reg1.ti_sexo, reg1.fe_nacimiento);

    --FOR reg2 IN EXECUTE 'SELECT idtipomaterial, desctipomaterial FROM inventario.tipomaterial WHERE  idtipomaterial='||reg1.idtipomaterial
   -- LOOP
       
      update inventario.inventario set valorinventariado= 'TECNOLOGIA' where idmaterialgeneral=reg1.idmaterialgeneral and idcaracteristica=18;

   -- END LOOP;

    END LOOP;

END;
$$;


ALTER FUNCTION inventario.fnc_update_valorinventariado_tic() OWNER TO postgres;

--
-- TOC entry 1618 (class 1255 OID 189140)
-- Name: fnc_update_valorinventariado_tr(); Type: FUNCTION; Schema: inventario; Owner: postgres
--

CREATE FUNCTION fnc_update_valorinventariado_tr() RETURNS void
    LANGUAGE plpgsql
    AS $$
DECLARE
reg1 RECORD;
--reg2 RECORD;

BEGIN

    FOR reg1 IN EXECUTE 'SELECT  a.idinventario, a.idmaterialgeneral,b.idtipomaterial FROM inventario.inventario a, inventario.materialgeneral b 
    WHERE a.idmaterialgeneral =b.idmaterialgeneral and a.idcaracteristica=18 and b.idtipomaterial in (155,156,157,192)'  
    LOOP

    --INSERT INTO hcm.listado VALUES (reg1.co_empleado, reg1.nu_documen, reg1.de_corto, 'TITULAR',reg1.ti_sexo, reg1.fe_nacimiento);

    --FOR reg2 IN EXECUTE 'SELECT idtipomaterial, desctipomaterial FROM inventario.tipomaterial WHERE  idtipomaterial='||reg1.idtipomaterial
   -- LOOP
       
      update inventario.inventario set valorinventariado= 'TRANSPORTE' where idmaterialgeneral=reg1.idmaterialgeneral and idcaracteristica=18;

   -- END LOOP;

    END LOOP;

END;
$$;


ALTER FUNCTION inventario.fnc_update_valorinventariado_tr() OWNER TO postgres;

SET search_path = f5, pg_catalog;

--
-- TOC entry 4641 (class 1255 OID 177382)
-- Name: concatena(text); Type: AGGREGATE; Schema: f5; Owner: postgres
--

CREATE AGGREGATE concatena(text) (
    SFUNC = concatenar,
    STYPE = text
);


ALTER AGGREGATE f5.concatena(text) OWNER TO postgres;

SET search_path = inventario, pg_catalog;

--
-- TOC entry 4642 (class 1255 OID 177383)
-- Name: concatena(text); Type: AGGREGATE; Schema: inventario; Owner: postgres
--

CREATE AGGREGATE concatena(text) (
    SFUNC = f5.concatenar,
    STYPE = text
);


ALTER AGGREGATE inventario.concatena(text) OWNER TO postgres;

SET search_path = f5, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 622 (class 1259 OID 153206)
-- Name: t_estatus_pauta; Type: TABLE; Schema: f5; Owner: postgres; Tablespace: 
--

CREATE TABLE t_estatus_pauta (
    id_estatus_pauta integer NOT NULL,
    id_estatus integer,
    id_pauta integer,
    fecha_exp date DEFAULT '2222-12-31'::date,
    observaciones text,
    fecha_reg date DEFAULT now(),
    user_reg character varying
)
WITH (autovacuum_enabled=true);


ALTER TABLE f5.t_estatus_pauta OWNER TO postgres;

--
-- TOC entry 6523 (class 0 OID 0)
-- Dependencies: 622
-- Name: TABLE t_estatus_pauta; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON TABLE t_estatus_pauta IS 'Tabla que almacena los esatutus por los que ha pasado una pauta desde su ecomienzo hasta que se expira la pauta';


--
-- TOC entry 623 (class 1259 OID 153214)
-- Name: cant_estatus_por_pauta; Type: VIEW; Schema: f5; Owner: postgres
--

CREATE VIEW cant_estatus_por_pauta AS
SELECT t_estatus_pauta.id_pauta, t_estatus_pauta.id_estatus, t_estatus_pauta.fecha_reg, count(t_estatus_pauta.id_estatus) AS cantidad FROM t_estatus_pauta GROUP BY t_estatus_pauta.id_estatus, t_estatus_pauta.id_pauta, t_estatus_pauta.fecha_reg ORDER BY t_estatus_pauta.id_pauta;


ALTER TABLE f5.cant_estatus_por_pauta OWNER TO postgres;

--
-- TOC entry 624 (class 1259 OID 153218)
-- Name: t_detalle_servicio; Type: TABLE; Schema: f5; Owner: postgres; Tablespace: 
--

CREATE TABLE t_detalle_servicio (
    id_detalle_servicio integer NOT NULL,
    id_pauta integer NOT NULL,
    fecha_reg date,
    user_reg character varying,
    fecha_exp date DEFAULT '2222-12-31'::date,
    user_exp character varying,
    id_recurso integer,
    id_recurso_asignado integer DEFAULT 1,
    id_tipo_recurso integer
)
WITH (autovacuum_enabled=true);


ALTER TABLE f5.t_detalle_servicio OWNER TO postgres;

--
-- TOC entry 6524 (class 0 OID 0)
-- Dependencies: 624
-- Name: TABLE t_detalle_servicio; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON TABLE t_detalle_servicio IS 'Tabla que almacena los recursos de una pauta';


--
-- TOC entry 625 (class 1259 OID 153226)
-- Name: cantidades_recursos; Type: VIEW; Schema: f5; Owner: postgres
--

CREATE VIEW cantidades_recursos AS
SELECT t_detalle_servicio.id_recurso, t_detalle_servicio.id_pauta, count(t_detalle_servicio.id_recurso) AS cantidad FROM t_detalle_servicio WHERE ((t_detalle_servicio.fecha_exp = '2222-12-31'::date) AND (t_detalle_servicio.id_recurso_asignado = 1)) GROUP BY t_detalle_servicio.id_recurso, t_detalle_servicio.id_pauta ORDER BY t_detalle_servicio.id_pauta, t_detalle_servicio.id_recurso;


ALTER TABLE f5.cantidades_recursos OWNER TO postgres;

--
-- TOC entry 626 (class 1259 OID 153230)
-- Name: t_estatus_detalle_servicio; Type: TABLE; Schema: f5; Owner: postgres; Tablespace: 
--

CREATE TABLE t_estatus_detalle_servicio (
    id_estatus_detalle_servicio integer NOT NULL,
    id_pauta integer,
    id_estatus integer,
    id_detalle_servicio integer,
    user_exp character varying,
    fecha_exp date DEFAULT '2222-12-31'::date,
    fecha_reg date DEFAULT now(),
    id_recurso_asignado integer
)
WITH (autovacuum_enabled=true);


ALTER TABLE f5.t_estatus_detalle_servicio OWNER TO postgres;

--
-- TOC entry 6525 (class 0 OID 0)
-- Dependencies: 626
-- Name: TABLE t_estatus_detalle_servicio; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON TABLE t_estatus_detalle_servicio IS 'Tabla que almacena los historicos de los servicios relacionados a una pauta';


SET search_path = inventario, pg_catalog;

--
-- TOC entry 627 (class 1259 OID 153238)
-- Name: inventario; Type: TABLE; Schema: inventario; Owner: postgres; Tablespace: 
--

CREATE TABLE inventario (
    idinventario integer NOT NULL,
    idcaracteristica integer,
    idmaterialgeneral integer,
    valorinventariado text,
    fecha_exp timestamp without time zone
);


ALTER TABLE inventario.inventario OWNER TO postgres;

--
-- TOC entry 628 (class 1259 OID 153244)
-- Name: materialgeneral; Type: TABLE; Schema: inventario; Owner: postgres; Tablespace: 
--

CREATE TABLE materialgeneral (
    idtipomaterial integer,
    idtipoinventario integer,
    idmaterialgeneral integer NOT NULL,
    fecha_exp timestamp without time zone,
    cedula_registro integer,
    estado_seleccion character(2) DEFAULT 0,
    expiracion_seleccion text
);


ALTER TABLE inventario.materialgeneral OWNER TO postgres;

--
-- TOC entry 629 (class 1259 OID 153251)
-- Name: tipoinventario; Type: TABLE; Schema: inventario; Owner: postgres; Tablespace: 
--

CREATE TABLE tipoinventario (
    idtipoinventario integer NOT NULL,
    desctipoinventario text,
    id_fintipoinventario integer,
    id_gerencia integer,
    id_division integer
);


ALTER TABLE inventario.tipoinventario OWNER TO postgres;

--
-- TOC entry 630 (class 1259 OID 153257)
-- Name: tipomaterial; Type: TABLE; Schema: inventario; Owner: postgres; Tablespace: 
--

CREATE TABLE tipomaterial (
    idtipomaterial integer NOT NULL,
    desctipomaterial text,
    sigai_id integer
);


ALTER TABLE inventario.tipomaterial OWNER TO postgres;

--
-- TOC entry 631 (class 1259 OID 153263)
-- Name: materialtipomaterialinv; Type: VIEW; Schema: inventario; Owner: postgres
--

CREATE VIEW materialtipomaterialinv AS
SELECT materialgeneral.idtipomaterial, materialgeneral.idtipoinventario, inventario.valorinventariado, tipomaterial.desctipomaterial, tipoinventario.id_fintipoinventario FROM (((inventario LEFT JOIN materialgeneral ON ((inventario.idmaterialgeneral = materialgeneral.idmaterialgeneral))) LEFT JOIN tipomaterial ON ((materialgeneral.idtipomaterial = tipomaterial.idtipomaterial))) LEFT JOIN tipoinventario ON ((materialgeneral.idtipoinventario = tipoinventario.idtipoinventario))) WHERE ((inventario.idcaracteristica = 18) AND (materialgeneral.fecha_exp IS NULL)) GROUP BY materialgeneral.idtipomaterial, materialgeneral.idtipoinventario, inventario.valorinventariado, tipomaterial.desctipomaterial, tipoinventario.id_fintipoinventario ORDER BY inventario.valorinventariado, tipomaterial.desctipomaterial;


ALTER TABLE inventario.materialtipomaterialinv OWNER TO postgres;

--
-- TOC entry 1118 (class 1259 OID 189214)
-- Name: select_servicios; Type: VIEW; Schema: inventario; Owner: postgres
--

CREATE VIEW select_servicios AS
SELECT materialtipomaterialinv.idtipomaterial, concatena((materialtipomaterialinv.idtipoinventario)::text) AS idtiposinventario, concatena(upper(materialtipomaterialinv.valorinventariado)) AS valorinventariado, materialtipomaterialinv.desctipomaterial, materialtipomaterialinv.id_fintipoinventario FROM materialtipomaterialinv WHERE (materialtipomaterialinv.valorinventariado <> 'PERSONAL ADMINISTRATIVO'::text) GROUP BY materialtipomaterialinv.idtipomaterial, materialtipomaterialinv.desctipomaterial, materialtipomaterialinv.id_fintipoinventario ORDER BY materialtipomaterialinv.desctipomaterial;


ALTER TABLE inventario.select_servicios OWNER TO postgres;

SET search_path = f5, pg_catalog;

--
-- TOC entry 1119 (class 1259 OID 189218)
-- Name: cont_recur_por_estatus; Type: VIEW; Schema: f5; Owner: postgres
--

CREATE VIEW cont_recur_por_estatus AS
SELECT t_estatus_detalle_servicio.id_pauta, t_detalle_servicio.id_recurso, count(t_detalle_servicio.id_recurso) AS cantidad, select_servicios.id_fintipoinventario, t_estatus_detalle_servicio.id_estatus FROM ((t_estatus_detalle_servicio LEFT JOIN t_detalle_servicio ON ((t_estatus_detalle_servicio.id_detalle_servicio = t_detalle_servicio.id_detalle_servicio))) LEFT JOIN inventario.select_servicios ON ((select_servicios.idtipomaterial = t_detalle_servicio.id_recurso))) GROUP BY t_estatus_detalle_servicio.id_pauta, t_detalle_servicio.id_recurso, select_servicios.id_fintipoinventario, t_estatus_detalle_servicio.id_estatus ORDER BY t_estatus_detalle_servicio.id_pauta, t_detalle_servicio.id_recurso, select_servicios.id_fintipoinventario, t_estatus_detalle_servicio.id_estatus;


ALTER TABLE f5.cont_recur_por_estatus OWNER TO postgres;

--
-- TOC entry 1120 (class 1259 OID 189223)
-- Name: cont_recur_por_estatus_sol; Type: VIEW; Schema: f5; Owner: postgres
--

CREATE VIEW cont_recur_por_estatus_sol AS
SELECT t_estatus_detalle_servicio.id_pauta, t_detalle_servicio.id_recurso, count(t_detalle_servicio.id_recurso) AS cantidad, select_servicios.id_fintipoinventario FROM ((t_estatus_detalle_servicio LEFT JOIN t_detalle_servicio ON ((t_estatus_detalle_servicio.id_detalle_servicio = t_detalle_servicio.id_detalle_servicio))) LEFT JOIN inventario.select_servicios ON ((select_servicios.idtipomaterial = t_detalle_servicio.id_recurso))) WHERE ((t_estatus_detalle_servicio.id_estatus = ANY (ARRAY[13, 17, 20])) AND (t_detalle_servicio.fecha_exp = '2222-12-31'::date)) GROUP BY t_estatus_detalle_servicio.id_pauta, t_detalle_servicio.id_recurso, select_servicios.id_fintipoinventario ORDER BY t_estatus_detalle_servicio.id_pauta, t_detalle_servicio.id_recurso, select_servicios.id_fintipoinventario;


ALTER TABLE f5.cont_recur_por_estatus_sol OWNER TO postgres;

--
-- TOC entry 1121 (class 1259 OID 189228)
-- Name: cont_recur_por_estatus_sol_pord; Type: VIEW; Schema: f5; Owner: postgres
--

CREATE VIEW cont_recur_por_estatus_sol_pord AS
SELECT t_estatus_detalle_servicio.id_pauta, t_detalle_servicio.id_recurso, count(t_detalle_servicio.id_recurso) AS cantidad, select_servicios.id_fintipoinventario FROM ((t_estatus_detalle_servicio LEFT JOIN t_detalle_servicio ON ((t_estatus_detalle_servicio.id_detalle_servicio = t_detalle_servicio.id_detalle_servicio))) LEFT JOIN inventario.select_servicios ON ((select_servicios.idtipomaterial = t_detalle_servicio.id_recurso))) WHERE (t_estatus_detalle_servicio.id_estatus = ANY (ARRAY[13, 17, 20])) GROUP BY t_estatus_detalle_servicio.id_pauta, t_detalle_servicio.id_recurso, select_servicios.id_fintipoinventario ORDER BY t_estatus_detalle_servicio.id_pauta, t_detalle_servicio.id_recurso, select_servicios.id_fintipoinventario;


ALTER TABLE f5.cont_recur_por_estatus_sol_pord OWNER TO postgres;

--
-- TOC entry 632 (class 1259 OID 153268)
-- Name: contador; Type: VIEW; Schema: f5; Owner: postgres
--

CREATE VIEW contador AS
SELECT t_estatus_pauta.id_estatus, count(t_estatus_pauta.id_pauta) AS cantidad FROM t_estatus_pauta GROUP BY t_estatus_pauta.id_estatus ORDER BY t_estatus_pauta.id_estatus;


ALTER TABLE f5.contador OWNER TO postgres;

SET search_path = inventario, pg_catalog;

--
-- TOC entry 633 (class 1259 OID 153272)
-- Name: caracteristica; Type: TABLE; Schema: inventario; Owner: postgres; Tablespace: 
--

CREATE TABLE caracteristica (
    idcaracteristica integer NOT NULL,
    desccararacteristica text,
    idtipocaracteristica integer,
    cedula_registro integer
);


ALTER TABLE inventario.caracteristica OWNER TO postgres;

--
-- TOC entry 634 (class 1259 OID 153278)
-- Name: movimiento; Type: TABLE; Schema: inventario; Owner: postgres; Tablespace: 
--

CREATE TABLE movimiento (
    idmovimiento integer NOT NULL,
    idtipomovimiento integer,
    idmaterialgeneral integer,
    cedula_ejecutante integer,
    cedula_responsable integer,
    fecha_movimiento timestamp without time zone DEFAULT now(),
    fecha_exp timestamp without time zone,
    cedula_exp integer,
    caracteristicas_asignadas text,
    lugar text,
    evento text,
    idasignacion integer DEFAULT 0
);


ALTER TABLE inventario.movimiento OWNER TO postgres;

--
-- TOC entry 635 (class 1259 OID 153286)
-- Name: tipomovimiento; Type: TABLE; Schema: inventario; Owner: postgres; Tablespace: 
--

CREATE TABLE tipomovimiento (
    idtipomovimiento integer NOT NULL,
    desctipomovimiento text
);


ALTER TABLE inventario.tipomovimiento OWNER TO postgres;

--
-- TOC entry 1265 (class 1259 OID 195460)
-- Name: equipoasignado; Type: VIEW; Schema: inventario; Owner: postgres
--

CREATE VIEW equipoasignado AS
SELECT movimiento.idmaterialgeneral, materialgeneral.idtipoinventario, tipoinventario.desctipoinventario, tipomaterial.desctipomaterial, movimiento.caracteristicas_asignadas AS inventario_valor, ((((movimiento.caracteristicas_asignadas || ' | Responsable: '::text) || (datos_trabajador.nl_apelynom)::text) || ' '::text) || movimiento.cedula_responsable) AS busqueda, movimiento.idmovimiento, tipomovimiento.desctipomovimiento, movimiento.cedula_ejecutante, movimiento.cedula_responsable, movimiento.fecha_movimiento, datos_trabajador.nl_apelynom AS nombreresponsable, movimiento.idasignacion FROM (((((movimiento LEFT JOIN materialgeneral ON ((materialgeneral.idmaterialgeneral = movimiento.idmaterialgeneral))) LEFT JOIN sigai.datos_trabajador ON ((datos_trabajador.nu_documen = movimiento.cedula_responsable))) LEFT JOIN tipoinventario ON ((tipoinventario.idtipoinventario = materialgeneral.idtipoinventario))) LEFT JOIN tipomaterial ON ((tipomaterial.idtipomaterial = materialgeneral.idtipomaterial))) LEFT JOIN tipomovimiento ON ((tipomovimiento.idtipomovimiento = movimiento.idtipomovimiento))) WHERE (((movimiento.fecha_exp IS NULL) AND (movimiento.fecha_movimiento IS NOT NULL)) AND (materialgeneral.fecha_exp IS NULL));


ALTER TABLE inventario.equipoasignado OWNER TO postgres;

--
-- TOC entry 636 (class 1259 OID 153297)
-- Name: vista_inventario; Type: VIEW; Schema: inventario; Owner: postgres
--

CREATE VIEW vista_inventario AS
SELECT inventario.idinventario, inventario.idcaracteristica, inventario.idmaterialgeneral, inventario.valorinventariado FROM (inventario LEFT JOIN caracteristica ON ((caracteristica.idcaracteristica = inventario.idcaracteristica))) WHERE (inventario.fecha_exp IS NULL) ORDER BY inventario.idmaterialgeneral DESC, caracteristica.desccararacteristica;


ALTER TABLE inventario.vista_inventario OWNER TO postgres;

--
-- TOC entry 1122 (class 1259 OID 189233)
-- Name: vistageneral_inventario; Type: VIEW; Schema: inventario; Owner: postgres
--

CREATE VIEW vistageneral_inventario AS
SELECT materialgeneral.idmaterialgeneral, materialgeneral.idtipoinventario, tipoinventario.desctipoinventario, tipomaterial.desctipomaterial, concatena(((caracteristica.desccararacteristica || ':'::text) || vista_inventario.valorinventariado)) AS inventario_valor, concatena((((tipomaterial.desctipomaterial || ' '::text) || (caracteristica.desccararacteristica || ':'::text)) || vista_inventario.valorinventariado)) AS busqueda, materialgeneral.estado_seleccion, materialgeneral.expiracion_seleccion, tipomaterial.idtipomaterial, tipoinventario.id_gerencia, tipoinventario.id_division, tipoinventario.id_fintipoinventario FROM ((((vista_inventario RIGHT JOIN materialgeneral ON ((vista_inventario.idmaterialgeneral = materialgeneral.idmaterialgeneral))) LEFT JOIN tipoinventario ON ((tipoinventario.idtipoinventario = materialgeneral.idtipoinventario))) LEFT JOIN tipomaterial ON ((tipomaterial.idtipomaterial = materialgeneral.idtipomaterial))) LEFT JOIN caracteristica ON ((caracteristica.idcaracteristica = vista_inventario.idcaracteristica))) WHERE (materialgeneral.fecha_exp IS NULL) GROUP BY materialgeneral.idmaterialgeneral, materialgeneral.idtipoinventario, tipoinventario.desctipoinventario, tipomaterial.desctipomaterial, materialgeneral.estado_seleccion, materialgeneral.expiracion_seleccion, tipomaterial.idtipomaterial, tipoinventario.id_gerencia, tipoinventario.id_division, tipoinventario.id_fintipoinventario ORDER BY materialgeneral.idmaterialgeneral DESC;


ALTER TABLE inventario.vistageneral_inventario OWNER TO postgres;

--
-- TOC entry 1266 (class 1259 OID 195465)
-- Name: inventariostok; Type: VIEW; Schema: inventario; Owner: postgres
--

CREATE VIEW inventariostok AS
SELECT vistageneral_inventario.idmaterialgeneral, vistageneral_inventario.idtipoinventario, vistageneral_inventario.desctipoinventario, vistageneral_inventario.desctipomaterial, vistageneral_inventario.inventario_valor, vistageneral_inventario.busqueda, vistageneral_inventario.idtipomaterial, vistageneral_inventario.id_gerencia, vistageneral_inventario.id_division, vistageneral_inventario.id_fintipoinventario FROM vistageneral_inventario WHERE ((vistageneral_inventario.inventario_valor IS NOT NULL) AND (NOT (vistageneral_inventario.idmaterialgeneral IN (SELECT equipoasignado.idmaterialgeneral FROM equipoasignado))));


ALTER TABLE inventario.inventariostok OWNER TO postgres;

SET search_path = f5, pg_catalog;

--
-- TOC entry 1268 (class 1259 OID 195474)
-- Name: datos_recurso_inv; Type: VIEW; Schema: f5; Owner: postgres
--

CREATE VIEW datos_recurso_inv AS
SELECT t_detalle_servicio.id_detalle_servicio, t_detalle_servicio.id_pauta, t_detalle_servicio.fecha_exp, t_detalle_servicio.id_recurso, t_detalle_servicio.id_recurso_asignado, materialtipomaterialinv.desctipomaterial, inventariostok.inventario_valor, inventariostok.id_gerencia, inventariostok.id_division, inventariostok.id_fintipoinventario FROM ((t_detalle_servicio LEFT JOIN inventario.materialtipomaterialinv ON ((t_detalle_servicio.id_recurso = materialtipomaterialinv.idtipomaterial))) LEFT JOIN inventario.inventariostok ON ((t_detalle_servicio.id_recurso_asignado = inventariostok.idmaterialgeneral))) GROUP BY t_detalle_servicio.id_detalle_servicio, t_detalle_servicio.id_pauta, t_detalle_servicio.fecha_exp, t_detalle_servicio.id_recurso, t_detalle_servicio.id_recurso_asignado, materialtipomaterialinv.desctipomaterial, inventariostok.inventario_valor, inventariostok.id_gerencia, inventariostok.id_division, inventariostok.id_fintipoinventario ORDER BY materialtipomaterialinv.desctipomaterial;


ALTER TABLE f5.datos_recurso_inv OWNER TO postgres;

--
-- TOC entry 637 (class 1259 OID 153301)
-- Name: t_citacion; Type: TABLE; Schema: f5; Owner: postgres; Tablespace: 
--

CREATE TABLE t_citacion (
    id_citacion integer NOT NULL,
    fecha date,
    hora time without time zone,
    id_lugar integer NOT NULL
)
WITH (autovacuum_enabled=true);


ALTER TABLE f5.t_citacion OWNER TO postgres;

--
-- TOC entry 6526 (class 0 OID 0)
-- Dependencies: 637
-- Name: TABLE t_citacion; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON TABLE t_citacion IS 'Tabla que almacena los distintos tipos de citacion de la pauta';


--
-- TOC entry 6527 (class 0 OID 0)
-- Dependencies: 637
-- Name: COLUMN t_citacion.id_citacion; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON COLUMN t_citacion.id_citacion IS 'Campo identificacion de la tabla t_citacion';


--
-- TOC entry 6528 (class 0 OID 0)
-- Dependencies: 637
-- Name: COLUMN t_citacion.fecha; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON COLUMN t_citacion.fecha IS 'Fecha de la citacion';


--
-- TOC entry 6529 (class 0 OID 0)
-- Dependencies: 637
-- Name: COLUMN t_citacion.hora; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON COLUMN t_citacion.hora IS 'Hora de la citacion';


--
-- TOC entry 6530 (class 0 OID 0)
-- Dependencies: 637
-- Name: COLUMN t_citacion.id_lugar; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON COLUMN t_citacion.id_lugar IS 'lugar donde se realizara la citacion ';


--
-- TOC entry 638 (class 1259 OID 153304)
-- Name: t_emision_grabacion; Type: TABLE; Schema: f5; Owner: postgres; Tablespace: 
--

CREATE TABLE t_emision_grabacion (
    id_emision_grabacion integer NOT NULL,
    fecha date,
    hora time without time zone
)
WITH (autovacuum_enabled=true);


ALTER TABLE f5.t_emision_grabacion OWNER TO postgres;

--
-- TOC entry 6531 (class 0 OID 0)
-- Dependencies: 638
-- Name: TABLE t_emision_grabacion; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON TABLE t_emision_grabacion IS 'Tabla que almacena la fecha de la emision o grabacion de una pauta';


--
-- TOC entry 639 (class 1259 OID 153307)
-- Name: t_estatus; Type: TABLE; Schema: f5; Owner: postgres; Tablespace: 
--

CREATE TABLE t_estatus (
    id_estatus integer NOT NULL,
    descripcion character varying,
    id_tipo_estatus integer
);


ALTER TABLE f5.t_estatus OWNER TO postgres;

--
-- TOC entry 6532 (class 0 OID 0)
-- Dependencies: 639
-- Name: TABLE t_estatus; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON TABLE t_estatus IS 'Tabla que almacena los tipos de estatus para la pauta y para el servicio';


--
-- TOC entry 6533 (class 0 OID 0)
-- Dependencies: 639
-- Name: COLUMN t_estatus.id_estatus; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON COLUMN t_estatus.id_estatus IS 'Campo identificativo de la tabla estatus';


--
-- TOC entry 6534 (class 0 OID 0)
-- Dependencies: 639
-- Name: COLUMN t_estatus.descripcion; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON COLUMN t_estatus.descripcion IS 'Descripcion del estatus';


--
-- TOC entry 6535 (class 0 OID 0)
-- Dependencies: 639
-- Name: COLUMN t_estatus.id_tipo_estatus; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON COLUMN t_estatus.id_tipo_estatus IS 'Id del tipo de estatus al que va hacer refeencia esta descripcion';


--
-- TOC entry 640 (class 1259 OID 153313)
-- Name: t_locacion; Type: TABLE; Schema: f5; Owner: postgres; Tablespace: 
--

CREATE TABLE t_locacion (
    id_locacion integer NOT NULL,
    descripcion character varying,
    CONSTRAINT t_locacion_descripcion_check CHECK ((((descripcion)::text = 'REMOTO'::text) OR ((descripcion)::text = 'ESTUDIO'::text)))
);


ALTER TABLE f5.t_locacion OWNER TO postgres;

--
-- TOC entry 6536 (class 0 OID 0)
-- Dependencies: 640
-- Name: TABLE t_locacion; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON TABLE t_locacion IS 'Tabla que almacena las locaciones de la pauta';


--
-- TOC entry 6537 (class 0 OID 0)
-- Dependencies: 640
-- Name: COLUMN t_locacion.id_locacion; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON COLUMN t_locacion.id_locacion IS 'Campo identificativo de la tabla locacion';


--
-- TOC entry 6538 (class 0 OID 0)
-- Dependencies: 640
-- Name: COLUMN t_locacion.descripcion; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON COLUMN t_locacion.descripcion IS 'Descripcion de la locacion';


--
-- TOC entry 641 (class 1259 OID 153320)
-- Name: t_lugar; Type: TABLE; Schema: f5; Owner: postgres; Tablespace: 
--

CREATE TABLE t_lugar (
    id_lugar integer NOT NULL,
    descripcion character varying,
    CONSTRAINT t_lugar_descripcion_check CHECK ((((((((descripcion)::text = 'VTV'::text) OR ((descripcion)::text = 'EN SITIO'::text)) OR ((descripcion)::text = 'ESTUDIO 1'::text)) OR ((descripcion)::text = 'ESTUDIO 2'::text)) OR ((descripcion)::text = 'ESTUDIO 3'::text)) OR ((descripcion)::text = 'ESTUDIO 4'::text)))
)
WITH (autovacuum_enabled=true);


ALTER TABLE f5.t_lugar OWNER TO postgres;

--
-- TOC entry 6539 (class 0 OID 0)
-- Dependencies: 641
-- Name: TABLE t_lugar; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON TABLE t_lugar IS 'Tabla que almacena los distintos lugares de la citacion para la pauta';


--
-- TOC entry 6540 (class 0 OID 0)
-- Dependencies: 641
-- Name: COLUMN t_lugar.id_lugar; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON COLUMN t_lugar.id_lugar IS 'Campo identificativo de la tabla lugar';


--
-- TOC entry 6541 (class 0 OID 0)
-- Dependencies: 641
-- Name: COLUMN t_lugar.descripcion; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON COLUMN t_lugar.descripcion IS 'Descripcion del lugar';


--
-- TOC entry 642 (class 1259 OID 153327)
-- Name: t_montaje; Type: TABLE; Schema: f5; Owner: postgres; Tablespace: 
--

CREATE TABLE t_montaje (
    id_montaje integer NOT NULL,
    fecha date,
    hora time without time zone
);


ALTER TABLE f5.t_montaje OWNER TO postgres;

--
-- TOC entry 6542 (class 0 OID 0)
-- Dependencies: 642
-- Name: TABLE t_montaje; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON TABLE t_montaje IS 'Tabla donde se almacenan los montajes de la pauta';


--
-- TOC entry 6543 (class 0 OID 0)
-- Dependencies: 642
-- Name: COLUMN t_montaje.id_montaje; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON COLUMN t_montaje.id_montaje IS 'Campo identificativo de la tabla montaje';


--
-- TOC entry 6544 (class 0 OID 0)
-- Dependencies: 642
-- Name: COLUMN t_montaje.fecha; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON COLUMN t_montaje.fecha IS 'Fecha a realizarse el montaje de la pauta';


--
-- TOC entry 6545 (class 0 OID 0)
-- Dependencies: 642
-- Name: COLUMN t_montaje.hora; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON COLUMN t_montaje.hora IS 'Hora a realizarse el montaje de la pauta';


--
-- TOC entry 643 (class 1259 OID 153330)
-- Name: t_pauta; Type: TABLE; Schema: f5; Owner: postgres; Tablespace: 
--

CREATE TABLE t_pauta (
    id_pauta integer NOT NULL,
    fecha_reg date DEFAULT now(),
    fecha_exp date DEFAULT '2222-12-31'::date,
    user_reg character varying,
    user_exp character varying,
    id_tipo_pauta integer NOT NULL,
    id_locacion integer NOT NULL,
    id_tipo_traje integer NOT NULL,
    id_program integer NOT NULL,
    id_citacion integer NOT NULL,
    id_montaje integer NOT NULL,
    id_emision_grabacion integer NOT NULL,
    id_retorno integer NOT NULL,
    id_tipo_evento integer NOT NULL,
    lugar character varying,
    descripcion character varying
)
WITH (autovacuum_enabled=true);


ALTER TABLE f5.t_pauta OWNER TO postgres;

--
-- TOC entry 6546 (class 0 OID 0)
-- Dependencies: 643
-- Name: TABLE t_pauta; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON TABLE t_pauta IS 'Tabla donde se almacenan todos los datos relacionados a la pauta';


--
-- TOC entry 6547 (class 0 OID 0)
-- Dependencies: 643
-- Name: COLUMN t_pauta.id_pauta; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON COLUMN t_pauta.id_pauta IS 'Campo identificativo de la tabla pauta';


--
-- TOC entry 6548 (class 0 OID 0)
-- Dependencies: 643
-- Name: COLUMN t_pauta.fecha_reg; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON COLUMN t_pauta.fecha_reg IS 'Fecha en la se reliza el registro de la pauta';


--
-- TOC entry 6549 (class 0 OID 0)
-- Dependencies: 643
-- Name: COLUMN t_pauta.fecha_exp; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON COLUMN t_pauta.fecha_exp IS 'Fecha en la que se realiza el eliminado logico de la pauta ';


--
-- TOC entry 6550 (class 0 OID 0)
-- Dependencies: 643
-- Name: COLUMN t_pauta.user_reg; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON COLUMN t_pauta.user_reg IS 'Usuario que hace el registro de la pauta';


--
-- TOC entry 6551 (class 0 OID 0)
-- Dependencies: 643
-- Name: COLUMN t_pauta.user_exp; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON COLUMN t_pauta.user_exp IS 'Usuario que realiza el eliminado logico de la pauta';


--
-- TOC entry 6552 (class 0 OID 0)
-- Dependencies: 643
-- Name: COLUMN t_pauta.id_tipo_pauta; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON COLUMN t_pauta.id_tipo_pauta IS 'id del tipo de pauta para la pauta';


--
-- TOC entry 6553 (class 0 OID 0)
-- Dependencies: 643
-- Name: COLUMN t_pauta.id_locacion; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON COLUMN t_pauta.id_locacion IS 'id de la locacion de la pauta';


--
-- TOC entry 6554 (class 0 OID 0)
-- Dependencies: 643
-- Name: COLUMN t_pauta.id_tipo_traje; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON COLUMN t_pauta.id_tipo_traje IS 'id del tipo de traje para la pauta';


--
-- TOC entry 6555 (class 0 OID 0)
-- Dependencies: 643
-- Name: COLUMN t_pauta.id_program; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON COLUMN t_pauta.id_program IS 'id del programa al que pertenece la pauta';


--
-- TOC entry 6556 (class 0 OID 0)
-- Dependencies: 643
-- Name: COLUMN t_pauta.id_citacion; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON COLUMN t_pauta.id_citacion IS 'id de la citacion para la pauta';


--
-- TOC entry 6557 (class 0 OID 0)
-- Dependencies: 643
-- Name: COLUMN t_pauta.id_montaje; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON COLUMN t_pauta.id_montaje IS 'id del montaje para la pauta';


--
-- TOC entry 6558 (class 0 OID 0)
-- Dependencies: 643
-- Name: COLUMN t_pauta.id_emision_grabacion; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON COLUMN t_pauta.id_emision_grabacion IS 'id de la emision de la grabacion ';


--
-- TOC entry 6559 (class 0 OID 0)
-- Dependencies: 643
-- Name: COLUMN t_pauta.id_retorno; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON COLUMN t_pauta.id_retorno IS 'id del retorno de la pauta';


--
-- TOC entry 6560 (class 0 OID 0)
-- Dependencies: 643
-- Name: COLUMN t_pauta.id_tipo_evento; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON COLUMN t_pauta.id_tipo_evento IS 'id del tipo de evento que sera la pauta';


--
-- TOC entry 6561 (class 0 OID 0)
-- Dependencies: 643
-- Name: COLUMN t_pauta.lugar; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON COLUMN t_pauta.lugar IS 'Descricpcion del lugar donde se llevara a cabo la pauta';


--
-- TOC entry 6562 (class 0 OID 0)
-- Dependencies: 643
-- Name: COLUMN t_pauta.descripcion; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON COLUMN t_pauta.descripcion IS 'Descripcion de la pauta';


--
-- TOC entry 644 (class 1259 OID 153338)
-- Name: t_programa; Type: TABLE; Schema: f5; Owner: postgres; Tablespace: 
--

CREATE TABLE t_programa (
    id_program integer NOT NULL,
    descripcion character varying,
    fecha_exp date DEFAULT '2222-12-31'::date
);


ALTER TABLE f5.t_programa OWNER TO postgres;

--
-- TOC entry 6563 (class 0 OID 0)
-- Dependencies: 644
-- Name: TABLE t_programa; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON TABLE t_programa IS 'Tabla en la que se almacena los distintos programas en los que puede hacer o a los que corresponde una pauta';


--
-- TOC entry 6564 (class 0 OID 0)
-- Dependencies: 644
-- Name: COLUMN t_programa.id_program; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON COLUMN t_programa.id_program IS 'Campo identificativo de la tabla programa';


--
-- TOC entry 6565 (class 0 OID 0)
-- Dependencies: 644
-- Name: COLUMN t_programa.descripcion; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON COLUMN t_programa.descripcion IS 'Descripcion del nombre del programa ';


--
-- TOC entry 645 (class 1259 OID 153345)
-- Name: t_retorno; Type: TABLE; Schema: f5; Owner: postgres; Tablespace: 
--

CREATE TABLE t_retorno (
    id_retorno integer NOT NULL,
    fecha date,
    hora time without time zone
);


ALTER TABLE f5.t_retorno OWNER TO postgres;

--
-- TOC entry 6566 (class 0 OID 0)
-- Dependencies: 645
-- Name: TABLE t_retorno; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON TABLE t_retorno IS 'Tabla donde se almacena los datos del retorno de la pauta';


--
-- TOC entry 6567 (class 0 OID 0)
-- Dependencies: 645
-- Name: COLUMN t_retorno.id_retorno; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON COLUMN t_retorno.id_retorno IS 'Campo identificativo de la tabla retorno';


--
-- TOC entry 6568 (class 0 OID 0)
-- Dependencies: 645
-- Name: COLUMN t_retorno.fecha; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON COLUMN t_retorno.fecha IS 'Fecha del retorno de la pauta';


--
-- TOC entry 6569 (class 0 OID 0)
-- Dependencies: 645
-- Name: COLUMN t_retorno.hora; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON COLUMN t_retorno.hora IS 'Hora del retorno de la pauta';


--
-- TOC entry 646 (class 1259 OID 153348)
-- Name: t_tipo_estatus; Type: TABLE; Schema: f5; Owner: postgres; Tablespace: 
--

CREATE TABLE t_tipo_estatus (
    id_tipo_estatus integer NOT NULL,
    descripcion character varying,
    CONSTRAINT t_tipo_estatus_descripcion_check CHECK ((((descripcion)::text = 'DE PAUTA'::text) OR ((descripcion)::text = 'DE SERVICIO'::text)))
);


ALTER TABLE f5.t_tipo_estatus OWNER TO postgres;

--
-- TOC entry 6570 (class 0 OID 0)
-- Dependencies: 646
-- Name: TABLE t_tipo_estatus; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON TABLE t_tipo_estatus IS 'Tabla que almacena a que tabla se refiere el tipo de estatus';


--
-- TOC entry 6571 (class 0 OID 0)
-- Dependencies: 646
-- Name: COLUMN t_tipo_estatus.id_tipo_estatus; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON COLUMN t_tipo_estatus.id_tipo_estatus IS 'Campo identificativo de la tabla tipo_estatus';


--
-- TOC entry 6572 (class 0 OID 0)
-- Dependencies: 646
-- Name: COLUMN t_tipo_estatus.descripcion; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON COLUMN t_tipo_estatus.descripcion IS 'Descricpcion del tipo de estatus';


--
-- TOC entry 647 (class 1259 OID 153355)
-- Name: t_tipo_evento; Type: TABLE; Schema: f5; Owner: postgres; Tablespace: 
--

CREATE TABLE t_tipo_evento (
    id_tipo_evento integer NOT NULL,
    descripcion character varying,
    CONSTRAINT t_tipo_evento_descripcion_check CHECK ((((descripcion)::text = 'EN VIVO'::text) OR ((descripcion)::text = 'GRABADO'::text)))
);


ALTER TABLE f5.t_tipo_evento OWNER TO postgres;

--
-- TOC entry 6573 (class 0 OID 0)
-- Dependencies: 647
-- Name: TABLE t_tipo_evento; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON TABLE t_tipo_evento IS 'Tabla que almacena los distintos tipos de eventos para la pauta';


--
-- TOC entry 6574 (class 0 OID 0)
-- Dependencies: 647
-- Name: COLUMN t_tipo_evento.id_tipo_evento; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON COLUMN t_tipo_evento.id_tipo_evento IS 'Campo identificativo de la tabla tipo_evento';


--
-- TOC entry 6575 (class 0 OID 0)
-- Dependencies: 647
-- Name: COLUMN t_tipo_evento.descripcion; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON COLUMN t_tipo_evento.descripcion IS 'Descripcion del tipo de evento';


--
-- TOC entry 648 (class 1259 OID 153362)
-- Name: t_tipo_pauta; Type: TABLE; Schema: f5; Owner: postgres; Tablespace: 
--

CREATE TABLE t_tipo_pauta (
    id_tipo_pauta integer NOT NULL,
    descripcion character varying,
    CONSTRAINT chk_tipo_pauta CHECK ((((descripcion)::text = 'PROGRAMA'::text) OR ((descripcion)::text = 'EVENTO'::text)))
);


ALTER TABLE f5.t_tipo_pauta OWNER TO postgres;

--
-- TOC entry 6576 (class 0 OID 0)
-- Dependencies: 648
-- Name: TABLE t_tipo_pauta; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON TABLE t_tipo_pauta IS 'Tabla que almcena los distintos tipos de pauta';


--
-- TOC entry 6577 (class 0 OID 0)
-- Dependencies: 648
-- Name: COLUMN t_tipo_pauta.id_tipo_pauta; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON COLUMN t_tipo_pauta.id_tipo_pauta IS 'Campo identificativo de la tabla tipo_pauta';


--
-- TOC entry 6578 (class 0 OID 0)
-- Dependencies: 648
-- Name: COLUMN t_tipo_pauta.descripcion; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON COLUMN t_tipo_pauta.descripcion IS 'Descripcion del tipo de pauta';


--
-- TOC entry 649 (class 1259 OID 153369)
-- Name: t_tipo_traje; Type: TABLE; Schema: f5; Owner: postgres; Tablespace: 
--

CREATE TABLE t_tipo_traje (
    id_tipo_traje integer NOT NULL,
    descripcion character varying,
    fecha_exp date DEFAULT '2222-12-31'::date
);


ALTER TABLE f5.t_tipo_traje OWNER TO postgres;

--
-- TOC entry 6579 (class 0 OID 0)
-- Dependencies: 649
-- Name: TABLE t_tipo_traje; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON TABLE t_tipo_traje IS 'Tabla que almacena los distintos tipos de traje que se utilizaran para la pauta';


--
-- TOC entry 6580 (class 0 OID 0)
-- Dependencies: 649
-- Name: COLUMN t_tipo_traje.id_tipo_traje; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON COLUMN t_tipo_traje.id_tipo_traje IS 'Campo identificativo de la tabla tipo_traje';


--
-- TOC entry 6581 (class 0 OID 0)
-- Dependencies: 649
-- Name: COLUMN t_tipo_traje.descripcion; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON COLUMN t_tipo_traje.descripcion IS 'Descripcion del tipo de traje';


--
-- TOC entry 650 (class 1259 OID 153376)
-- Name: sys_main; Type: VIEW; Schema: f5; Owner: postgres
--

CREATE VIEW sys_main AS
SELECT a.id_pauta, a.lugar, a.descripcion, a.user_reg AS productor, k.descripcion AS tipo_pauta, m.descripcion AS tipo_evento, d.descripcion AS nombre_programa, x.descripcion AS pauta_locacion, j.descripcion AS pauta_traje, n0.descripcion AS citacion_lugar, g.fecha AS fecha_citacion, g.hora AS hora_citacion, h.fecha AS fecha_retorno, h.hora AS hora_retorno, i.fecha AS fecha_montaje, i.hora AS hora_montaje, c.fecha AS fecha_emision, c.hora AS hora_emision, f.descripcion AS pauta_estatus, CASE WHEN ((a.descripcion)::text = ''::text) THEN ((((((((((((((((((((((((((((((((((a.id_pauta || ' | '::text) || (a.lugar)::text) || ' | '::text) || (a.user_reg)::text) || ' | '::text) || (k.descripcion)::text) || ' | '::text) || (m.descripcion)::text) || ' | '::text) || (d.descripcion)::text) || ' | '::text) || (x.descripcion)::text) || ' | '::text) || (j.descripcion)::text) || ' | '::text) || (n0.descripcion)::text) || ' | '::text) || g.fecha) || ' | '::text) || g.hora) || ' | '::text) || h.fecha) || ' | '::text) || h.hora) || ' | '::text) || i.fecha) || ' | '::text) || i.hora) || ' | '::text) || c.fecha) || ' | '::text) || c.hora) || ' | '::text) || (f.descripcion)::text) ELSE ((((((((((((((((((((((((((((((((((a.id_pauta || ' | '::text) || (a.lugar)::text) || ' | '::text) || (a.descripcion)::text) || ' | '::text) || (a.user_reg)::text) || ' | '::text) || (k.descripcion)::text) || ' | '::text) || (m.descripcion)::text) || ' | '::text) || (x.descripcion)::text) || ' | '::text) || (j.descripcion)::text) || ' | '::text) || (n0.descripcion)::text) || ' | '::text) || g.fecha) || ' | '::text) || g.hora) || ' | '::text) || h.fecha) || ' | '::text) || h.hora) || ' | '::text) || i.fecha) || ' | '::text) || i.hora) || ' | '::text) || c.fecha) || ' | '::text) || c.hora) || ' | '::text) || (f.descripcion)::text) END AS busqueda, f.id_estatus FROM (((((((((((((t_pauta a LEFT JOIN t_estatus_pauta b USING (id_pauta)) LEFT JOIN t_emision_grabacion c USING (id_emision_grabacion)) LEFT JOIN t_programa d USING (id_program)) LEFT JOIN t_estatus f USING (id_estatus)) LEFT JOIN t_citacion g USING (id_citacion)) LEFT JOIN t_retorno h USING (id_retorno)) LEFT JOIN t_montaje i USING (id_montaje)) LEFT JOIN t_tipo_traje j USING (id_tipo_traje)) LEFT JOIN t_tipo_pauta k USING (id_tipo_pauta)) LEFT JOIN t_tipo_evento m USING (id_tipo_evento)) LEFT JOIN t_tipo_estatus l USING (id_tipo_estatus)) LEFT JOIN t_locacion x USING (id_locacion)) LEFT JOIN t_lugar n0 ON ((n0.id_lugar = g.id_lugar))) WHERE (b.fecha_exp = '2222-12-31'::date) ORDER BY a.id_pauta DESC;


ALTER TABLE f5.sys_main OWNER TO postgres;

SET search_path = inventario, pg_catalog;

--
-- TOC entry 651 (class 1259 OID 153381)
-- Name: tipomaterial_gerencia_div; Type: VIEW; Schema: inventario; Owner: postgres
--

CREATE VIEW tipomaterial_gerencia_div AS
SELECT materialtipomaterialinv.idtipoinventario, materialtipomaterialinv.idtipomaterial, tipoinventario.id_gerencia, tipoinventario.id_division FROM (materialtipomaterialinv LEFT JOIN tipoinventario USING (idtipoinventario)) ORDER BY materialtipomaterialinv.idtipomaterial;


ALTER TABLE inventario.tipomaterial_gerencia_div OWNER TO postgres;

SET search_path = f5, pg_catalog;

--
-- TOC entry 1269 (class 1259 OID 195479)
-- Name: lista_de_pautas_por_gerencia; Type: VIEW; Schema: f5; Owner: postgres
--

CREATE VIEW lista_de_pautas_por_gerencia AS
SELECT sys_main.id_pauta, sys_main.descripcion, sys_main.tipo_pauta, sys_main.nombre_programa, sys_main.pauta_estatus, sys_main.id_estatus, tipomaterial_gerencia_div.id_gerencia, sys_main.busqueda, grlgerencia.de_gerencia, sys_main.fecha_citacion, sys_main.fecha_emision, sys_main.fecha_montaje, sys_main.fecha_retorno FROM (((sys_main sys_main LEFT JOIN t_detalle_servicio USING (id_pauta)) LEFT JOIN inventario.tipomaterial_gerencia_div ON ((t_detalle_servicio.id_recurso = tipomaterial_gerencia_div.idtipomaterial))) LEFT JOIN sigai.grlgerencia ON ((tipomaterial_gerencia_div.id_gerencia = grlgerencia.id_gerencia))) WHERE (t_detalle_servicio.user_exp IS NULL) GROUP BY sys_main.id_pauta, sys_main.descripcion, sys_main.tipo_pauta, sys_main.nombre_programa, sys_main.pauta_estatus, sys_main.id_estatus, tipomaterial_gerencia_div.id_gerencia, sys_main.busqueda, grlgerencia.de_gerencia, sys_main.fecha_citacion, sys_main.fecha_emision, sys_main.fecha_montaje, sys_main.fecha_retorno ORDER BY sys_main.id_pauta;


ALTER TABLE f5.lista_de_pautas_por_gerencia OWNER TO postgres;

--
-- TOC entry 1264 (class 1259 OID 195455)
-- Name: usuarios; Type: VIEW; Schema: f5; Owner: postgres
--

CREATE VIEW usuarios AS
SELECT t_datos_personales.cedula, t_datos_personales.nombre_usuario, t_datos_personales.apellido_usuario, t_tipo_usuario.desc_usuario, t_aplicacion.nombre AS aplicacion, t_aplicacion.version, t_modulo.nombre AS modulo, ((((((((((t_datos_personales.cedula || ' '::text) || (t_datos_personales.nombre_usuario)::text) || ' '::text) || (t_datos_personales.apellido_usuario)::text) || ' '::text) || (t_tipo_usuario.desc_usuario)::text) || ' '::text) || (grlgerencia.de_gerencia)::text) || ' '::text) || (grldivision.de_division)::text) AS busqueda, t_acceso.idgerencia AS id_gerencia, t_acceso.iddivision, t_datos_personales.correo, t_acceso.id_tipo_usuario, grlgerencia.de_gerencia, grldivision.de_division FROM ((((((usuario.t_acceso LEFT JOIN usuario.t_aplicacion ON ((t_aplicacion.id_aplicacion = t_acceso.id_aplicacion))) LEFT JOIN usuario.t_datos_personales ON (((t_datos_personales.cedula)::text = (t_acceso.cedula)::text))) LEFT JOIN usuario.t_tipo_usuario ON ((t_tipo_usuario.id_tipo_usuario = t_acceso.id_tipo_usuario))) LEFT JOIN usuario.t_modulo ON ((t_modulo.id_modulo = t_acceso.id_modulo))) LEFT JOIN sigai.grlgerencia ON ((grlgerencia.id_gerencia = t_acceso.idgerencia))) LEFT JOIN sigai.grldivision ON ((grldivision.id_division = t_acceso.iddivision))) WHERE ((t_acceso.fecha_exp = '2222-12-31'::date) AND (t_acceso.id_aplicacion = 12)) ORDER BY t_datos_personales.cedula;


ALTER TABLE f5.usuarios OWNER TO postgres;

--
-- TOC entry 1270 (class 1259 OID 195484)
-- Name: lista_de_pautas_por_gerencias; Type: VIEW; Schema: f5; Owner: postgres
--

CREATE VIEW lista_de_pautas_por_gerencias AS
SELECT sys_main.id_pauta, sys_main.descripcion, sys_main.tipo_pauta, sys_main.nombre_programa, sys_main.pauta_estatus, sys_main.id_estatus, tipomaterial_gerencia_div.id_gerencia, sys_main.busqueda, grlgerencia.de_gerencia AS gerencia_afectada, sys_main.fecha_citacion, sys_main.fecha_emision, sys_main.fecha_montaje, sys_main.fecha_retorno, sys_main.productor, grlpersona.de_corto AS nom_prod, usuarios.de_gerencia AS gerencia_prod FROM (((((sys_main sys_main LEFT JOIN t_detalle_servicio USING (id_pauta)) LEFT JOIN inventario.tipomaterial_gerencia_div ON ((t_detalle_servicio.id_recurso = tipomaterial_gerencia_div.idtipomaterial))) LEFT JOIN sigai.grlgerencia ON ((tipomaterial_gerencia_div.id_gerencia = grlgerencia.id_gerencia))) LEFT JOIN sigai.grlpersona ON (((sys_main.productor)::text = (grlpersona.nu_documen)::text))) LEFT JOIN usuarios ON (((sys_main.productor)::text = (usuarios.cedula)::text))) WHERE ((t_detalle_servicio.user_exp IS NULL) AND (sys_main.id_estatus = ANY (ARRAY[25, 26, 36]))) GROUP BY sys_main.id_pauta, sys_main.descripcion, sys_main.tipo_pauta, sys_main.nombre_programa, sys_main.pauta_estatus, sys_main.id_estatus, tipomaterial_gerencia_div.id_gerencia, sys_main.busqueda, grlgerencia.de_gerencia, sys_main.fecha_citacion, sys_main.fecha_emision, sys_main.fecha_montaje, sys_main.fecha_retorno, sys_main.productor, grlpersona.de_corto, usuarios.de_gerencia ORDER BY sys_main.id_pauta;


ALTER TABLE f5.lista_de_pautas_por_gerencias OWNER TO postgres;

--
-- TOC entry 1271 (class 1259 OID 195489)
-- Name: lista_pauta_gerencia; Type: VIEW; Schema: f5; Owner: postgres
--

CREATE VIEW lista_pauta_gerencia AS
SELECT a.id_pauta, a.tipo_pauta, a.nombre_programa, a.pauta_estatus, a.id_estatus, a.productor, a.busqueda, a.descripcion, c.id_gerencia, c.iddivision FROM (((sys_main a LEFT JOIN usuarios c ON ((c.cedula = (a.productor)::integer))) LEFT JOIN t_detalle_servicio b ON ((b.id_pauta = a.id_pauta))) LEFT JOIN inventario.inventariostok d ON ((d.idmaterialgeneral = b.id_recurso_asignado))) WHERE (b.user_exp IS NULL) GROUP BY a.id_pauta, a.tipo_pauta, a.nombre_programa, a.pauta_estatus, a.id_estatus, a.productor, a.busqueda, a.descripcion, c.id_gerencia, c.iddivision ORDER BY a.id_pauta DESC;


ALTER TABLE f5.lista_pauta_gerencia OWNER TO postgres;

--
-- TOC entry 1272 (class 1259 OID 195494)
-- Name: lista_pauta_gerencia_ao; Type: VIEW; Schema: f5; Owner: postgres
--

CREATE VIEW lista_pauta_gerencia_ao AS
SELECT sys_main.id_pauta, sys_main.descripcion, sys_main.tipo_pauta, sys_main.nombre_programa, sys_main.pauta_estatus, sys_main.id_estatus, inventariostok.id_gerencia, sys_main.busqueda FROM ((sys_main sys_main LEFT JOIN t_detalle_servicio USING (id_pauta)) LEFT JOIN inventario.inventariostok ON ((t_detalle_servicio.id_recurso_asignado = inventariostok.idmaterialgeneral))) WHERE (t_detalle_servicio.user_exp IS NULL) GROUP BY sys_main.id_pauta, sys_main.descripcion, sys_main.tipo_pauta, sys_main.nombre_programa, sys_main.pauta_estatus, sys_main.id_estatus, inventariostok.id_gerencia, sys_main.busqueda ORDER BY sys_main.id_pauta;


ALTER TABLE f5.lista_pauta_gerencia_ao OWNER TO postgres;

--
-- TOC entry 654 (class 1259 OID 153408)
-- Name: sys_main_old; Type: VIEW; Schema: f5; Owner: postgres
--

CREATE VIEW sys_main_old AS
SELECT a.id_pauta, a.user_reg AS productor, k.descripcion AS tipo_pauta, m.descripcion AS tipo_evento, d.descripcion AS nombre_programa, x.descripcion AS pauta_locacion, j.descripcion AS pauta_traje, n0.descripcion AS citacion_lugar, g.fecha AS fecha_citacion, g.hora AS hora_citacion, h.fecha AS fecha_retorno, h.hora AS hora_retorno, i.fecha AS fecha_montaje, i.hora AS hora_montaje, c.fecha AS fecha_emision, c.hora AS hora_emision, f.descripcion AS pauta_estatus, CASE WHEN ((k.descripcion)::text = 'remoto'::text) THEN ((((((((((((((((((((((((((((((((a.id_pauta || ' | '::text) || (a.user_reg)::text) || ' | '::text) || (k.descripcion)::text) || ' | '::text) || (m.descripcion)::text) || ' | '::text) || (d.descripcion)::text) || ' | '::text) || (x.descripcion)::text) || ' | '::text) || (j.descripcion)::text) || ' | '::text) || (n0.descripcion)::text) || ' | '::text) || g.fecha) || ' | '::text) || g.hora) || ' | '::text) || h.fecha) || ' | '::text) || h.hora) || ' | '::text) || i.fecha) || ' | '::text) || i.hora) || ' | '::text) || c.fecha) || ' | '::text) || c.hora) || ' | '::text) || (f.descripcion)::text) ELSE ((((((((((((((((((((a.id_pauta || ' | '::text) || (a.user_reg)::text) || ' | '::text) || (k.descripcion)::text) || ' | '::text) || (m.descripcion)::text) || ' | '::text) || (d.descripcion)::text) || ' | '::text) || (x.descripcion)::text) || ' | '::text) || (j.descripcion)::text) || ' | '::text) || (n0.descripcion)::text) || ' | '::text) || g.fecha) || ' | '::text) || g.hora) || ' | '::text) || (f.descripcion)::text) END AS busqueda, f.id_estatus FROM (((((((((((((t_pauta a LEFT JOIN t_estatus_pauta b USING (id_pauta)) LEFT JOIN t_emision_grabacion c USING (id_emision_grabacion)) LEFT JOIN t_programa d USING (id_program)) LEFT JOIN t_estatus f USING (id_estatus)) LEFT JOIN t_citacion g USING (id_citacion)) LEFT JOIN t_retorno h USING (id_retorno)) LEFT JOIN t_montaje i USING (id_montaje)) LEFT JOIN t_tipo_traje j USING (id_tipo_traje)) LEFT JOIN t_tipo_pauta k USING (id_tipo_pauta)) LEFT JOIN t_tipo_evento m USING (id_tipo_evento)) LEFT JOIN t_tipo_estatus l USING (id_tipo_estatus)) LEFT JOIN t_locacion x USING (id_locacion)) LEFT JOIN t_lugar n0 ON ((n0.id_lugar = g.id_lugar))) WHERE (b.fecha_exp = '2222-12-31'::date) ORDER BY a.id_pauta DESC;


ALTER TABLE f5.sys_main_old OWNER TO postgres;

--
-- TOC entry 1273 (class 1259 OID 195499)
-- Name: lista_pauta_gerencia_ao_old; Type: VIEW; Schema: f5; Owner: postgres
--

CREATE VIEW lista_pauta_gerencia_ao_old AS
SELECT sys_main.id_pauta, sys_main.tipo_pauta, sys_main.nombre_programa, sys_main.pauta_estatus, sys_main.id_estatus, inventariostok.id_gerencia, sys_main.busqueda FROM ((sys_main_old sys_main LEFT JOIN t_detalle_servicio USING (id_pauta)) LEFT JOIN inventario.inventariostok ON ((t_detalle_servicio.id_recurso_asignado = inventariostok.idmaterialgeneral))) WHERE (t_detalle_servicio.user_exp IS NULL) GROUP BY sys_main.id_pauta, sys_main.tipo_pauta, sys_main.nombre_programa, sys_main.pauta_estatus, sys_main.id_estatus, inventariostok.id_gerencia, sys_main.busqueda ORDER BY sys_main.id_pauta;


ALTER TABLE f5.lista_pauta_gerencia_ao_old OWNER TO postgres;

--
-- TOC entry 1274 (class 1259 OID 195504)
-- Name: lista_pauta_gerencia_ao_rep; Type: VIEW; Schema: f5; Owner: postgres
--

CREATE VIEW lista_pauta_gerencia_ao_rep AS
SELECT sys_main.id_pauta, sys_main.descripcion, sys_main.tipo_pauta, sys_main.nombre_programa, sys_main.pauta_estatus, sys_main.id_estatus, inventariostok.id_gerencia, sys_main.busqueda FROM ((sys_main sys_main LEFT JOIN t_detalle_servicio USING (id_pauta)) LEFT JOIN inventario.inventariostok ON ((t_detalle_servicio.id_recurso_asignado = inventariostok.idmaterialgeneral))) WHERE ((t_detalle_servicio.user_exp IS NULL) AND (sys_main.id_estatus = ANY (ARRAY[5, 6, 7, 8, 9, 10, 21, 22, 24, 25, 26, 29, 32]))) GROUP BY sys_main.id_pauta, sys_main.descripcion, sys_main.tipo_pauta, sys_main.nombre_programa, sys_main.pauta_estatus, sys_main.id_estatus, inventariostok.id_gerencia, sys_main.busqueda ORDER BY sys_main.id_pauta;


ALTER TABLE f5.lista_pauta_gerencia_ao_rep OWNER TO postgres;

--
-- TOC entry 655 (class 1259 OID 153413)
-- Name: lista_pauta_gerencia_ao_sinasignacion; Type: VIEW; Schema: f5; Owner: postgres
--

CREATE VIEW lista_pauta_gerencia_ao_sinasignacion AS
SELECT sys_main.id_pauta, sys_main.descripcion, sys_main.tipo_pauta, sys_main.nombre_programa, sys_main.pauta_estatus, sys_main.id_estatus, tipomaterial_gerencia_div.id_gerencia, tipomaterial_gerencia_div.id_division, sys_main.busqueda FROM ((sys_main sys_main LEFT JOIN t_detalle_servicio USING (id_pauta)) LEFT JOIN inventario.tipomaterial_gerencia_div ON ((t_detalle_servicio.id_recurso = tipomaterial_gerencia_div.idtipomaterial))) WHERE (t_detalle_servicio.user_exp IS NULL) GROUP BY sys_main.id_pauta, sys_main.descripcion, sys_main.tipo_pauta, sys_main.nombre_programa, sys_main.pauta_estatus, sys_main.id_estatus, tipomaterial_gerencia_div.id_gerencia, tipomaterial_gerencia_div.id_division, sys_main.busqueda ORDER BY sys_main.id_pauta;


ALTER TABLE f5.lista_pauta_gerencia_ao_sinasignacion OWNER TO postgres;

--
-- TOC entry 1275 (class 1259 OID 195510)
-- Name: lista_pauta_gerencia_old; Type: VIEW; Schema: f5; Owner: postgres
--

CREATE VIEW lista_pauta_gerencia_old AS
SELECT sys_main.id_pauta, sys_main.tipo_pauta, sys_main.nombre_programa, sys_main.pauta_estatus, sys_main.id_estatus, sys_main.productor, datos_trabajador.id_gerencia, sys_main.busqueda, sys_main.descripcion FROM (((sys_main LEFT JOIN t_detalle_servicio USING (id_pauta)) LEFT JOIN inventario.inventariostok ON ((t_detalle_servicio.id_recurso_asignado = inventariostok.idmaterialgeneral))) LEFT JOIN sigai.datos_trabajador ON ((datos_trabajador.nu_documen = (sys_main.productor)::integer))) WHERE (t_detalle_servicio.user_exp IS NULL) GROUP BY sys_main.id_pauta, sys_main.tipo_pauta, sys_main.nombre_programa, sys_main.pauta_estatus, sys_main.id_estatus, sys_main.productor, datos_trabajador.id_gerencia, sys_main.busqueda, sys_main.descripcion;


ALTER TABLE f5.lista_pauta_gerencia_old OWNER TO postgres;

--
-- TOC entry 1267 (class 1259 OID 195469)
-- Name: lista_pauta_gerencia_rep; Type: VIEW; Schema: f5; Owner: postgres
--

CREATE VIEW lista_pauta_gerencia_rep AS
SELECT a.id_pauta, a.tipo_pauta, a.nombre_programa, a.pauta_estatus, a.id_estatus, a.productor, a.busqueda, a.descripcion, c.id_gerencia, c.iddivision FROM (((sys_main a LEFT JOIN usuarios c ON ((c.cedula = (a.productor)::integer))) LEFT JOIN t_detalle_servicio b ON ((b.id_pauta = a.id_pauta))) LEFT JOIN inventario.inventariostok d ON ((d.idmaterialgeneral = b.id_recurso_asignado))) WHERE ((b.user_exp IS NULL) AND (a.id_estatus = ANY (ARRAY[5, 6, 7, 8, 9, 10, 21, 22, 24, 25, 26, 29, 32]))) GROUP BY a.id_pauta, a.tipo_pauta, a.nombre_programa, a.pauta_estatus, a.id_estatus, a.productor, a.busqueda, a.descripcion, c.id_gerencia, c.iddivision ORDER BY a.id_pauta DESC;


ALTER TABLE f5.lista_pauta_gerencia_rep OWNER TO postgres;

--
-- TOC entry 1276 (class 1259 OID 195516)
-- Name: lista_recursos_por_gerencia; Type: VIEW; Schema: f5; Owner: postgres
--

CREATE VIEW lista_recursos_por_gerencia AS
SELECT sys_main.id_pauta, sys_main.descripcion, sys_main.tipo_pauta, sys_main.nombre_programa, sys_main.pauta_estatus, sys_main.id_estatus, tipomaterial_gerencia_div.id_gerencia, sys_main.busqueda, t_detalle_servicio.id_recurso, grlgerencia.de_gerencia FROM (((sys_main sys_main LEFT JOIN t_detalle_servicio USING (id_pauta)) LEFT JOIN inventario.tipomaterial_gerencia_div ON ((t_detalle_servicio.id_recurso = tipomaterial_gerencia_div.idtipomaterial))) LEFT JOIN sigai.grlgerencia ON ((tipomaterial_gerencia_div.id_gerencia = grlgerencia.id_gerencia))) WHERE (t_detalle_servicio.user_exp IS NULL) GROUP BY sys_main.id_pauta, sys_main.descripcion, sys_main.tipo_pauta, sys_main.nombre_programa, sys_main.pauta_estatus, sys_main.id_estatus, tipomaterial_gerencia_div.id_gerencia, sys_main.busqueda, t_detalle_servicio.id_recurso, grlgerencia.de_gerencia ORDER BY sys_main.id_pauta;


ALTER TABLE f5.lista_recursos_por_gerencia OWNER TO postgres;

--
-- TOC entry 1277 (class 1259 OID 195521)
-- Name: pauta_gerencia; Type: VIEW; Schema: f5; Owner: postgres
--

CREATE VIEW pauta_gerencia AS
SELECT sys_main.id_pauta, sys_main.tipo_pauta, sys_main.nombre_programa, sys_main.lugar, sys_main.descripcion, sys_main.pauta_estatus, sys_main.id_estatus, inventariostok.id_gerencia, inventariostok.id_division, sys_main.busqueda FROM ((sys_main LEFT JOIN t_detalle_servicio USING (id_pauta)) LEFT JOIN inventario.inventariostok ON ((t_detalle_servicio.id_recurso_asignado = inventariostok.idmaterialgeneral))) WHERE (t_detalle_servicio.user_exp IS NULL) GROUP BY sys_main.id_pauta, sys_main.tipo_pauta, sys_main.nombre_programa, sys_main.lugar, sys_main.descripcion, sys_main.pauta_estatus, sys_main.id_estatus, inventariostok.id_gerencia, inventariostok.id_division, sys_main.busqueda ORDER BY sys_main.id_pauta;


ALTER TABLE f5.pauta_gerencia OWNER TO postgres;

--
-- TOC entry 656 (class 1259 OID 153423)
-- Name: pautas_por_fechas; Type: VIEW; Schema: f5; Owner: postgres
--

CREATE VIEW pautas_por_fechas AS
SELECT t_estatus_pauta.id_estatus, t_estatus_pauta.fecha_reg, count(t_estatus_pauta.id_pauta) AS cantidad, t_estatus_pauta.id_pauta FROM t_estatus_pauta GROUP BY t_estatus_pauta.id_estatus, t_estatus_pauta.fecha_reg, t_estatus_pauta.id_pauta ORDER BY t_estatus_pauta.id_estatus;


ALTER TABLE f5.pautas_por_fechas OWNER TO postgres;

--
-- TOC entry 657 (class 1259 OID 153427)
-- Name: programa; Type: VIEW; Schema: f5; Owner: postgres
--

CREATE VIEW programa AS
SELECT t_programa.descripcion, (((t_programa.id_program)::text || ' '::text) || (t_programa.descripcion)::text) AS busqueda, t_programa.id_program, t_programa.fecha_exp FROM t_programa WHERE (t_programa.fecha_exp = '2222-12-31'::date) ORDER BY t_programa.descripcion;


ALTER TABLE f5.programa OWNER TO postgres;

--
-- TOC entry 658 (class 1259 OID 153431)
-- Name: recur_asig; Type: VIEW; Schema: f5; Owner: postgres
--

CREATE VIEW recur_asig AS
SELECT t_detalle_servicio.id_pauta, t_detalle_servicio.id_detalle_servicio, t_detalle_servicio.id_recurso_asignado, t_detalle_servicio.fecha_reg, t_detalle_servicio.user_reg AS user_exp FROM t_detalle_servicio WHERE ((t_detalle_servicio.id_recurso_asignado <> 1) AND (t_detalle_servicio.fecha_exp = '2222-12-31'::date)) GROUP BY t_detalle_servicio.id_pauta, t_detalle_servicio.id_detalle_servicio, t_detalle_servicio.id_recurso_asignado, t_detalle_servicio.fecha_reg, t_detalle_servicio.user_reg ORDER BY t_detalle_servicio.id_pauta, t_detalle_servicio.id_detalle_servicio, t_detalle_servicio.id_recurso_asignado, t_detalle_servicio.fecha_reg, t_detalle_servicio.user_reg;


ALTER TABLE f5.recur_asig OWNER TO postgres;

--
-- TOC entry 659 (class 1259 OID 153435)
-- Name: recur_elim_ual; Type: VIEW; Schema: f5; Owner: postgres
--

CREATE VIEW recur_elim_ual AS
SELECT t_estatus_detalle_servicio.id_pauta, t_detalle_servicio.id_recurso, count(t_detalle_servicio.id_recurso) AS cantidad FROM (t_estatus_detalle_servicio LEFT JOIN t_detalle_servicio ON ((t_estatus_detalle_servicio.id_detalle_servicio = t_detalle_servicio.id_detalle_servicio))) WHERE (t_estatus_detalle_servicio.id_estatus = 16) GROUP BY t_estatus_detalle_servicio.id_pauta, t_detalle_servicio.id_recurso ORDER BY t_estatus_detalle_servicio.id_pauta, t_detalle_servicio.id_recurso;


ALTER TABLE f5.recur_elim_ual OWNER TO postgres;

--
-- TOC entry 660 (class 1259 OID 153439)
-- Name: recur_sin_asig; Type: VIEW; Schema: f5; Owner: postgres
--

CREATE VIEW recur_sin_asig AS
SELECT t_detalle_servicio.id_pauta, t_detalle_servicio.id_recurso, count(t_detalle_servicio.id_recurso) AS cantidad FROM t_detalle_servicio WHERE ((t_detalle_servicio.id_recurso_asignado = 1) AND (t_detalle_servicio.fecha_exp = '2222-12-31'::date)) GROUP BY t_detalle_servicio.id_pauta, t_detalle_servicio.id_recurso ORDER BY t_detalle_servicio.id_pauta, t_detalle_servicio.id_recurso;


ALTER TABLE f5.recur_sin_asig OWNER TO postgres;

--
-- TOC entry 1123 (class 1259 OID 189282)
-- Name: recur_val_estatus; Type: VIEW; Schema: f5; Owner: postgres
--

CREATE VIEW recur_val_estatus AS
SELECT t_estatus_detalle_servicio.id_pauta, t_detalle_servicio.id_recurso, count(t_detalle_servicio.id_recurso) AS cantidad, select_servicios.id_fintipoinventario, t_estatus_detalle_servicio.id_estatus FROM ((t_estatus_detalle_servicio LEFT JOIN t_detalle_servicio ON ((t_estatus_detalle_servicio.id_detalle_servicio = t_detalle_servicio.id_detalle_servicio))) LEFT JOIN inventario.select_servicios ON ((select_servicios.idtipomaterial = t_detalle_servicio.id_recurso))) WHERE (t_estatus_detalle_servicio.fecha_exp = '2222-12-31'::date) GROUP BY t_estatus_detalle_servicio.id_pauta, t_detalle_servicio.id_recurso, select_servicios.id_fintipoinventario, t_estatus_detalle_servicio.id_estatus ORDER BY t_estatus_detalle_servicio.id_pauta, t_detalle_servicio.id_recurso, select_servicios.id_fintipoinventario, t_estatus_detalle_servicio.id_estatus;


ALTER TABLE f5.recur_val_estatus OWNER TO postgres;

--
-- TOC entry 661 (class 1259 OID 153443)
-- Name: recur_val_ger; Type: VIEW; Schema: f5; Owner: postgres
--

CREATE VIEW recur_val_ger AS
SELECT t_estatus_detalle_servicio.id_pauta, t_detalle_servicio.id_recurso, count(t_detalle_servicio.id_recurso) AS cantidad FROM (t_estatus_detalle_servicio LEFT JOIN t_detalle_servicio ON ((t_estatus_detalle_servicio.id_detalle_servicio = t_detalle_servicio.id_detalle_servicio))) WHERE ((t_estatus_detalle_servicio.id_estatus = ANY (ARRAY[13, 20])) OR (t_estatus_detalle_servicio.id_estatus = 13)) GROUP BY t_estatus_detalle_servicio.id_pauta, t_detalle_servicio.id_recurso ORDER BY t_estatus_detalle_servicio.id_pauta, t_detalle_servicio.id_recurso;


ALTER TABLE f5.recur_val_ger OWNER TO postgres;

--
-- TOC entry 1124 (class 1259 OID 189287)
-- Name: recur_val_jefejefe; Type: VIEW; Schema: f5; Owner: postgres
--

CREATE VIEW recur_val_jefejefe AS
SELECT t_estatus_detalle_servicio.id_pauta, t_detalle_servicio.id_recurso, count(t_detalle_servicio.id_recurso) AS cantidad, select_servicios.id_fintipoinventario FROM ((t_estatus_detalle_servicio LEFT JOIN t_detalle_servicio ON ((t_estatus_detalle_servicio.id_detalle_servicio = t_detalle_servicio.id_detalle_servicio))) LEFT JOIN inventario.select_servicios ON ((select_servicios.idtipomaterial = t_detalle_servicio.id_recurso))) WHERE ((t_estatus_detalle_servicio.id_estatus = 20) AND (t_estatus_detalle_servicio.fecha_exp = '2222-12-31'::date)) GROUP BY t_estatus_detalle_servicio.id_pauta, t_detalle_servicio.id_recurso, select_servicios.id_fintipoinventario ORDER BY t_estatus_detalle_servicio.id_pauta, t_detalle_servicio.id_recurso, select_servicios.id_fintipoinventario;


ALTER TABLE f5.recur_val_jefejefe OWNER TO postgres;

--
-- TOC entry 1125 (class 1259 OID 189292)
-- Name: recur_val_prodjefe; Type: VIEW; Schema: f5; Owner: postgres
--

CREATE VIEW recur_val_prodjefe AS
SELECT t_estatus_detalle_servicio.id_pauta, t_detalle_servicio.id_recurso, count(t_detalle_servicio.id_recurso) AS cantidad, select_servicios.id_fintipoinventario FROM ((t_estatus_detalle_servicio LEFT JOIN t_detalle_servicio ON ((t_estatus_detalle_servicio.id_detalle_servicio = t_detalle_servicio.id_detalle_servicio))) LEFT JOIN inventario.select_servicios ON ((select_servicios.idtipomaterial = t_detalle_servicio.id_recurso))) WHERE ((t_estatus_detalle_servicio.id_estatus = 13) AND (t_estatus_detalle_servicio.fecha_exp = '2222-12-31'::date)) GROUP BY t_estatus_detalle_servicio.id_pauta, t_detalle_servicio.id_recurso, select_servicios.id_fintipoinventario ORDER BY t_estatus_detalle_servicio.id_pauta, t_detalle_servicio.id_recurso, select_servicios.id_fintipoinventario;


ALTER TABLE f5.recur_val_prodjefe OWNER TO postgres;

--
-- TOC entry 662 (class 1259 OID 153447)
-- Name: recur_val_ual; Type: VIEW; Schema: f5; Owner: postgres
--

CREATE VIEW recur_val_ual AS
SELECT t_estatus_detalle_servicio.id_pauta, t_detalle_servicio.id_recurso, count(t_detalle_servicio.id_recurso) AS cantidad FROM (t_estatus_detalle_servicio LEFT JOIN t_detalle_servicio ON ((t_estatus_detalle_servicio.id_detalle_servicio = t_detalle_servicio.id_detalle_servicio))) WHERE (t_estatus_detalle_servicio.id_estatus = 14) GROUP BY t_estatus_detalle_servicio.id_pauta, t_detalle_servicio.id_recurso ORDER BY t_estatus_detalle_servicio.id_pauta, t_detalle_servicio.id_recurso;


ALTER TABLE f5.recur_val_ual OWNER TO postgres;

--
-- TOC entry 1126 (class 1259 OID 189297)
-- Name: selectlistmat_group; Type: VIEW; Schema: f5; Owner: postgres
--

CREATE VIEW selectlistmat_group AS
SELECT concatena((t_detalle_servicio.id_detalle_servicio)::text) AS ids_detalles_servicios, t_detalle_servicio.id_pauta, t_detalle_servicio.id_recurso, count(t_detalle_servicio.id_detalle_servicio) AS cantidad FROM t_detalle_servicio WHERE ((t_detalle_servicio.id_recurso_asignado = 1) AND (t_detalle_servicio.fecha_exp = '2222-12-31'::date)) GROUP BY t_detalle_servicio.id_pauta, t_detalle_servicio.id_recurso;


ALTER TABLE f5.selectlistmat_group OWNER TO postgres;

--
-- TOC entry 663 (class 1259 OID 153451)
-- Name: t_citacion_id_citacion_seq; Type: SEQUENCE; Schema: f5; Owner: postgres
--

CREATE SEQUENCE t_citacion_id_citacion_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE f5.t_citacion_id_citacion_seq OWNER TO postgres;

--
-- TOC entry 6582 (class 0 OID 0)
-- Dependencies: 663
-- Name: t_citacion_id_citacion_seq; Type: SEQUENCE OWNED BY; Schema: f5; Owner: postgres
--

ALTER SEQUENCE t_citacion_id_citacion_seq OWNED BY t_citacion.id_citacion;


--
-- TOC entry 664 (class 1259 OID 153453)
-- Name: t_correo; Type: TABLE; Schema: f5; Owner: postgres; Tablespace: 
--

CREATE TABLE t_correo (
    id_gerencia integer,
    id_division integer DEFAULT 0,
    correo character varying,
    user_reg character varying,
    fecha_reg date DEFAULT now(),
    fecha_exp date DEFAULT '2222-12-31'::date,
    id_correo integer NOT NULL
);


ALTER TABLE f5.t_correo OWNER TO postgres;

--
-- TOC entry 6583 (class 0 OID 0)
-- Dependencies: 664
-- Name: TABLE t_correo; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON TABLE t_correo IS 'Tabla que almacena los correos electronicos del personal involucrado en una pauta';


--
-- TOC entry 6584 (class 0 OID 0)
-- Dependencies: 664
-- Name: COLUMN t_correo.id_gerencia; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON COLUMN t_correo.id_gerencia IS 'Id de la gerencia a la que petenece la cuenta de correo registrada';


--
-- TOC entry 6585 (class 0 OID 0)
-- Dependencies: 664
-- Name: COLUMN t_correo.id_division; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON COLUMN t_correo.id_division IS 'Id de la division a la que petenece la cuenta de correo registrada';


--
-- TOC entry 6586 (class 0 OID 0)
-- Dependencies: 664
-- Name: COLUMN t_correo.correo; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON COLUMN t_correo.correo IS 'cuenta de correo';


--
-- TOC entry 6587 (class 0 OID 0)
-- Dependencies: 664
-- Name: COLUMN t_correo.user_reg; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON COLUMN t_correo.user_reg IS 'Usuario que registro la cuenta de correo en el sistema';


--
-- TOC entry 6588 (class 0 OID 0)
-- Dependencies: 664
-- Name: COLUMN t_correo.fecha_reg; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON COLUMN t_correo.fecha_reg IS 'Fecha en la que se registro la cuenta de correo en el sistema';


--
-- TOC entry 6589 (class 0 OID 0)
-- Dependencies: 664
-- Name: COLUMN t_correo.fecha_exp; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON COLUMN t_correo.fecha_exp IS 'Fecha en la que se realizo el eliminado logico de la cuenta de correo del sistema';


--
-- TOC entry 6590 (class 0 OID 0)
-- Dependencies: 664
-- Name: COLUMN t_correo.id_correo; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON COLUMN t_correo.id_correo IS 'Campo identificativo de la tabla correo';


--
-- TOC entry 665 (class 1259 OID 153462)
-- Name: t_correo_id_correo_seq; Type: SEQUENCE; Schema: f5; Owner: postgres
--

CREATE SEQUENCE t_correo_id_correo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE f5.t_correo_id_correo_seq OWNER TO postgres;

--
-- TOC entry 6591 (class 0 OID 0)
-- Dependencies: 665
-- Name: t_correo_id_correo_seq; Type: SEQUENCE OWNED BY; Schema: f5; Owner: postgres
--

ALTER SEQUENCE t_correo_id_correo_seq OWNED BY t_correo.id_correo;


--
-- TOC entry 666 (class 1259 OID 153464)
-- Name: t_datos_persona; Type: TABLE; Schema: f5; Owner: postgres; Tablespace: 
--

CREATE TABLE t_datos_persona (
    id_datos_persona integer NOT NULL,
    id_persona integer,
    telefono1 character varying,
    telefono2 character varying,
    correo character varying
);


ALTER TABLE f5.t_datos_persona OWNER TO postgres;

--
-- TOC entry 6592 (class 0 OID 0)
-- Dependencies: 666
-- Name: TABLE t_datos_persona; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON TABLE t_datos_persona IS 'Tabla donde se almacenan los datos de contacto de las personas relacionadas a la pauta. ';


--
-- TOC entry 6593 (class 0 OID 0)
-- Dependencies: 666
-- Name: COLUMN t_datos_persona.id_datos_persona; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON COLUMN t_datos_persona.id_datos_persona IS 'Campo identificativo de la tabla';


--
-- TOC entry 6594 (class 0 OID 0)
-- Dependencies: 666
-- Name: COLUMN t_datos_persona.id_persona; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON COLUMN t_datos_persona.id_persona IS 'Este es el id_persona proveniente de la tabla grlpersona del esquema sigai';


--
-- TOC entry 6595 (class 0 OID 0)
-- Dependencies: 666
-- Name: COLUMN t_datos_persona.telefono1; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON COLUMN t_datos_persona.telefono1 IS 'Numero de telefono de la persona';


--
-- TOC entry 6596 (class 0 OID 0)
-- Dependencies: 666
-- Name: COLUMN t_datos_persona.telefono2; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON COLUMN t_datos_persona.telefono2 IS 'Numero de telefono de la persona';


--
-- TOC entry 6597 (class 0 OID 0)
-- Dependencies: 666
-- Name: COLUMN t_datos_persona.correo; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON COLUMN t_datos_persona.correo IS 'Direccion electronica de la persona';


--
-- TOC entry 667 (class 1259 OID 153470)
-- Name: t_datos_persona_id_datos_persona_seq; Type: SEQUENCE; Schema: f5; Owner: postgres
--

CREATE SEQUENCE t_datos_persona_id_datos_persona_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE f5.t_datos_persona_id_datos_persona_seq OWNER TO postgres;

--
-- TOC entry 6598 (class 0 OID 0)
-- Dependencies: 667
-- Name: t_datos_persona_id_datos_persona_seq; Type: SEQUENCE OWNED BY; Schema: f5; Owner: postgres
--

ALTER SEQUENCE t_datos_persona_id_datos_persona_seq OWNED BY t_datos_persona.id_datos_persona;


--
-- TOC entry 668 (class 1259 OID 153472)
-- Name: t_detalle_servicio_id_detalle_servicio_seq; Type: SEQUENCE; Schema: f5; Owner: postgres
--

CREATE SEQUENCE t_detalle_servicio_id_detalle_servicio_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE f5.t_detalle_servicio_id_detalle_servicio_seq OWNER TO postgres;

--
-- TOC entry 6599 (class 0 OID 0)
-- Dependencies: 668
-- Name: t_detalle_servicio_id_detalle_servicio_seq; Type: SEQUENCE OWNED BY; Schema: f5; Owner: postgres
--

ALTER SEQUENCE t_detalle_servicio_id_detalle_servicio_seq OWNED BY t_detalle_servicio.id_detalle_servicio;


--
-- TOC entry 1127 (class 1259 OID 189301)
-- Name: t_detalle_servicio_mod2; Type: VIEW; Schema: f5; Owner: postgres
--

CREATE VIEW t_detalle_servicio_mod2 AS
SELECT t_detalle_servicio.id_detalle_servicio AS serial, t_detalle_servicio.id_pauta, t_detalle_servicio.fecha_reg, t_detalle_servicio.user_reg, t_detalle_servicio.fecha_exp, t_detalle_servicio.user_exp, t_detalle_servicio.id_recurso, t_detalle_servicio.id_recurso_asignado, vistageneral_inventario.id_fintipoinventario FROM (t_detalle_servicio LEFT JOIN inventario.vistageneral_inventario ON ((vistageneral_inventario.idmaterialgeneral = t_detalle_servicio.id_recurso_asignado)));


ALTER TABLE f5.t_detalle_servicio_mod2 OWNER TO postgres;

--
-- TOC entry 669 (class 1259 OID 153474)
-- Name: t_emision_grabacion_id_emision_grabacion_seq; Type: SEQUENCE; Schema: f5; Owner: postgres
--

CREATE SEQUENCE t_emision_grabacion_id_emision_grabacion_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE f5.t_emision_grabacion_id_emision_grabacion_seq OWNER TO postgres;

--
-- TOC entry 6600 (class 0 OID 0)
-- Dependencies: 669
-- Name: t_emision_grabacion_id_emision_grabacion_seq; Type: SEQUENCE OWNED BY; Schema: f5; Owner: postgres
--

ALTER SEQUENCE t_emision_grabacion_id_emision_grabacion_seq OWNED BY t_emision_grabacion.id_emision_grabacion;


--
-- TOC entry 670 (class 1259 OID 153476)
-- Name: t_estado_informe; Type: TABLE; Schema: f5; Owner: postgres; Tablespace: 
--

CREATE TABLE t_estado_informe (
    id_estado_informe integer NOT NULL,
    descripcion character varying,
    clasificacion character varying
);


ALTER TABLE f5.t_estado_informe OWNER TO postgres;

--
-- TOC entry 6601 (class 0 OID 0)
-- Dependencies: 670
-- Name: TABLE t_estado_informe; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON TABLE t_estado_informe IS 'Tabla que almacena los estados en los que se puede evaluar el informe en la tabla informe';


--
-- TOC entry 671 (class 1259 OID 153482)
-- Name: t_estado_informe_id_estado_informe_seq; Type: SEQUENCE; Schema: f5; Owner: postgres
--

CREATE SEQUENCE t_estado_informe_id_estado_informe_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE f5.t_estado_informe_id_estado_informe_seq OWNER TO postgres;

--
-- TOC entry 6602 (class 0 OID 0)
-- Dependencies: 671
-- Name: t_estado_informe_id_estado_informe_seq; Type: SEQUENCE OWNED BY; Schema: f5; Owner: postgres
--

ALTER SEQUENCE t_estado_informe_id_estado_informe_seq OWNED BY t_estado_informe.id_estado_informe;


--
-- TOC entry 672 (class 1259 OID 153484)
-- Name: t_estatus_detalle_servicio_id_estatus_detalle_servicio_seq; Type: SEQUENCE; Schema: f5; Owner: postgres
--

CREATE SEQUENCE t_estatus_detalle_servicio_id_estatus_detalle_servicio_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE f5.t_estatus_detalle_servicio_id_estatus_detalle_servicio_seq OWNER TO postgres;

--
-- TOC entry 6603 (class 0 OID 0)
-- Dependencies: 672
-- Name: t_estatus_detalle_servicio_id_estatus_detalle_servicio_seq; Type: SEQUENCE OWNED BY; Schema: f5; Owner: postgres
--

ALTER SEQUENCE t_estatus_detalle_servicio_id_estatus_detalle_servicio_seq OWNED BY t_estatus_detalle_servicio.id_estatus_detalle_servicio;


--
-- TOC entry 673 (class 1259 OID 153486)
-- Name: t_estatus_id_estatus_seq; Type: SEQUENCE; Schema: f5; Owner: postgres
--

CREATE SEQUENCE t_estatus_id_estatus_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE f5.t_estatus_id_estatus_seq OWNER TO postgres;

--
-- TOC entry 6604 (class 0 OID 0)
-- Dependencies: 673
-- Name: t_estatus_id_estatus_seq; Type: SEQUENCE OWNED BY; Schema: f5; Owner: postgres
--

ALTER SEQUENCE t_estatus_id_estatus_seq OWNED BY t_estatus.id_estatus;


--
-- TOC entry 674 (class 1259 OID 153488)
-- Name: t_estatus_pauta_id_estatus_pauta_seq; Type: SEQUENCE; Schema: f5; Owner: postgres
--

CREATE SEQUENCE t_estatus_pauta_id_estatus_pauta_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE f5.t_estatus_pauta_id_estatus_pauta_seq OWNER TO postgres;

--
-- TOC entry 6605 (class 0 OID 0)
-- Dependencies: 674
-- Name: t_estatus_pauta_id_estatus_pauta_seq; Type: SEQUENCE OWNED BY; Schema: f5; Owner: postgres
--

ALTER SEQUENCE t_estatus_pauta_id_estatus_pauta_seq OWNED BY t_estatus_pauta.id_estatus_pauta;


--
-- TOC entry 675 (class 1259 OID 153490)
-- Name: t_informe; Type: TABLE; Schema: f5; Owner: postgres; Tablespace: 
--

CREATE TABLE t_informe (
    id_informe integer NOT NULL,
    id_pauta integer,
    id_detalle_servicio integer,
    observaciones character varying,
    user_reg character varying,
    user_exp character varying,
    fecha_reg date,
    fecha_exp date,
    id_estado_informe integer
)
WITH (autovacuum_enabled=true);


ALTER TABLE f5.t_informe OWNER TO postgres;

--
-- TOC entry 6606 (class 0 OID 0)
-- Dependencies: 675
-- Name: TABLE t_informe; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON TABLE t_informe IS 'Tabla para almacenar los comentario y/o evaluacion de un servicio de una pauta';


--
-- TOC entry 676 (class 1259 OID 153496)
-- Name: t_informe_id_informe_seq; Type: SEQUENCE; Schema: f5; Owner: postgres
--

CREATE SEQUENCE t_informe_id_informe_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE f5.t_informe_id_informe_seq OWNER TO postgres;

--
-- TOC entry 6607 (class 0 OID 0)
-- Dependencies: 676
-- Name: t_informe_id_informe_seq; Type: SEQUENCE OWNED BY; Schema: f5; Owner: postgres
--

ALTER SEQUENCE t_informe_id_informe_seq OWNED BY t_informe.id_informe;


--
-- TOC entry 677 (class 1259 OID 153498)
-- Name: t_locacion_id_locacion_seq; Type: SEQUENCE; Schema: f5; Owner: postgres
--

CREATE SEQUENCE t_locacion_id_locacion_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE f5.t_locacion_id_locacion_seq OWNER TO postgres;

--
-- TOC entry 6608 (class 0 OID 0)
-- Dependencies: 677
-- Name: t_locacion_id_locacion_seq; Type: SEQUENCE OWNED BY; Schema: f5; Owner: postgres
--

ALTER SEQUENCE t_locacion_id_locacion_seq OWNED BY t_locacion.id_locacion;


--
-- TOC entry 678 (class 1259 OID 153500)
-- Name: t_lugar_id_lugar_seq; Type: SEQUENCE; Schema: f5; Owner: postgres
--

CREATE SEQUENCE t_lugar_id_lugar_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE f5.t_lugar_id_lugar_seq OWNER TO postgres;

--
-- TOC entry 6609 (class 0 OID 0)
-- Dependencies: 678
-- Name: t_lugar_id_lugar_seq; Type: SEQUENCE OWNED BY; Schema: f5; Owner: postgres
--

ALTER SEQUENCE t_lugar_id_lugar_seq OWNED BY t_lugar.id_lugar;


--
-- TOC entry 679 (class 1259 OID 153502)
-- Name: t_montaje_id_montaje_seq; Type: SEQUENCE; Schema: f5; Owner: postgres
--

CREATE SEQUENCE t_montaje_id_montaje_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE f5.t_montaje_id_montaje_seq OWNER TO postgres;

--
-- TOC entry 6610 (class 0 OID 0)
-- Dependencies: 679
-- Name: t_montaje_id_montaje_seq; Type: SEQUENCE OWNED BY; Schema: f5; Owner: postgres
--

ALTER SEQUENCE t_montaje_id_montaje_seq OWNED BY t_montaje.id_montaje;


--
-- TOC entry 680 (class 1259 OID 153504)
-- Name: t_pauta_id_pauta_seq; Type: SEQUENCE; Schema: f5; Owner: postgres
--

CREATE SEQUENCE t_pauta_id_pauta_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE f5.t_pauta_id_pauta_seq OWNER TO postgres;

--
-- TOC entry 6611 (class 0 OID 0)
-- Dependencies: 680
-- Name: t_pauta_id_pauta_seq; Type: SEQUENCE OWNED BY; Schema: f5; Owner: postgres
--

ALTER SEQUENCE t_pauta_id_pauta_seq OWNED BY t_pauta.id_pauta;


--
-- TOC entry 681 (class 1259 OID 153506)
-- Name: t_productor; Type: TABLE; Schema: f5; Owner: postgres; Tablespace: 
--

CREATE TABLE t_productor (
    id_productor integer NOT NULL,
    id_persona integer,
    fecha_reg date DEFAULT now(),
    user_reg character varying,
    fecha_exp date,
    user_exp character varying
);


ALTER TABLE f5.t_productor OWNER TO postgres;

--
-- TOC entry 6612 (class 0 OID 0)
-- Dependencies: 681
-- Name: TABLE t_productor; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON TABLE t_productor IS 'Tabla donde se almacenan los productores de las pautas';


--
-- TOC entry 6613 (class 0 OID 0)
-- Dependencies: 681
-- Name: COLUMN t_productor.id_productor; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON COLUMN t_productor.id_productor IS 'Campo identificativo de la tabla productor';


--
-- TOC entry 6614 (class 0 OID 0)
-- Dependencies: 681
-- Name: COLUMN t_productor.id_persona; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON COLUMN t_productor.id_persona IS 'id  de la persona que va hacer registrada como productor, este id_persona proviene de la tabla rglpersona de sigai';


--
-- TOC entry 6615 (class 0 OID 0)
-- Dependencies: 681
-- Name: COLUMN t_productor.fecha_reg; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON COLUMN t_productor.fecha_reg IS 'Fecha en la que se creo el registro';


--
-- TOC entry 6616 (class 0 OID 0)
-- Dependencies: 681
-- Name: COLUMN t_productor.user_reg; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON COLUMN t_productor.user_reg IS 'Usuario que creo el registro en la tabla';


--
-- TOC entry 6617 (class 0 OID 0)
-- Dependencies: 681
-- Name: COLUMN t_productor.fecha_exp; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON COLUMN t_productor.fecha_exp IS 'Fecha en la que se realizo el eliminado logico del registro';


--
-- TOC entry 6618 (class 0 OID 0)
-- Dependencies: 681
-- Name: COLUMN t_productor.user_exp; Type: COMMENT; Schema: f5; Owner: postgres
--

COMMENT ON COLUMN t_productor.user_exp IS 'Usuario que realizo el eliminado logico del registro';


--
-- TOC entry 682 (class 1259 OID 153513)
-- Name: t_productor_id_productor_seq; Type: SEQUENCE; Schema: f5; Owner: postgres
--

CREATE SEQUENCE t_productor_id_productor_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE f5.t_productor_id_productor_seq OWNER TO postgres;

--
-- TOC entry 6619 (class 0 OID 0)
-- Dependencies: 682
-- Name: t_productor_id_productor_seq; Type: SEQUENCE OWNED BY; Schema: f5; Owner: postgres
--

ALTER SEQUENCE t_productor_id_productor_seq OWNED BY t_productor.id_productor;


--
-- TOC entry 683 (class 1259 OID 153515)
-- Name: t_programa_id_program_seq; Type: SEQUENCE; Schema: f5; Owner: postgres
--

CREATE SEQUENCE t_programa_id_program_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE f5.t_programa_id_program_seq OWNER TO postgres;

--
-- TOC entry 6620 (class 0 OID 0)
-- Dependencies: 683
-- Name: t_programa_id_program_seq; Type: SEQUENCE OWNED BY; Schema: f5; Owner: postgres
--

ALTER SEQUENCE t_programa_id_program_seq OWNED BY t_programa.id_program;


--
-- TOC entry 684 (class 1259 OID 153517)
-- Name: t_retorno_id_retorno_seq; Type: SEQUENCE; Schema: f5; Owner: postgres
--

CREATE SEQUENCE t_retorno_id_retorno_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE f5.t_retorno_id_retorno_seq OWNER TO postgres;

--
-- TOC entry 6621 (class 0 OID 0)
-- Dependencies: 684
-- Name: t_retorno_id_retorno_seq; Type: SEQUENCE OWNED BY; Schema: f5; Owner: postgres
--

ALTER SEQUENCE t_retorno_id_retorno_seq OWNED BY t_retorno.id_retorno;


--
-- TOC entry 685 (class 1259 OID 153519)
-- Name: t_tipo_estatus_id_tipo_estatus_seq; Type: SEQUENCE; Schema: f5; Owner: postgres
--

CREATE SEQUENCE t_tipo_estatus_id_tipo_estatus_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE f5.t_tipo_estatus_id_tipo_estatus_seq OWNER TO postgres;

--
-- TOC entry 6622 (class 0 OID 0)
-- Dependencies: 685
-- Name: t_tipo_estatus_id_tipo_estatus_seq; Type: SEQUENCE OWNED BY; Schema: f5; Owner: postgres
--

ALTER SEQUENCE t_tipo_estatus_id_tipo_estatus_seq OWNED BY t_tipo_estatus.id_tipo_estatus;


--
-- TOC entry 686 (class 1259 OID 153521)
-- Name: t_tipo_evento_id_tipo_evento_seq; Type: SEQUENCE; Schema: f5; Owner: postgres
--

CREATE SEQUENCE t_tipo_evento_id_tipo_evento_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE f5.t_tipo_evento_id_tipo_evento_seq OWNER TO postgres;

--
-- TOC entry 6623 (class 0 OID 0)
-- Dependencies: 686
-- Name: t_tipo_evento_id_tipo_evento_seq; Type: SEQUENCE OWNED BY; Schema: f5; Owner: postgres
--

ALTER SEQUENCE t_tipo_evento_id_tipo_evento_seq OWNED BY t_tipo_evento.id_tipo_evento;


--
-- TOC entry 687 (class 1259 OID 153523)
-- Name: t_tipo_pauta_id_tipo_pauta_seq; Type: SEQUENCE; Schema: f5; Owner: postgres
--

CREATE SEQUENCE t_tipo_pauta_id_tipo_pauta_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE f5.t_tipo_pauta_id_tipo_pauta_seq OWNER TO postgres;

--
-- TOC entry 6624 (class 0 OID 0)
-- Dependencies: 687
-- Name: t_tipo_pauta_id_tipo_pauta_seq; Type: SEQUENCE OWNED BY; Schema: f5; Owner: postgres
--

ALTER SEQUENCE t_tipo_pauta_id_tipo_pauta_seq OWNED BY t_tipo_pauta.id_tipo_pauta;


--
-- TOC entry 688 (class 1259 OID 153525)
-- Name: t_tipo_traje_id_tipo_traje_seq; Type: SEQUENCE; Schema: f5; Owner: postgres
--

CREATE SEQUENCE t_tipo_traje_id_tipo_traje_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE f5.t_tipo_traje_id_tipo_traje_seq OWNER TO postgres;

--
-- TOC entry 6625 (class 0 OID 0)
-- Dependencies: 688
-- Name: t_tipo_traje_id_tipo_traje_seq; Type: SEQUENCE OWNED BY; Schema: f5; Owner: postgres
--

ALTER SEQUENCE t_tipo_traje_id_tipo_traje_seq OWNED BY t_tipo_traje.id_tipo_traje;


--
-- TOC entry 689 (class 1259 OID 153527)
-- Name: trajes; Type: VIEW; Schema: f5; Owner: postgres
--

CREATE VIEW trajes AS
SELECT t_tipo_traje.descripcion, (((t_tipo_traje.id_tipo_traje)::text || ' '::text) || (t_tipo_traje.descripcion)::text) AS busqueda, t_tipo_traje.id_tipo_traje, t_tipo_traje.fecha_exp FROM t_tipo_traje WHERE (t_tipo_traje.fecha_exp = '2222-12-31'::date) ORDER BY t_tipo_traje.descripcion;


ALTER TABLE f5.trajes OWNER TO postgres;

SET search_path = inventario, pg_catalog;

--
-- TOC entry 778 (class 1259 OID 153974)
-- Name: Raul - Listado de Material por Tipo de Material; Type: VIEW; Schema: inventario; Owner: postgres
--

CREATE VIEW "Raul - Listado de Material por Tipo de Material" AS
SELECT materialgeneral.idtipomaterial, tipomaterial.desctipomaterial, materialgeneral.idmaterialgeneral, inventario.idinventario, inventario.idcaracteristica, inventario.valorinventariado FROM ((inventario JOIN materialgeneral ON ((inventario.idmaterialgeneral = materialgeneral.idmaterialgeneral))) JOIN tipomaterial ON ((tipomaterial.idtipomaterial = materialgeneral.idtipomaterial))) WHERE ((inventario.idcaracteristica = 18) AND (inventario.valorinventariado = 'Personal Operativo'::text)) ORDER BY materialgeneral.idtipomaterial;


ALTER TABLE inventario."Raul - Listado de Material por Tipo de Material" OWNER TO postgres;

--
-- TOC entry 779 (class 1259 OID 153979)
-- Name: asignacion; Type: TABLE; Schema: inventario; Owner: postgres; Tablespace: 
--

CREATE TABLE asignacion (
    idasignacion integer NOT NULL,
    descasignaccion text,
    cedula_registro integer,
    fe_registro timestamp without time zone DEFAULT now(),
    fe_expiracion timestamp without time zone,
    idtipoinventario integer,
    lugar text,
    evento text
);


ALTER TABLE inventario.asignacion OWNER TO postgres;

--
-- TOC entry 780 (class 1259 OID 153986)
-- Name: asignacion_idasignacion_seq; Type: SEQUENCE; Schema: inventario; Owner: postgres
--

CREATE SEQUENCE asignacion_idasignacion_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE inventario.asignacion_idasignacion_seq OWNER TO postgres;

--
-- TOC entry 6626 (class 0 OID 0)
-- Dependencies: 780
-- Name: asignacion_idasignacion_seq; Type: SEQUENCE OWNED BY; Schema: inventario; Owner: postgres
--

ALTER SEQUENCE asignacion_idasignacion_seq OWNED BY asignacion.idasignacion;


--
-- TOC entry 781 (class 1259 OID 153993)
-- Name: caract_tipomaterial; Type: VIEW; Schema: inventario; Owner: postgres
--

CREATE VIEW caract_tipomaterial AS
SELECT materialgeneral.idtipomaterial, lower(tipomaterial.desctipomaterial) AS tipomaterialmaneg, materialgeneral.idtipoinventario FROM ((inventario JOIN materialgeneral ON ((inventario.idmaterialgeneral = materialgeneral.idmaterialgeneral))) JOIN tipomaterial ON ((tipomaterial.idtipomaterial = materialgeneral.idtipomaterial))) WHERE (materialgeneral.fecha_exp IS NULL) GROUP BY materialgeneral.idtipomaterial, tipomaterial.desctipomaterial, materialgeneral.idtipoinventario ORDER BY tipomaterial.desctipomaterial;


ALTER TABLE inventario.caract_tipomaterial OWNER TO postgres;

--
-- TOC entry 782 (class 1259 OID 153997)
-- Name: caract_usuario; Type: VIEW; Schema: inventario; Owner: postgres
--

CREATE VIEW caract_usuario AS
SELECT inventario.idcaracteristica, lower(inventario.valorinventariado) AS valor, materialgeneral.idtipoinventario FROM (inventario JOIN materialgeneral ON ((inventario.idmaterialgeneral = materialgeneral.idmaterialgeneral))) WHERE (inventario.idcaracteristica = 6) GROUP BY inventario.idcaracteristica, inventario.valorinventariado, materialgeneral.idtipoinventario;


ALTER TABLE inventario.caract_usuario OWNER TO postgres;

--
-- TOC entry 783 (class 1259 OID 154001)
-- Name: caracteristica_idcaracteristica_seq; Type: SEQUENCE; Schema: inventario; Owner: postgres
--

CREATE SEQUENCE caracteristica_idcaracteristica_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE inventario.caracteristica_idcaracteristica_seq OWNER TO postgres;

--
-- TOC entry 6627 (class 0 OID 0)
-- Dependencies: 783
-- Name: caracteristica_idcaracteristica_seq; Type: SEQUENCE OWNED BY; Schema: inventario; Owner: postgres
--

ALTER SEQUENCE caracteristica_idcaracteristica_seq OWNED BY caracteristica.idcaracteristica;


--
-- TOC entry 784 (class 1259 OID 154003)
-- Name: comentario; Type: TABLE; Schema: inventario; Owner: postgres; Tablespace: 
--

CREATE TABLE comentario (
    idcomentario integer NOT NULL,
    comentario text,
    idmaterialgeneral integer,
    fe_registro timestamp without time zone,
    cedula_registro integer
);


ALTER TABLE inventario.comentario OWNER TO postgres;

--
-- TOC entry 785 (class 1259 OID 154009)
-- Name: comentarios_idcomentario_seq; Type: SEQUENCE; Schema: inventario; Owner: postgres
--

CREATE SEQUENCE comentarios_idcomentario_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE inventario.comentarios_idcomentario_seq OWNER TO postgres;

--
-- TOC entry 6628 (class 0 OID 0)
-- Dependencies: 785
-- Name: comentarios_idcomentario_seq; Type: SEQUENCE OWNED BY; Schema: inventario; Owner: postgres
--

ALTER SEQUENCE comentarios_idcomentario_seq OWNED BY comentario.idcomentario;


--
-- TOC entry 786 (class 1259 OID 154016)
-- Name: evento; Type: TABLE; Schema: inventario; Owner: postgres; Tablespace: 
--

CREATE TABLE evento (
    idevento integer NOT NULL,
    desevento text,
    cedula_registro integer,
    fe_expiracion timestamp without time zone DEFAULT '2222-12-31 00:00:00'::timestamp without time zone,
    fe_registro timestamp without time zone DEFAULT now()
);


ALTER TABLE inventario.evento OWNER TO postgres;

--
-- TOC entry 787 (class 1259 OID 154024)
-- Name: evento_idevento_seq; Type: SEQUENCE; Schema: inventario; Owner: postgres
--

CREATE SEQUENCE evento_idevento_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE inventario.evento_idevento_seq OWNER TO postgres;

--
-- TOC entry 6629 (class 0 OID 0)
-- Dependencies: 787
-- Name: evento_idevento_seq; Type: SEQUENCE OWNED BY; Schema: inventario; Owner: postgres
--

ALTER SEQUENCE evento_idevento_seq OWNED BY evento.idevento;


--
-- TOC entry 788 (class 1259 OID 154026)
-- Name: eventoasignacion; Type: VIEW; Schema: inventario; Owner: postgres
--

CREATE VIEW eventoasignacion AS
SELECT asignacion.idasignacion, tipoinventario.idtipoinventario, materialgeneral.idmaterialgeneral, tipomovimiento.desctipomovimiento, tipomaterial.desctipomaterial, movimiento.caracteristicas_asignadas, movimiento.evento, movimiento.lugar FROM asignacion, movimiento, materialgeneral, tipoinventario, tipomovimiento, tipomaterial WHERE (((((((asignacion.idasignacion = movimiento.idasignacion) AND (movimiento.idtipomovimiento = tipomovimiento.idtipomovimiento)) AND (materialgeneral.idmaterialgeneral = movimiento.idmaterialgeneral)) AND (materialgeneral.idtipoinventario = tipoinventario.idtipoinventario)) AND (materialgeneral.idtipomaterial = tipomaterial.idtipomaterial)) AND (movimiento.fecha_exp IS NULL)) AND (asignacion.fe_expiracion IS NULL));


ALTER TABLE inventario.eventoasignacion OWNER TO postgres;

--
-- TOC entry 789 (class 1259 OID 154030)
-- Name: fintipoinventario; Type: TABLE; Schema: inventario; Owner: postgres; Tablespace: 
--

CREATE TABLE fintipoinventario (
    id_fintipoinventario integer NOT NULL,
    desc_fintipoinventario text
);


ALTER TABLE inventario.fintipoinventario OWNER TO postgres;

--
-- TOC entry 790 (class 1259 OID 154036)
-- Name: fintipoinventario_id_fintipoinventario_seq; Type: SEQUENCE; Schema: inventario; Owner: postgres
--

CREATE SEQUENCE fintipoinventario_id_fintipoinventario_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE inventario.fintipoinventario_id_fintipoinventario_seq OWNER TO postgres;

--
-- TOC entry 6630 (class 0 OID 0)
-- Dependencies: 790
-- Name: fintipoinventario_id_fintipoinventario_seq; Type: SEQUENCE OWNED BY; Schema: inventario; Owner: postgres
--

ALTER SEQUENCE fintipoinventario_id_fintipoinventario_seq OWNED BY fintipoinventario.id_fintipoinventario;


--
-- TOC entry 791 (class 1259 OID 154038)
-- Name: inventario_idinventario_seq; Type: SEQUENCE; Schema: inventario; Owner: postgres
--

CREATE SEQUENCE inventario_idinventario_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE inventario.inventario_idinventario_seq OWNER TO postgres;

--
-- TOC entry 6631 (class 0 OID 0)
-- Dependencies: 791
-- Name: inventario_idinventario_seq; Type: SEQUENCE OWNED BY; Schema: inventario; Owner: postgres
--

ALTER SEQUENCE inventario_idinventario_seq OWNED BY inventario.idinventario;


--
-- TOC entry 792 (class 1259 OID 154040)
-- Name: materialgeneral_idmaterialgeneral_seq; Type: SEQUENCE; Schema: inventario; Owner: postgres
--

CREATE SEQUENCE materialgeneral_idmaterialgeneral_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE inventario.materialgeneral_idmaterialgeneral_seq OWNER TO postgres;

--
-- TOC entry 6632 (class 0 OID 0)
-- Dependencies: 792
-- Name: materialgeneral_idmaterialgeneral_seq; Type: SEQUENCE OWNED BY; Schema: inventario; Owner: postgres
--

ALTER SEQUENCE materialgeneral_idmaterialgeneral_seq OWNED BY materialgeneral.idmaterialgeneral;


--
-- TOC entry 793 (class 1259 OID 154042)
-- Name: movimiento_idmovimiento_seq; Type: SEQUENCE; Schema: inventario; Owner: postgres
--

CREATE SEQUENCE movimiento_idmovimiento_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE inventario.movimiento_idmovimiento_seq OWNER TO postgres;

--
-- TOC entry 6633 (class 0 OID 0)
-- Dependencies: 793
-- Name: movimiento_idmovimiento_seq; Type: SEQUENCE OWNED BY; Schema: inventario; Owner: postgres
--

ALTER SEQUENCE movimiento_idmovimiento_seq OWNED BY movimiento.idmovimiento;


--
-- TOC entry 794 (class 1259 OID 154048)
-- Name: rel_caracteristicacerradainv; Type: TABLE; Schema: inventario; Owner: postgres; Tablespace: 
--

CREATE TABLE rel_caracteristicacerradainv (
    id_relcaracteristicacerradainv integer NOT NULL,
    idvalorcerrado integer,
    idtipoinventario integer
);


ALTER TABLE inventario.rel_caracteristicacerradainv OWNER TO postgres;

--
-- TOC entry 795 (class 1259 OID 154051)
-- Name: rel_caracteristicacerradainv_id_relcaracteristicacerradainv_seq; Type: SEQUENCE; Schema: inventario; Owner: postgres
--

CREATE SEQUENCE rel_caracteristicacerradainv_id_relcaracteristicacerradainv_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE inventario.rel_caracteristicacerradainv_id_relcaracteristicacerradainv_seq OWNER TO postgres;

--
-- TOC entry 6634 (class 0 OID 0)
-- Dependencies: 795
-- Name: rel_caracteristicacerradainv_id_relcaracteristicacerradainv_seq; Type: SEQUENCE OWNED BY; Schema: inventario; Owner: postgres
--

ALTER SEQUENCE rel_caracteristicacerradainv_id_relcaracteristicacerradainv_seq OWNED BY rel_caracteristicacerradainv.id_relcaracteristicacerradainv;


--
-- TOC entry 796 (class 1259 OID 154053)
-- Name: rel_tipoinventario_caracteristica; Type: TABLE; Schema: inventario; Owner: postgres; Tablespace: 
--

CREATE TABLE rel_tipoinventario_caracteristica (
    idrel_tipoinventariocaracteristica integer NOT NULL,
    idtipoinventario integer,
    idcaracteristica integer
);


ALTER TABLE inventario.rel_tipoinventario_caracteristica OWNER TO postgres;

--
-- TOC entry 797 (class 1259 OID 154056)
-- Name: rel_tinventariocaracteristica; Type: VIEW; Schema: inventario; Owner: postgres
--

CREATE VIEW rel_tinventariocaracteristica AS
SELECT caracteristica.idcaracteristica, caracteristica.desccararacteristica, caracteristica.idtipocaracteristica, rel_tipoinventario_caracteristica.idtipoinventario FROM (rel_tipoinventario_caracteristica LEFT JOIN caracteristica USING (idcaracteristica));


ALTER TABLE inventario.rel_tinventariocaracteristica OWNER TO postgres;

--
-- TOC entry 798 (class 1259 OID 154060)
-- Name: rel_tipoinventario_caracteris_idrel_tipoinventariocaracteri_seq; Type: SEQUENCE; Schema: inventario; Owner: postgres
--

CREATE SEQUENCE rel_tipoinventario_caracteris_idrel_tipoinventariocaracteri_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE inventario.rel_tipoinventario_caracteris_idrel_tipoinventariocaracteri_seq OWNER TO postgres;

--
-- TOC entry 6635 (class 0 OID 0)
-- Dependencies: 798
-- Name: rel_tipoinventario_caracteris_idrel_tipoinventariocaracteri_seq; Type: SEQUENCE OWNED BY; Schema: inventario; Owner: postgres
--

ALTER SEQUENCE rel_tipoinventario_caracteris_idrel_tipoinventariocaracteri_seq OWNED BY rel_tipoinventario_caracteristica.idrel_tipoinventariocaracteristica;


--
-- TOC entry 799 (class 1259 OID 154062)
-- Name: rel_tipomaterial_servicios; Type: VIEW; Schema: inventario; Owner: postgres
--

CREATE VIEW rel_tipomaterial_servicios AS
SELECT materialgeneral.idtipomaterial, upper(inventario.valorinventariado) AS valorinventariado, tipoinventario.id_fintipoinventario FROM ((inventario LEFT JOIN materialgeneral ON ((inventario.idmaterialgeneral = materialgeneral.idmaterialgeneral))) LEFT JOIN tipoinventario ON ((materialgeneral.idtipoinventario = tipoinventario.idtipoinventario))) WHERE (inventario.idcaracteristica = 18) GROUP BY inventario.valorinventariado, materialgeneral.idtipomaterial, tipoinventario.id_fintipoinventario ORDER BY upper(inventario.valorinventariado);


ALTER TABLE inventario.rel_tipomaterial_servicios OWNER TO postgres;

--
-- TOC entry 800 (class 1259 OID 154066)
-- Name: rel_tipomaterialtipoinventario; Type: TABLE; Schema: inventario; Owner: postgres; Tablespace: 
--

CREATE TABLE rel_tipomaterialtipoinventario (
    idtipomaterial integer,
    idtipoinventario integer,
    id_reltmaterialtinven integer NOT NULL
);


ALTER TABLE inventario.rel_tipomaterialtipoinventario OWNER TO postgres;

--
-- TOC entry 801 (class 1259 OID 154069)
-- Name: rel_tipomaterialtipoinventario_id_reltmaterialtinven_seq; Type: SEQUENCE; Schema: inventario; Owner: postgres
--

CREATE SEQUENCE rel_tipomaterialtipoinventario_id_reltmaterialtinven_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE inventario.rel_tipomaterialtipoinventario_id_reltmaterialtinven_seq OWNER TO postgres;

--
-- TOC entry 6636 (class 0 OID 0)
-- Dependencies: 801
-- Name: rel_tipomaterialtipoinventario_id_reltmaterialtinven_seq; Type: SEQUENCE OWNED BY; Schema: inventario; Owner: postgres
--

ALTER SEQUENCE rel_tipomaterialtipoinventario_id_reltmaterialtinven_seq OWNED BY rel_tipomaterialtipoinventario.id_reltmaterialtinven;


--
-- TOC entry 802 (class 1259 OID 154071)
-- Name: valorcerrado; Type: TABLE; Schema: inventario; Owner: postgres; Tablespace: 
--

CREATE TABLE valorcerrado (
    idvalorcerrado integer NOT NULL,
    descvalorcerrado text,
    idcaracteristica integer
);


ALTER TABLE inventario.valorcerrado OWNER TO postgres;

--
-- TOC entry 803 (class 1259 OID 154077)
-- Name: reltinventariocaracerrada; Type: VIEW; Schema: inventario; Owner: postgres
--

CREATE VIEW reltinventariocaracerrada AS
SELECT rel_caracteristicacerradainv.idvalorcerrado, rel_caracteristicacerradainv.id_relcaracteristicacerradainv, rel_caracteristicacerradainv.idtipoinventario, valorcerrado.descvalorcerrado, valorcerrado.idcaracteristica FROM (rel_caracteristicacerradainv LEFT JOIN valorcerrado USING (idvalorcerrado));


ALTER TABLE inventario.reltinventariocaracerrada OWNER TO postgres;

--
-- TOC entry 804 (class 1259 OID 154081)
-- Name: relttipomatipoinvent; Type: VIEW; Schema: inventario; Owner: postgres
--

CREATE VIEW relttipomatipoinvent AS
SELECT tipomaterial.idtipomaterial, tipomaterial.desctipomaterial, rel_tipomaterialtipoinventario.idtipoinventario, rel_tipomaterialtipoinventario.id_reltmaterialtinven, tipomaterial.sigai_id FROM (tipomaterial LEFT JOIN rel_tipomaterialtipoinventario USING (idtipomaterial));


ALTER TABLE inventario.relttipomatipoinvent OWNER TO postgres;

--
-- TOC entry 805 (class 1259 OID 154085)
-- Name: reporteinventario; Type: VIEW; Schema: inventario; Owner: postgres
--

CREATE VIEW reporteinventario AS
SELECT materialgeneral.idmaterialgeneral, materialgeneral.idtipoinventario, tipoinventario.desctipoinventario, tipomaterial.desctipomaterial, caracteristica.desccararacteristica, vista_inventario.valorinventariado FROM ((((vista_inventario RIGHT JOIN materialgeneral ON ((vista_inventario.idmaterialgeneral = materialgeneral.idmaterialgeneral))) LEFT JOIN tipoinventario ON ((tipoinventario.idtipoinventario = materialgeneral.idtipoinventario))) LEFT JOIN tipomaterial ON ((tipomaterial.idtipomaterial = materialgeneral.idtipomaterial))) LEFT JOIN caracteristica ON ((caracteristica.idcaracteristica = vista_inventario.idcaracteristica))) WHERE (materialgeneral.fecha_exp IS NULL) ORDER BY materialgeneral.idmaterialgeneral DESC;


ALTER TABLE inventario.reporteinventario OWNER TO postgres;

--
-- TOC entry 1131 (class 1259 OID 189343)
-- Name: select_servicios2; Type: VIEW; Schema: inventario; Owner: postgres
--

CREATE VIEW select_servicios2 AS
SELECT concatena((rel_tipomaterial_servicios.idtipomaterial)::text) AS idstipomaterial, rel_tipomaterial_servicios.valorinventariado, rel_tipomaterial_servicios.id_fintipoinventario FROM rel_tipomaterial_servicios GROUP BY rel_tipomaterial_servicios.valorinventariado, rel_tipomaterial_servicios.id_fintipoinventario ORDER BY rel_tipomaterial_servicios.valorinventariado;


ALTER TABLE inventario.select_servicios2 OWNER TO postgres;

--
-- TOC entry 806 (class 1259 OID 154090)
-- Name: tipobusquedaselect; Type: VIEW; Schema: inventario; Owner: postgres
--

CREATE VIEW tipobusquedaselect AS
SELECT inventario.idcaracteristica, lower(inventario.valorinventariado) AS valor, materialgeneral.idtipoinventario, lower(inventario.valorinventariado) AS busqueda FROM (inventario JOIN materialgeneral ON ((inventario.idmaterialgeneral = materialgeneral.idmaterialgeneral))) WHERE (inventario.idcaracteristica = 18) GROUP BY inventario.idcaracteristica, lower(inventario.valorinventariado), materialgeneral.idtipoinventario;


ALTER TABLE inventario.tipobusquedaselect OWNER TO postgres;

--
-- TOC entry 807 (class 1259 OID 154094)
-- Name: tipocaracteristica; Type: TABLE; Schema: inventario; Owner: postgres; Tablespace: 
--

CREATE TABLE tipocaracteristica (
    idtipocaracteristica integer NOT NULL,
    desctipocaracteristica text
);


ALTER TABLE inventario.tipocaracteristica OWNER TO postgres;

--
-- TOC entry 808 (class 1259 OID 154100)
-- Name: tipocaracteristica_idtipocaracteristica_seq; Type: SEQUENCE; Schema: inventario; Owner: postgres
--

CREATE SEQUENCE tipocaracteristica_idtipocaracteristica_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE inventario.tipocaracteristica_idtipocaracteristica_seq OWNER TO postgres;

--
-- TOC entry 6637 (class 0 OID 0)
-- Dependencies: 808
-- Name: tipocaracteristica_idtipocaracteristica_seq; Type: SEQUENCE OWNED BY; Schema: inventario; Owner: postgres
--

ALTER SEQUENCE tipocaracteristica_idtipocaracteristica_seq OWNED BY tipocaracteristica.idtipocaracteristica;


--
-- TOC entry 809 (class 1259 OID 154102)
-- Name: tipoinventario_idtipoinventario_seq; Type: SEQUENCE; Schema: inventario; Owner: postgres
--

CREATE SEQUENCE tipoinventario_idtipoinventario_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE inventario.tipoinventario_idtipoinventario_seq OWNER TO postgres;

--
-- TOC entry 6638 (class 0 OID 0)
-- Dependencies: 809
-- Name: tipoinventario_idtipoinventario_seq; Type: SEQUENCE OWNED BY; Schema: inventario; Owner: postgres
--

ALTER SEQUENCE tipoinventario_idtipoinventario_seq OWNED BY tipoinventario.idtipoinventario;


--
-- TOC entry 810 (class 1259 OID 154104)
-- Name: tipomaterial_idtipomaterial_seq; Type: SEQUENCE; Schema: inventario; Owner: postgres
--

CREATE SEQUENCE tipomaterial_idtipomaterial_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE inventario.tipomaterial_idtipomaterial_seq OWNER TO postgres;

--
-- TOC entry 6639 (class 0 OID 0)
-- Dependencies: 810
-- Name: tipomaterial_idtipomaterial_seq; Type: SEQUENCE OWNED BY; Schema: inventario; Owner: postgres
--

ALTER SEQUENCE tipomaterial_idtipomaterial_seq OWNED BY tipomaterial.idtipomaterial;


--
-- TOC entry 811 (class 1259 OID 154106)
-- Name: tipomovimiento_idtipomovimiento_seq; Type: SEQUENCE; Schema: inventario; Owner: postgres
--

CREATE SEQUENCE tipomovimiento_idtipomovimiento_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE inventario.tipomovimiento_idtipomovimiento_seq OWNER TO postgres;

--
-- TOC entry 6640 (class 0 OID 0)
-- Dependencies: 811
-- Name: tipomovimiento_idtipomovimiento_seq; Type: SEQUENCE OWNED BY; Schema: inventario; Owner: postgres
--

ALTER SEQUENCE tipomovimiento_idtipomovimiento_seq OWNED BY tipomovimiento.idtipomovimiento;


--
-- TOC entry 812 (class 1259 OID 154108)
-- Name: ubicacion; Type: TABLE; Schema: inventario; Owner: postgres; Tablespace: 
--

CREATE TABLE ubicacion (
    idubicacion integer NOT NULL,
    descubicacion text,
    cedula_registro integer,
    fe_expiracion timestamp without time zone DEFAULT '2222-12-31 00:00:00'::timestamp without time zone,
    fe_registro timestamp without time zone DEFAULT now()
);


ALTER TABLE inventario.ubicacion OWNER TO postgres;

--
-- TOC entry 813 (class 1259 OID 154116)
-- Name: ubicaciones_idubicacion_seq; Type: SEQUENCE; Schema: inventario; Owner: postgres
--

CREATE SEQUENCE ubicaciones_idubicacion_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE inventario.ubicaciones_idubicacion_seq OWNER TO postgres;

--
-- TOC entry 6641 (class 0 OID 0)
-- Dependencies: 813
-- Name: ubicaciones_idubicacion_seq; Type: SEQUENCE OWNED BY; Schema: inventario; Owner: postgres
--

ALTER SEQUENCE ubicaciones_idubicacion_seq OWNED BY ubicacion.idubicacion;


--
-- TOC entry 814 (class 1259 OID 154118)
-- Name: valorcerrado_idvalorcerrado_seq; Type: SEQUENCE; Schema: inventario; Owner: postgres
--

CREATE SEQUENCE valorcerrado_idvalorcerrado_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE inventario.valorcerrado_idvalorcerrado_seq OWNER TO postgres;

--
-- TOC entry 6642 (class 0 OID 0)
-- Dependencies: 814
-- Name: valorcerrado_idvalorcerrado_seq; Type: SEQUENCE OWNED BY; Schema: inventario; Owner: postgres
--

ALTER SEQUENCE valorcerrado_idvalorcerrado_seq OWNED BY valorcerrado.idvalorcerrado;


--
-- TOC entry 815 (class 1259 OID 154120)
-- Name: vistageneral_inventario_f5; Type: VIEW; Schema: inventario; Owner: postgres
--

CREATE VIEW vistageneral_inventario_f5 AS
SELECT materialgeneral.idmaterialgeneral, materialgeneral.idtipoinventario, caracteristica.idcaracteristica, tipoinventario.desctipoinventario, tipomaterial.desctipomaterial, caracteristica.desccararacteristica, vista_inventario.valorinventariado, materialgeneral.estado_seleccion, materialgeneral.expiracion_seleccion, tipomaterial.idtipomaterial, tipoinventario.id_gerencia, tipoinventario.id_division, tipoinventario.id_fintipoinventario FROM ((((vista_inventario RIGHT JOIN materialgeneral ON ((vista_inventario.idmaterialgeneral = materialgeneral.idmaterialgeneral))) LEFT JOIN tipoinventario ON ((tipoinventario.idtipoinventario = materialgeneral.idtipoinventario))) LEFT JOIN tipomaterial ON ((tipomaterial.idtipomaterial = materialgeneral.idtipomaterial))) LEFT JOIN caracteristica ON ((caracteristica.idcaracteristica = vista_inventario.idcaracteristica))) WHERE (materialgeneral.fecha_exp IS NULL) ORDER BY materialgeneral.idmaterialgeneral DESC;


ALTER TABLE inventario.vistageneral_inventario_f5 OWNER TO postgres;

SET search_path = f5, pg_catalog;

--
-- TOC entry 5917 (class 2604 OID 189520)
-- Name: id_citacion; Type: DEFAULT; Schema: f5; Owner: postgres
--

ALTER TABLE ONLY t_citacion ALTER COLUMN id_citacion SET DEFAULT nextval('t_citacion_id_citacion_seq'::regclass);


--
-- TOC entry 5942 (class 2604 OID 189521)
-- Name: id_correo; Type: DEFAULT; Schema: f5; Owner: postgres
--

ALTER TABLE ONLY t_correo ALTER COLUMN id_correo SET DEFAULT nextval('t_correo_id_correo_seq'::regclass);


--
-- TOC entry 5943 (class 2604 OID 189522)
-- Name: id_datos_persona; Type: DEFAULT; Schema: f5; Owner: postgres
--

ALTER TABLE ONLY t_datos_persona ALTER COLUMN id_datos_persona SET DEFAULT nextval('t_datos_persona_id_datos_persona_seq'::regclass);


--
-- TOC entry 5903 (class 2604 OID 189523)
-- Name: id_detalle_servicio; Type: DEFAULT; Schema: f5; Owner: postgres
--

ALTER TABLE ONLY t_detalle_servicio ALTER COLUMN id_detalle_servicio SET DEFAULT nextval('t_detalle_servicio_id_detalle_servicio_seq'::regclass);


--
-- TOC entry 5918 (class 2604 OID 189524)
-- Name: id_emision_grabacion; Type: DEFAULT; Schema: f5; Owner: postgres
--

ALTER TABLE ONLY t_emision_grabacion ALTER COLUMN id_emision_grabacion SET DEFAULT nextval('t_emision_grabacion_id_emision_grabacion_seq'::regclass);


--
-- TOC entry 5944 (class 2604 OID 189525)
-- Name: id_estado_informe; Type: DEFAULT; Schema: f5; Owner: postgres
--

ALTER TABLE ONLY t_estado_informe ALTER COLUMN id_estado_informe SET DEFAULT nextval('t_estado_informe_id_estado_informe_seq'::regclass);


--
-- TOC entry 5919 (class 2604 OID 189526)
-- Name: id_estatus; Type: DEFAULT; Schema: f5; Owner: postgres
--

ALTER TABLE ONLY t_estatus ALTER COLUMN id_estatus SET DEFAULT nextval('t_estatus_id_estatus_seq'::regclass);


--
-- TOC entry 5906 (class 2604 OID 189527)
-- Name: id_estatus_detalle_servicio; Type: DEFAULT; Schema: f5; Owner: postgres
--

ALTER TABLE ONLY t_estatus_detalle_servicio ALTER COLUMN id_estatus_detalle_servicio SET DEFAULT nextval('t_estatus_detalle_servicio_id_estatus_detalle_servicio_seq'::regclass);


--
-- TOC entry 5900 (class 2604 OID 189528)
-- Name: id_estatus_pauta; Type: DEFAULT; Schema: f5; Owner: postgres
--

ALTER TABLE ONLY t_estatus_pauta ALTER COLUMN id_estatus_pauta SET DEFAULT nextval('t_estatus_pauta_id_estatus_pauta_seq'::regclass);


--
-- TOC entry 5945 (class 2604 OID 189529)
-- Name: id_informe; Type: DEFAULT; Schema: f5; Owner: postgres
--

ALTER TABLE ONLY t_informe ALTER COLUMN id_informe SET DEFAULT nextval('t_informe_id_informe_seq'::regclass);


--
-- TOC entry 5920 (class 2604 OID 189530)
-- Name: id_locacion; Type: DEFAULT; Schema: f5; Owner: postgres
--

ALTER TABLE ONLY t_locacion ALTER COLUMN id_locacion SET DEFAULT nextval('t_locacion_id_locacion_seq'::regclass);


--
-- TOC entry 5922 (class 2604 OID 189531)
-- Name: id_lugar; Type: DEFAULT; Schema: f5; Owner: postgres
--

ALTER TABLE ONLY t_lugar ALTER COLUMN id_lugar SET DEFAULT nextval('t_lugar_id_lugar_seq'::regclass);


--
-- TOC entry 5924 (class 2604 OID 189532)
-- Name: id_montaje; Type: DEFAULT; Schema: f5; Owner: postgres
--

ALTER TABLE ONLY t_montaje ALTER COLUMN id_montaje SET DEFAULT nextval('t_montaje_id_montaje_seq'::regclass);


--
-- TOC entry 5927 (class 2604 OID 189533)
-- Name: id_pauta; Type: DEFAULT; Schema: f5; Owner: postgres
--

ALTER TABLE ONLY t_pauta ALTER COLUMN id_pauta SET DEFAULT nextval('t_pauta_id_pauta_seq'::regclass);


--
-- TOC entry 5946 (class 2604 OID 189534)
-- Name: id_productor; Type: DEFAULT; Schema: f5; Owner: postgres
--

ALTER TABLE ONLY t_productor ALTER COLUMN id_productor SET DEFAULT nextval('t_productor_id_productor_seq'::regclass);


--
-- TOC entry 5928 (class 2604 OID 189535)
-- Name: id_program; Type: DEFAULT; Schema: f5; Owner: postgres
--

ALTER TABLE ONLY t_programa ALTER COLUMN id_program SET DEFAULT nextval('t_programa_id_program_seq'::regclass);


--
-- TOC entry 5930 (class 2604 OID 189536)
-- Name: id_retorno; Type: DEFAULT; Schema: f5; Owner: postgres
--

ALTER TABLE ONLY t_retorno ALTER COLUMN id_retorno SET DEFAULT nextval('t_retorno_id_retorno_seq'::regclass);


--
-- TOC entry 5931 (class 2604 OID 189537)
-- Name: id_tipo_estatus; Type: DEFAULT; Schema: f5; Owner: postgres
--

ALTER TABLE ONLY t_tipo_estatus ALTER COLUMN id_tipo_estatus SET DEFAULT nextval('t_tipo_estatus_id_tipo_estatus_seq'::regclass);


--
-- TOC entry 5933 (class 2604 OID 189538)
-- Name: id_tipo_evento; Type: DEFAULT; Schema: f5; Owner: postgres
--

ALTER TABLE ONLY t_tipo_evento ALTER COLUMN id_tipo_evento SET DEFAULT nextval('t_tipo_evento_id_tipo_evento_seq'::regclass);


--
-- TOC entry 5935 (class 2604 OID 189539)
-- Name: id_tipo_pauta; Type: DEFAULT; Schema: f5; Owner: postgres
--

ALTER TABLE ONLY t_tipo_pauta ALTER COLUMN id_tipo_pauta SET DEFAULT nextval('t_tipo_pauta_id_tipo_pauta_seq'::regclass);


--
-- TOC entry 5937 (class 2604 OID 189540)
-- Name: id_tipo_traje; Type: DEFAULT; Schema: f5; Owner: postgres
--

ALTER TABLE ONLY t_tipo_traje ALTER COLUMN id_tipo_traje SET DEFAULT nextval('t_tipo_traje_id_tipo_traje_seq'::regclass);


SET search_path = inventario, pg_catalog;

--
-- TOC entry 5948 (class 2604 OID 189581)
-- Name: idasignacion; Type: DEFAULT; Schema: inventario; Owner: postgres
--

ALTER TABLE ONLY asignacion ALTER COLUMN idasignacion SET DEFAULT nextval('asignacion_idasignacion_seq'::regclass);


--
-- TOC entry 5912 (class 2604 OID 189582)
-- Name: idcaracteristica; Type: DEFAULT; Schema: inventario; Owner: postgres
--

ALTER TABLE ONLY caracteristica ALTER COLUMN idcaracteristica SET DEFAULT nextval('caracteristica_idcaracteristica_seq'::regclass);


--
-- TOC entry 5950 (class 2604 OID 189583)
-- Name: idcomentario; Type: DEFAULT; Schema: inventario; Owner: postgres
--

ALTER TABLE ONLY comentario ALTER COLUMN idcomentario SET DEFAULT nextval('comentarios_idcomentario_seq'::regclass);


--
-- TOC entry 5953 (class 2604 OID 189584)
-- Name: idevento; Type: DEFAULT; Schema: inventario; Owner: postgres
--

ALTER TABLE ONLY evento ALTER COLUMN idevento SET DEFAULT nextval('evento_idevento_seq'::regclass);


--
-- TOC entry 5954 (class 2604 OID 189585)
-- Name: id_fintipoinventario; Type: DEFAULT; Schema: inventario; Owner: postgres
--

ALTER TABLE ONLY fintipoinventario ALTER COLUMN id_fintipoinventario SET DEFAULT nextval('fintipoinventario_id_fintipoinventario_seq'::regclass);


--
-- TOC entry 5907 (class 2604 OID 189586)
-- Name: idinventario; Type: DEFAULT; Schema: inventario; Owner: postgres
--

ALTER TABLE ONLY inventario ALTER COLUMN idinventario SET DEFAULT nextval('inventario_idinventario_seq'::regclass);


--
-- TOC entry 5908 (class 2604 OID 189587)
-- Name: idmaterialgeneral; Type: DEFAULT; Schema: inventario; Owner: postgres
--

ALTER TABLE ONLY materialgeneral ALTER COLUMN idmaterialgeneral SET DEFAULT nextval('materialgeneral_idmaterialgeneral_seq'::regclass);


--
-- TOC entry 5915 (class 2604 OID 189588)
-- Name: idmovimiento; Type: DEFAULT; Schema: inventario; Owner: postgres
--

ALTER TABLE ONLY movimiento ALTER COLUMN idmovimiento SET DEFAULT nextval('movimiento_idmovimiento_seq'::regclass);


--
-- TOC entry 5955 (class 2604 OID 189589)
-- Name: id_relcaracteristicacerradainv; Type: DEFAULT; Schema: inventario; Owner: postgres
--

ALTER TABLE ONLY rel_caracteristicacerradainv ALTER COLUMN id_relcaracteristicacerradainv SET DEFAULT nextval('rel_caracteristicacerradainv_id_relcaracteristicacerradainv_seq'::regclass);


--
-- TOC entry 5956 (class 2604 OID 189590)
-- Name: idrel_tipoinventariocaracteristica; Type: DEFAULT; Schema: inventario; Owner: postgres
--

ALTER TABLE ONLY rel_tipoinventario_caracteristica ALTER COLUMN idrel_tipoinventariocaracteristica SET DEFAULT nextval('rel_tipoinventario_caracteris_idrel_tipoinventariocaracteri_seq'::regclass);


--
-- TOC entry 5957 (class 2604 OID 189591)
-- Name: id_reltmaterialtinven; Type: DEFAULT; Schema: inventario; Owner: postgres
--

ALTER TABLE ONLY rel_tipomaterialtipoinventario ALTER COLUMN id_reltmaterialtinven SET DEFAULT nextval('rel_tipomaterialtipoinventario_id_reltmaterialtinven_seq'::regclass);


--
-- TOC entry 5959 (class 2604 OID 189592)
-- Name: idtipocaracteristica; Type: DEFAULT; Schema: inventario; Owner: postgres
--

ALTER TABLE ONLY tipocaracteristica ALTER COLUMN idtipocaracteristica SET DEFAULT nextval('tipocaracteristica_idtipocaracteristica_seq'::regclass);


--
-- TOC entry 5910 (class 2604 OID 189593)
-- Name: idtipoinventario; Type: DEFAULT; Schema: inventario; Owner: postgres
--

ALTER TABLE ONLY tipoinventario ALTER COLUMN idtipoinventario SET DEFAULT nextval('tipoinventario_idtipoinventario_seq'::regclass);


--
-- TOC entry 5911 (class 2604 OID 189594)
-- Name: idtipomaterial; Type: DEFAULT; Schema: inventario; Owner: postgres
--

ALTER TABLE ONLY tipomaterial ALTER COLUMN idtipomaterial SET DEFAULT nextval('tipomaterial_idtipomaterial_seq'::regclass);


--
-- TOC entry 5916 (class 2604 OID 189595)
-- Name: idtipomovimiento; Type: DEFAULT; Schema: inventario; Owner: postgres
--

ALTER TABLE ONLY tipomovimiento ALTER COLUMN idtipomovimiento SET DEFAULT nextval('tipomovimiento_idtipomovimiento_seq'::regclass);


--
-- TOC entry 5962 (class 2604 OID 189596)
-- Name: idubicacion; Type: DEFAULT; Schema: inventario; Owner: postgres
--

ALTER TABLE ONLY ubicacion ALTER COLUMN idubicacion SET DEFAULT nextval('ubicaciones_idubicacion_seq'::regclass);


--
-- TOC entry 5958 (class 2604 OID 189597)
-- Name: idvalorcerrado; Type: DEFAULT; Schema: inventario; Owner: postgres
--

ALTER TABLE ONLY valorcerrado ALTER COLUMN idvalorcerrado SET DEFAULT nextval('valorcerrado_idvalorcerrado_seq'::regclass);


SET search_path = f5, pg_catalog;

--
-- TOC entry 6011 (class 2606 OID 161837)
-- Name: pk_id_correo; Type: CONSTRAINT; Schema: f5; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY t_correo
    ADD CONSTRAINT pk_id_correo PRIMARY KEY (id_correo);


--
-- TOC entry 5985 (class 2606 OID 161839)
-- Name: t_citacion_pkey; Type: CONSTRAINT; Schema: f5; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY t_citacion
    ADD CONSTRAINT t_citacion_pkey PRIMARY KEY (id_citacion);


--
-- TOC entry 6013 (class 2606 OID 161841)
-- Name: t_datos_persona_pkey; Type: CONSTRAINT; Schema: f5; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY t_datos_persona
    ADD CONSTRAINT t_datos_persona_pkey PRIMARY KEY (id_datos_persona);


--
-- TOC entry 5966 (class 2606 OID 161843)
-- Name: t_detalle_servicio_pkey; Type: CONSTRAINT; Schema: f5; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY t_detalle_servicio
    ADD CONSTRAINT t_detalle_servicio_pkey PRIMARY KEY (id_detalle_servicio);


--
-- TOC entry 5987 (class 2606 OID 161845)
-- Name: t_emision_grabacion_pkey; Type: CONSTRAINT; Schema: f5; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY t_emision_grabacion
    ADD CONSTRAINT t_emision_grabacion_pkey PRIMARY KEY (id_emision_grabacion);


--
-- TOC entry 6015 (class 2606 OID 161847)
-- Name: t_estado_informe_pkey; Type: CONSTRAINT; Schema: f5; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY t_estado_informe
    ADD CONSTRAINT t_estado_informe_pkey PRIMARY KEY (id_estado_informe);


--
-- TOC entry 5969 (class 2606 OID 161849)
-- Name: t_estatus_detalle_servicio_pkey; Type: CONSTRAINT; Schema: f5; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY t_estatus_detalle_servicio
    ADD CONSTRAINT t_estatus_detalle_servicio_pkey PRIMARY KEY (id_estatus_detalle_servicio);


--
-- TOC entry 5964 (class 2606 OID 161851)
-- Name: t_estatus_pauta_pkey; Type: CONSTRAINT; Schema: f5; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY t_estatus_pauta
    ADD CONSTRAINT t_estatus_pauta_pkey PRIMARY KEY (id_estatus_pauta);


--
-- TOC entry 5989 (class 2606 OID 161853)
-- Name: t_estatus_pkey; Type: CONSTRAINT; Schema: f5; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY t_estatus
    ADD CONSTRAINT t_estatus_pkey PRIMARY KEY (id_estatus);


--
-- TOC entry 6017 (class 2606 OID 161855)
-- Name: t_informe_pkey; Type: CONSTRAINT; Schema: f5; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY t_informe
    ADD CONSTRAINT t_informe_pkey PRIMARY KEY (id_informe);


--
-- TOC entry 5991 (class 2606 OID 161857)
-- Name: t_locacion_pkey; Type: CONSTRAINT; Schema: f5; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY t_locacion
    ADD CONSTRAINT t_locacion_pkey PRIMARY KEY (id_locacion);


--
-- TOC entry 5993 (class 2606 OID 161859)
-- Name: t_lugar_pkey; Type: CONSTRAINT; Schema: f5; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY t_lugar
    ADD CONSTRAINT t_lugar_pkey PRIMARY KEY (id_lugar);


--
-- TOC entry 5995 (class 2606 OID 161861)
-- Name: t_montaje_pkey; Type: CONSTRAINT; Schema: f5; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY t_montaje
    ADD CONSTRAINT t_montaje_pkey PRIMARY KEY (id_montaje);


--
-- TOC entry 5997 (class 2606 OID 161863)
-- Name: t_pauta_pkey; Type: CONSTRAINT; Schema: f5; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY t_pauta
    ADD CONSTRAINT t_pauta_pkey PRIMARY KEY (id_pauta);


--
-- TOC entry 6019 (class 2606 OID 161865)
-- Name: t_productor_pkey; Type: CONSTRAINT; Schema: f5; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY t_productor
    ADD CONSTRAINT t_productor_pkey PRIMARY KEY (id_productor);


--
-- TOC entry 5999 (class 2606 OID 161867)
-- Name: t_programa_pkey; Type: CONSTRAINT; Schema: f5; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY t_programa
    ADD CONSTRAINT t_programa_pkey PRIMARY KEY (id_program);


--
-- TOC entry 6001 (class 2606 OID 161869)
-- Name: t_retorno_pkey; Type: CONSTRAINT; Schema: f5; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY t_retorno
    ADD CONSTRAINT t_retorno_pkey PRIMARY KEY (id_retorno);


--
-- TOC entry 6003 (class 2606 OID 161871)
-- Name: t_tipo_estatus_pkey; Type: CONSTRAINT; Schema: f5; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY t_tipo_estatus
    ADD CONSTRAINT t_tipo_estatus_pkey PRIMARY KEY (id_tipo_estatus);


--
-- TOC entry 6005 (class 2606 OID 161873)
-- Name: t_tipo_evento_pkey; Type: CONSTRAINT; Schema: f5; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY t_tipo_evento
    ADD CONSTRAINT t_tipo_evento_pkey PRIMARY KEY (id_tipo_evento);


--
-- TOC entry 6007 (class 2606 OID 161875)
-- Name: t_tipo_pauta_pkey; Type: CONSTRAINT; Schema: f5; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY t_tipo_pauta
    ADD CONSTRAINT t_tipo_pauta_pkey PRIMARY KEY (id_tipo_pauta);


--
-- TOC entry 6009 (class 2606 OID 161877)
-- Name: t_tipo_traje_pkey; Type: CONSTRAINT; Schema: f5; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY t_tipo_traje
    ADD CONSTRAINT t_tipo_traje_pkey PRIMARY KEY (id_tipo_traje);


SET search_path = inventario, pg_catalog;

--
-- TOC entry 6027 (class 2606 OID 161959)
-- Name: pk_fintipoinventario; Type: CONSTRAINT; Schema: inventario; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY fintipoinventario
    ADD CONSTRAINT pk_fintipoinventario PRIMARY KEY (id_fintipoinventario);


--
-- TOC entry 6021 (class 2606 OID 161961)
-- Name: pk_idasignacion; Type: CONSTRAINT; Schema: inventario; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY asignacion
    ADD CONSTRAINT pk_idasignacion PRIMARY KEY (idasignacion);


--
-- TOC entry 5979 (class 2606 OID 161963)
-- Name: pk_idcaracteristica; Type: CONSTRAINT; Schema: inventario; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY caracteristica
    ADD CONSTRAINT pk_idcaracteristica PRIMARY KEY (idcaracteristica);


--
-- TOC entry 6023 (class 2606 OID 161965)
-- Name: pk_idcomentario; Type: CONSTRAINT; Schema: inventario; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY comentario
    ADD CONSTRAINT pk_idcomentario PRIMARY KEY (idcomentario);


--
-- TOC entry 6025 (class 2606 OID 161967)
-- Name: pk_idevento; Type: CONSTRAINT; Schema: inventario; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY evento
    ADD CONSTRAINT pk_idevento PRIMARY KEY (idevento);


--
-- TOC entry 5971 (class 2606 OID 161969)
-- Name: pk_idinventario; Type: CONSTRAINT; Schema: inventario; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY inventario
    ADD CONSTRAINT pk_idinventario PRIMARY KEY (idinventario);


--
-- TOC entry 5973 (class 2606 OID 161971)
-- Name: pk_idmaterialgeneral; Type: CONSTRAINT; Schema: inventario; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY materialgeneral
    ADD CONSTRAINT pk_idmaterialgeneral PRIMARY KEY (idmaterialgeneral);


--
-- TOC entry 5981 (class 2606 OID 161973)
-- Name: pk_idmovimiento; Type: CONSTRAINT; Schema: inventario; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY movimiento
    ADD CONSTRAINT pk_idmovimiento PRIMARY KEY (idmovimiento);


--
-- TOC entry 6037 (class 2606 OID 161975)
-- Name: pk_idtipocaracteristica; Type: CONSTRAINT; Schema: inventario; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tipocaracteristica
    ADD CONSTRAINT pk_idtipocaracteristica PRIMARY KEY (idtipocaracteristica);


--
-- TOC entry 5975 (class 2606 OID 161977)
-- Name: pk_idtipoinventario; Type: CONSTRAINT; Schema: inventario; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tipoinventario
    ADD CONSTRAINT pk_idtipoinventario PRIMARY KEY (idtipoinventario);


--
-- TOC entry 6031 (class 2606 OID 161979)
-- Name: pk_idtipoinventariocaracteristica; Type: CONSTRAINT; Schema: inventario; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY rel_tipoinventario_caracteristica
    ADD CONSTRAINT pk_idtipoinventariocaracteristica PRIMARY KEY (idrel_tipoinventariocaracteristica);


--
-- TOC entry 5977 (class 2606 OID 161981)
-- Name: pk_idtipomaterial; Type: CONSTRAINT; Schema: inventario; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tipomaterial
    ADD CONSTRAINT pk_idtipomaterial PRIMARY KEY (idtipomaterial);


--
-- TOC entry 5983 (class 2606 OID 161983)
-- Name: pk_idtipomovimiento; Type: CONSTRAINT; Schema: inventario; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tipomovimiento
    ADD CONSTRAINT pk_idtipomovimiento PRIMARY KEY (idtipomovimiento);


--
-- TOC entry 6039 (class 2606 OID 161985)
-- Name: pk_idubicacion; Type: CONSTRAINT; Schema: inventario; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ubicacion
    ADD CONSTRAINT pk_idubicacion PRIMARY KEY (idubicacion);


--
-- TOC entry 6035 (class 2606 OID 161987)
-- Name: pk_idvalorcerrado; Type: CONSTRAINT; Schema: inventario; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY valorcerrado
    ADD CONSTRAINT pk_idvalorcerrado PRIMARY KEY (idvalorcerrado);


--
-- TOC entry 6029 (class 2606 OID 161989)
-- Name: pk_relcaracteristicacerradainv; Type: CONSTRAINT; Schema: inventario; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY rel_caracteristicacerradainv
    ADD CONSTRAINT pk_relcaracteristicacerradainv PRIMARY KEY (id_relcaracteristicacerradainv);


--
-- TOC entry 6033 (class 2606 OID 161991)
-- Name: pk_reltmaterialtinvent; Type: CONSTRAINT; Schema: inventario; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY rel_tipomaterialtipoinventario
    ADD CONSTRAINT pk_reltmaterialtinvent PRIMARY KEY (id_reltmaterialtinven);


SET search_path = f5, pg_catalog;

--
-- TOC entry 5967 (class 1259 OID 162302)
-- Name: fki_; Type: INDEX; Schema: f5; Owner: postgres; Tablespace: 
--

CREATE INDEX fki_ ON t_estatus_detalle_servicio USING btree (id_detalle_servicio);


--
-- TOC entry 6054 (class 2606 OID 162809)
-- Name: t_citacion_id_lugar_fkey; Type: FK CONSTRAINT; Schema: f5; Owner: postgres
--

ALTER TABLE ONLY t_citacion
    ADD CONSTRAINT t_citacion_id_lugar_fkey FOREIGN KEY (id_lugar) REFERENCES t_lugar(id_lugar) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 6042 (class 2606 OID 162814)
-- Name: t_detalle_servicio_id_pauta_fkey; Type: FK CONSTRAINT; Schema: f5; Owner: postgres
--

ALTER TABLE ONLY t_detalle_servicio
    ADD CONSTRAINT t_detalle_servicio_id_pauta_fkey FOREIGN KEY (id_pauta) REFERENCES t_pauta(id_pauta) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 6045 (class 2606 OID 162819)
-- Name: t_estatus_detalle_servicio_id_detalle_servicio_fkey; Type: FK CONSTRAINT; Schema: f5; Owner: postgres
--

ALTER TABLE ONLY t_estatus_detalle_servicio
    ADD CONSTRAINT t_estatus_detalle_servicio_id_detalle_servicio_fkey FOREIGN KEY (id_detalle_servicio) REFERENCES t_detalle_servicio(id_detalle_servicio);


--
-- TOC entry 6044 (class 2606 OID 162824)
-- Name: t_estatus_detalle_servicio_id_estatus_fkey; Type: FK CONSTRAINT; Schema: f5; Owner: postgres
--

ALTER TABLE ONLY t_estatus_detalle_servicio
    ADD CONSTRAINT t_estatus_detalle_servicio_id_estatus_fkey FOREIGN KEY (id_estatus) REFERENCES t_estatus(id_estatus);


--
-- TOC entry 6043 (class 2606 OID 162829)
-- Name: t_estatus_detalle_servicio_id_pauta_fkey; Type: FK CONSTRAINT; Schema: f5; Owner: postgres
--

ALTER TABLE ONLY t_estatus_detalle_servicio
    ADD CONSTRAINT t_estatus_detalle_servicio_id_pauta_fkey FOREIGN KEY (id_pauta) REFERENCES t_pauta(id_pauta);


--
-- TOC entry 6055 (class 2606 OID 162834)
-- Name: t_estatus_id_tipo_estatus_fkey; Type: FK CONSTRAINT; Schema: f5; Owner: postgres
--

ALTER TABLE ONLY t_estatus
    ADD CONSTRAINT t_estatus_id_tipo_estatus_fkey FOREIGN KEY (id_tipo_estatus) REFERENCES t_tipo_estatus(id_tipo_estatus);


--
-- TOC entry 6041 (class 2606 OID 162839)
-- Name: t_estatus_pauta_id_estatus_fkey; Type: FK CONSTRAINT; Schema: f5; Owner: postgres
--

ALTER TABLE ONLY t_estatus_pauta
    ADD CONSTRAINT t_estatus_pauta_id_estatus_fkey FOREIGN KEY (id_estatus) REFERENCES t_estatus(id_estatus);


--
-- TOC entry 6040 (class 2606 OID 162844)
-- Name: t_estatus_pauta_id_pauta_fkey; Type: FK CONSTRAINT; Schema: f5; Owner: postgres
--

ALTER TABLE ONLY t_estatus_pauta
    ADD CONSTRAINT t_estatus_pauta_id_pauta_fkey FOREIGN KEY (id_pauta) REFERENCES t_pauta(id_pauta);


--
-- TOC entry 6066 (class 2606 OID 162849)
-- Name: t_informe_id_detalle_servicio_fkey; Type: FK CONSTRAINT; Schema: f5; Owner: postgres
--

ALTER TABLE ONLY t_informe
    ADD CONSTRAINT t_informe_id_detalle_servicio_fkey FOREIGN KEY (id_detalle_servicio) REFERENCES t_detalle_servicio(id_detalle_servicio);


--
-- TOC entry 6065 (class 2606 OID 162854)
-- Name: t_informe_id_pauta_fkey; Type: FK CONSTRAINT; Schema: f5; Owner: postgres
--

ALTER TABLE ONLY t_informe
    ADD CONSTRAINT t_informe_id_pauta_fkey FOREIGN KEY (id_pauta) REFERENCES t_pauta(id_pauta);


--
-- TOC entry 6056 (class 2606 OID 162859)
-- Name: t_pauta_id_citacion_fkey; Type: FK CONSTRAINT; Schema: f5; Owner: postgres
--

ALTER TABLE ONLY t_pauta
    ADD CONSTRAINT t_pauta_id_citacion_fkey FOREIGN KEY (id_citacion) REFERENCES t_citacion(id_citacion) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 6057 (class 2606 OID 162864)
-- Name: t_pauta_id_emision_grabacion_fkey; Type: FK CONSTRAINT; Schema: f5; Owner: postgres
--

ALTER TABLE ONLY t_pauta
    ADD CONSTRAINT t_pauta_id_emision_grabacion_fkey FOREIGN KEY (id_emision_grabacion) REFERENCES t_emision_grabacion(id_emision_grabacion) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 6058 (class 2606 OID 162869)
-- Name: t_pauta_id_locacion_fkey; Type: FK CONSTRAINT; Schema: f5; Owner: postgres
--

ALTER TABLE ONLY t_pauta
    ADD CONSTRAINT t_pauta_id_locacion_fkey FOREIGN KEY (id_locacion) REFERENCES t_locacion(id_locacion) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 6059 (class 2606 OID 162874)
-- Name: t_pauta_id_montaje_fkey; Type: FK CONSTRAINT; Schema: f5; Owner: postgres
--

ALTER TABLE ONLY t_pauta
    ADD CONSTRAINT t_pauta_id_montaje_fkey FOREIGN KEY (id_montaje) REFERENCES t_montaje(id_montaje) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 6060 (class 2606 OID 162879)
-- Name: t_pauta_id_program_fkey; Type: FK CONSTRAINT; Schema: f5; Owner: postgres
--

ALTER TABLE ONLY t_pauta
    ADD CONSTRAINT t_pauta_id_program_fkey FOREIGN KEY (id_program) REFERENCES t_programa(id_program) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 6061 (class 2606 OID 162884)
-- Name: t_pauta_id_retorno_fkey; Type: FK CONSTRAINT; Schema: f5; Owner: postgres
--

ALTER TABLE ONLY t_pauta
    ADD CONSTRAINT t_pauta_id_retorno_fkey FOREIGN KEY (id_retorno) REFERENCES t_retorno(id_retorno) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 6062 (class 2606 OID 162889)
-- Name: t_pauta_id_tipo_evento_fkey; Type: FK CONSTRAINT; Schema: f5; Owner: postgres
--

ALTER TABLE ONLY t_pauta
    ADD CONSTRAINT t_pauta_id_tipo_evento_fkey FOREIGN KEY (id_tipo_evento) REFERENCES t_tipo_evento(id_tipo_evento) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 6063 (class 2606 OID 162894)
-- Name: t_pauta_id_tipo_pauta_fkey; Type: FK CONSTRAINT; Schema: f5; Owner: postgres
--

ALTER TABLE ONLY t_pauta
    ADD CONSTRAINT t_pauta_id_tipo_pauta_fkey FOREIGN KEY (id_tipo_pauta) REFERENCES t_tipo_pauta(id_tipo_pauta) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 6064 (class 2606 OID 162899)
-- Name: t_pauta_id_tipo_traje_fkey; Type: FK CONSTRAINT; Schema: f5; Owner: postgres
--

ALTER TABLE ONLY t_pauta
    ADD CONSTRAINT t_pauta_id_tipo_traje_fkey FOREIGN KEY (id_tipo_traje) REFERENCES t_tipo_traje(id_tipo_traje) ON UPDATE RESTRICT ON DELETE RESTRICT;


SET search_path = inventario, pg_catalog;

--
-- TOC entry 6050 (class 2606 OID 163029)
-- Name: fk_fintipoinventario; Type: FK CONSTRAINT; Schema: inventario; Owner: postgres
--

ALTER TABLE ONLY tipoinventario
    ADD CONSTRAINT fk_fintipoinventario FOREIGN KEY (id_fintipoinventario) REFERENCES fintipoinventario(id_fintipoinventario) MATCH FULL;


--
-- TOC entry 6047 (class 2606 OID 163039)
-- Name: fk_idcaracteristica; Type: FK CONSTRAINT; Schema: inventario; Owner: postgres
--

ALTER TABLE ONLY inventario
    ADD CONSTRAINT fk_idcaracteristica FOREIGN KEY (idcaracteristica) REFERENCES caracteristica(idcaracteristica) MATCH FULL;


--
-- TOC entry 6074 (class 2606 OID 163044)
-- Name: fk_idcaracteristica; Type: FK CONSTRAINT; Schema: inventario; Owner: postgres
--

ALTER TABLE ONLY valorcerrado
    ADD CONSTRAINT fk_idcaracteristica FOREIGN KEY (idcaracteristica) REFERENCES caracteristica(idcaracteristica) MATCH FULL;


--
-- TOC entry 6046 (class 2606 OID 163049)
-- Name: fk_idmaterialgeneral; Type: FK CONSTRAINT; Schema: inventario; Owner: postgres
--

ALTER TABLE ONLY inventario
    ADD CONSTRAINT fk_idmaterialgeneral FOREIGN KEY (idmaterialgeneral) REFERENCES materialgeneral(idmaterialgeneral) MATCH FULL;


--
-- TOC entry 6053 (class 2606 OID 163054)
-- Name: fk_idmaterialgeneral; Type: FK CONSTRAINT; Schema: inventario; Owner: postgres
--

ALTER TABLE ONLY movimiento
    ADD CONSTRAINT fk_idmaterialgeneral FOREIGN KEY (idmaterialgeneral) REFERENCES materialgeneral(idmaterialgeneral) MATCH FULL;


--
-- TOC entry 6067 (class 2606 OID 163059)
-- Name: fk_idmaterialgeneral; Type: FK CONSTRAINT; Schema: inventario; Owner: postgres
--

ALTER TABLE ONLY comentario
    ADD CONSTRAINT fk_idmaterialgeneral FOREIGN KEY (idmaterialgeneral) REFERENCES materialgeneral(idmaterialgeneral) MATCH FULL;


--
-- TOC entry 6051 (class 2606 OID 163064)
-- Name: fk_idtipocaracteristica; Type: FK CONSTRAINT; Schema: inventario; Owner: postgres
--

ALTER TABLE ONLY caracteristica
    ADD CONSTRAINT fk_idtipocaracteristica FOREIGN KEY (idtipocaracteristica) REFERENCES tipocaracteristica(idtipocaracteristica) MATCH FULL;


--
-- TOC entry 6069 (class 2606 OID 163069)
-- Name: fk_idtipoinventario; Type: FK CONSTRAINT; Schema: inventario; Owner: postgres
--

ALTER TABLE ONLY rel_caracteristicacerradainv
    ADD CONSTRAINT fk_idtipoinventario FOREIGN KEY (idtipoinventario) REFERENCES tipoinventario(idtipoinventario) MATCH FULL;


--
-- TOC entry 6049 (class 2606 OID 163074)
-- Name: fk_idtipomaterial; Type: FK CONSTRAINT; Schema: inventario; Owner: postgres
--

ALTER TABLE ONLY materialgeneral
    ADD CONSTRAINT fk_idtipomaterial FOREIGN KEY (idtipomaterial) REFERENCES tipomaterial(idtipomaterial) MATCH FULL;


--
-- TOC entry 6073 (class 2606 OID 163079)
-- Name: fk_idtipomaterial; Type: FK CONSTRAINT; Schema: inventario; Owner: postgres
--

ALTER TABLE ONLY rel_tipomaterialtipoinventario
    ADD CONSTRAINT fk_idtipomaterial FOREIGN KEY (idtipomaterial) REFERENCES tipomaterial(idtipomaterial);


--
-- TOC entry 6052 (class 2606 OID 163084)
-- Name: fk_idtipomovimiento; Type: FK CONSTRAINT; Schema: inventario; Owner: postgres
--

ALTER TABLE ONLY movimiento
    ADD CONSTRAINT fk_idtipomovimiento FOREIGN KEY (idtipomovimiento) REFERENCES tipomovimiento(idtipomovimiento) MATCH FULL;


--
-- TOC entry 6068 (class 2606 OID 163089)
-- Name: fk_idvalorcerrado; Type: FK CONSTRAINT; Schema: inventario; Owner: postgres
--

ALTER TABLE ONLY rel_caracteristicacerradainv
    ADD CONSTRAINT fk_idvalorcerrado FOREIGN KEY (idvalorcerrado) REFERENCES valorcerrado(idvalorcerrado) MATCH FULL;


--
-- TOC entry 6048 (class 2606 OID 163094)
-- Name: fk_tipoinventario; Type: FK CONSTRAINT; Schema: inventario; Owner: postgres
--

ALTER TABLE ONLY materialgeneral
    ADD CONSTRAINT fk_tipoinventario FOREIGN KEY (idtipoinventario) REFERENCES tipoinventario(idtipoinventario) MATCH FULL;


--
-- TOC entry 6072 (class 2606 OID 163099)
-- Name: fk_tipoinventario; Type: FK CONSTRAINT; Schema: inventario; Owner: postgres
--

ALTER TABLE ONLY rel_tipomaterialtipoinventario
    ADD CONSTRAINT fk_tipoinventario FOREIGN KEY (idtipoinventario) REFERENCES tipoinventario(idtipoinventario);


--
-- TOC entry 6071 (class 2606 OID 163104)
-- Name: idcaracteristica; Type: FK CONSTRAINT; Schema: inventario; Owner: postgres
--

ALTER TABLE ONLY rel_tipoinventario_caracteristica
    ADD CONSTRAINT idcaracteristica FOREIGN KEY (idcaracteristica) REFERENCES caracteristica(idcaracteristica);


--
-- TOC entry 6070 (class 2606 OID 163109)
-- Name: idtipoinventario; Type: FK CONSTRAINT; Schema: inventario; Owner: postgres
--

ALTER TABLE ONLY rel_tipoinventario_caracteristica
    ADD CONSTRAINT idtipoinventario FOREIGN KEY (idtipoinventario) REFERENCES tipoinventario(idtipoinventario) MATCH FULL;


-- Completed on 2014-07-23 16:43:44

--
-- PostgreSQL database dump complete
--

